package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eventb.core.IEvent;
import org.eventb.core.IInvariant;
import org.eventb.core.IMachineRoot;
import org.eventb.core.IVariable;
import org.eventb.core.ast.Predicate;
import org.rodinp.core.RodinDBException;

public class DAMachine implements Axiomatizable{
	
	private IMachineRoot machine;
	private DAParser parser;

	/**
	 * @param machine
	 * @throws CoreException
	 */
	public DAMachine(IMachineRoot machine) throws CoreException {
		this.machine = machine;
		this.parser = new DAParser(machine.getFormulaFactory(), machine.getSCMachineRoot().getTypeEnvironment());
	}

	public IMachineRoot getMachineRoot() {
		return machine;
	}

	public DAParser getParser() {
		return this.parser;
	}
	
	public List<IEvent>	getEvents() throws RodinDBException {
		return new ArrayList<IEvent>(Arrays.asList(this.machine.getEvents()));
	}

	/**
	 * @param label
	 * 		The label of the invariant to parse.
	 * @return the parsed predicate of the invariant with corresponding label.
	 * 		Returns null if there is no invariant with this label in the machine.
	 * @throws RodinDBException
	 * 		if there was a problem accessing the database.
	 */
	@Override
	public Predicate getParsedAxiom(String label) throws RodinDBException {
		for(IInvariant invariant : machine.getInvariants()) {
			if(invariant.getLabel().equals(label)) {
				return parser.parsePredicate(invariant.getPredicateString());
			}
		}
		return null;
	}

	public List<String> getVariables() throws RodinDBException {
		List<String> variables = new ArrayList<String>();
		for(IVariable var : this.machine.getVariables()) {
			variables.add(var.getIdentifierString());
		}		
		return variables;
	}
}
