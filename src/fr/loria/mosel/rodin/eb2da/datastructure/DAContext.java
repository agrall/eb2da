package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eventb.core.IAxiom;
import org.eventb.core.IContextRoot;
import org.eventb.core.ast.Predicate;
import org.rodinp.core.RodinDBException;

public class DAContext implements Axiomatizable{
	private IContextRoot context;
	private DAParser parser;
	
	/**
	 * @param context
	 * @throws CoreException
	 */
	public DAContext(IContextRoot context) throws CoreException {
		this.context = context;
		this.parser = new DAParser(context.getFormulaFactory(),
				context.getSCContextRoot().getTypeEnvironment());
	}

	public DAParser getParser() {
		return this.parser;
	}
	
//	
//	public IContextRoot getContextRoot() {
//		return this.context;
//		// Note that it is not necessary to copy the IContextRoot object context
//		// 		since it is associated to a file 
//		//		and modifying the context is equivalent to modifying this file.
//		// 		Therefore modifying a copy of the context will modify the original file.
//	}
//	
	/**
	 * @param label
	 * 		the label of the axiom to look for
	 * @return the Predicate of the axiom with label label in this context.
	 * 		Returns null if no axiom with this label was found.
	 * @throws RodinDBException
	 * 		if there was a problem accessing the database
	 */
	@Override
	public Predicate getParsedAxiom(String label) throws RodinDBException {
		for(IAxiom axiom : context.getAxioms()) {
			if(axiom.getLabel().equals(label)) {
				return parser.parsePredicate(axiom.getPredicateString());
			}
		}
		return null;
	}
	
	public Predicate parsePredicate(String formula) {
		return parser.parsePredicate(formula);
	}

	public List<IAxiom> getAxioms() throws RodinDBException {
		return Arrays.asList(context.getAxioms());
	}
	
	
}
