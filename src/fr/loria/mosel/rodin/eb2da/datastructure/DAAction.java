package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.AssociativeExpression;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.SetExtension;

public class DAAction {
	
	private Boolean isReceive;
	private Boolean isSend;
	// assignedVariable is of the form var(proc)
	private String assignedVariable;
	private Expression assignmentExpression;

	public DAAction(String var, Expression expr, DANames names, String proc) {
		this.isReceive = false;
		this.isSend = false;
		if(var.equals(names.getChannels())) {
			this.assignedVariable = var;
			this.assignmentExpression = expr;
			FreeIdentifier[] fi = expr.getFreeIdentifiers();
			for(int i = 0; i < fi.length && !(this.isReceive && this.isSend); i++) {
				if(fi[i].getName().equals(names.getSend())) {
					this.isSend = true;
				} else if(fi[i].getName().equals(names.getReceive())) {
					this.isReceive = true;
				}
			}
		} else {
			this.assignedVariable = var+"("+proc+")";
			if(expr.getTag() == Formula.OVR) {
				AssociativeExpression assExpr = (AssociativeExpression) expr;
				if(assExpr.getChildCount() == 2 && assExpr.getChild(0).toString().equals(var)
						&& assExpr.getChild(1).getTag() == Formula.SETEXT) {
					SetExtension setExpr = (SetExtension) assExpr.getChild(1);
					if(setExpr.getChildCount() == 1 && setExpr.getChild(0).getTag() == Formula.MAPSTO) {
						BinaryExpression mapsTo = (BinaryExpression) setExpr.getChild(0);
						if(mapsTo.getLeft().toString().equals(proc)) {
							this.assignmentExpression = mapsTo.getRight();
						}
					}
				}
			}
		}
		if(this.assignmentExpression == null) {
			// TODO throw warning
			this.assignedVariable = var;
			this.assignmentExpression = expr;
		}
	}
	
	public String getAssignedVariable() {
		return this.assignedVariable;
	}
	
	public Expression getAssignmentExpression() {
		return this.assignmentExpression;
	}
	
	public Boolean isReceive() {
		return this.isReceive;
	}
	
	public Boolean isSend() {
		return this.isSend;
	}
	
	public Boolean isCommunication() {
		return this.isSend || this.isReceive;
	}
}
