package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.MultiplePredicate;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.RelationalPredicate;
import org.rodinp.core.RodinDBException;

import fr.loria.mosel.rodin.eb2da.utils.Constants;

public class DANames {

	private String nodes;
	private String neighbours;
	private String states;
	private String done;
	private String messages;
	private String channelsSet;
	private String send;
	private String receive;
	private String lose;
	private String sent;
	private String received;
	private String inchannel;
	private String readyForReception;
	private String channels;
	private String pc;

	public DANames(DAContext context, DAMachine machine) throws RodinDBException, DAException {
		this.nodes = getIdentifierFromAxiom(context, Constants.NODES, Formula.KPARTITION);
		this.states = getIdentifierFromAxiom(context, Constants.STATES, Formula.KPARTITION);
		this.messages = getIdentifierFromAxiom(context, Constants.MESSAGES, Formula.KPARTITION);
		this.done = getIdentifierFromAxiom(context, Constants.DONE, Formula.IN);
		this.send = getIdentifierFromAxiom(context, Constants.SEND, Formula.IN);
		this.receive = getIdentifierFromAxiom(context, Constants.RECEIVE, Formula.IN);
		this.sent = getIdentifierFromAxiom(context, Constants.SENT, Formula.IN);
		this.received = getIdentifierFromAxiom(context, Constants.RECEIVED, Formula.IN);
		this.inchannel = getIdentifierFromAxiom(context, Constants.INCHANNEL, Formula.IN);
		this.readyForReception = getIdentifierFromAxiom(context, Constants.READY_FOR_RECEPTION, Formula.IN);
		this.channelsSet = getIdentifierFromAxiom(context, Constants.CHANNELS_SET, Formula.EQUAL);
		this.neighbours = getIdentifierFromAxiom(context, Constants.NETWORK_TYPING, Formula.IN);
		this.pc = getIdentifierFromAxiom(machine, Constants.PC, Formula.IN);
		this.channels = getIdentifierFromAxiom(machine, Constants.CHANNELS, Formula.IN);

		// OPTIONAL
		try {
			this.lose = getIdentifierFromAxiom(context, Constants.LOSE, Formula.IN);
		} catch (DAException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	private String getIdentifierFromAxiom(Axiomatizable context, String axiomId, int tag) throws DAException {
		System.out.println("Get ID for IN: " + axiomId);
		String id = "";
		Predicate axiom;
		try {
			axiom = context.getParsedAxiom(axiomId);
		} catch (RodinDBException e) {
			throw new DAException("Rodin Exception for axiom" + axiomId);
		}
		// Predicate should be an inset
		if (axiom != null && axiom.getTag() == tag) {
			Expression expression;

			switch (tag) {
			case Formula.IN:
			case Formula.EQUAL:
				RelationalPredicate rp = (RelationalPredicate) axiom;
				expression = rp.getLeft();
				break;
			case Formula.KPARTITION:
				MultiplePredicate mp = (MultiplePredicate) axiom;
				// The name of the set of nodes is the first member of the partition
				expression = mp.getChild(0);
				break;
			default:
				throw new DAException("Bad tag for axiom " + axiomId);
			}

			if (expression.getTag() == Formula.FREE_IDENT) {
				// && rp.getRight().toString().equals(this.states)) { // done
				// && rp.getRight().getTag() == Formula.TFUN) { // send + sent + received +
				// inchannel + ready4reception + pc
				// && rp.getRight().getTag() == Formula.PFUN) { // receive + lose
				// && rp.getRight().toString().contentEquals(this.channelsSet)) { // channels
				// TODO : verify syntax of right hand side ?
				id = expression.toString();
			} else {
				throw new DAException("No identifier in axiom " + axiomId);
			}
		} else {
			throw new DAException("No (or ill-formed) axiom " + axiomId);
		}
		return id;
	}

//	Predicate neighboursAxiom = context.getParsedPredicateByLabel(Constants.NETWORK_TYPING);
//	setNeighbours(neighboursAxiom);

	public String getChannels() {
		return this.channels;
	}

	public String getChannelsSet() {
		return this.channelsSet;
	}

	public String getDone() {
		return this.done;
	}

	public String getInchannel() {
		return this.inchannel;
	}

	public String getLose() {
		return this.lose;
	}

	public String getMessages() {
		return this.messages;
	}

	public String getNeighbours() {
		return this.neighbours;
	}

	public String getNodes() {
		return this.nodes;
	}

	public String getPc() {
		return this.pc;
	}

	public String getReadyForReception() {
		return this.readyForReception;
	}

	public String getReceive() {
		return this.receive;
	}

	public String getReceived() {
		return this.received;
	}

	public String getSend() {
		return this.send;
	}

	public String getSent() {
		return this.sent;
	}

	public String getStates() {
		return this.states;
	}

	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();
		strb.append("NODES: ").append(this.nodes).append("\n");
		strb.append("pc: ").append(this.pc).append("\n");
		strb.append("channels: ").append(this.channels).append("\n");
		strb.append("CHANNELS: ").append(this.channelsSet).append("\n");
		strb.append("done: ").append(this.done).append("\n");
		strb.append("inchannel: ").append(this.inchannel).append("\n");
		strb.append("lose: ").append(this.lose).append("\n");
		strb.append("MESSAGES: ").append(this.messages).append("\n");
		strb.append("neighbours: ").append(this.neighbours).append("\n");
		strb.append("readyForReception: ").append(this.readyForReception).append("\n");
		strb.append("receive: ").append(this.receive).append("\n");
		strb.append("received : ").append(this.received).append("\n");
		strb.append("send: ").append(this.send).append("\n");
		strb.append("sent: ").append(this.sent).append("\n");
		strb.append("STATES: ").append(this.states);
		return strb.toString();
	}
}
