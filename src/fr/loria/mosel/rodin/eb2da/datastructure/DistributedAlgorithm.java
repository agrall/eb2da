package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.CoreException;
import org.eventb.core.IAxiom;
import org.eventb.core.IMachineRoot;
import org.eventb.core.ast.AssociativeExpression;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.MultiplePredicate;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.RelationalPredicate;
import org.rodinp.core.RodinDBException;

import fr.loria.mosel.rodin.eb2da.tom.PatternMatcher;
import fr.loria.mosel.rodin.eb2da.utils.Constants;

/**
 * @author Alexis Grall
 *
 */
public class DistributedAlgorithm {

	/**
	 * Machine specifying the distributed algorithm
	 */
	private DAMachine machine;
	
	/**
	 * Context seen by machine
	 */
	//private DAContext seenContext;

	/**
	 * Names of all the mandatory elements of the distributed algorithm.
	 */
	private DANames names;
	
	/**
	 * Process classes of the distributed algorithm
	 */
	private List<ProcessClass> processClasses;

	/**
	 * Constants defined in enumerated sets
	 */
	private List<DAEnumSet> enumSets;

	private List<DAMessageConstructor> msgConstructors;
	// TODO : a message constructor is either an element of Messages or 
	//        an injective function with image in Messages
	
	// TODO: set communication properties
	 private Boolean reliable;
	

	/**
	 * @param mch
	 * 		The machine specifying the distributed algorithm to construct.
	 * @throws CoreException
	 * @throws DAException 
	 */
	public DistributedAlgorithm (IMachineRoot mch) throws CoreException, DAException {
		this.machine = new DAMachine(mch);
		DAContext seenContext = new DAContext(mch.getSeesClauses()[0].getSeenContextRoot());
		this.names = new DANames(seenContext, machine);
		this.msgConstructors = new ArrayList<DAMessageConstructor>();
		setMsgConstructors(seenContext);
		this.processClasses = setProcessClasses(seenContext, this.machine);
		setConstants(seenContext,this.processClasses);
		this.enumSets = setEnumSets(seenContext, this.processClasses);		
		// TODO : set reliable
		this.reliable = true;
		System.out.println(this.getConstants());
	}

	/**
	 * Sets the list of DAMessageConstructor representing injective functions used
	 *  to construct messages in the Event-B specification.
	 * @throws RodinDBException
	 * if there was a problem accessing the database
	 * @throws DAException 
	 */
	private void setMsgConstructors(DAContext seenContext) throws RodinDBException, DAException {
		Predicate msgPartition = seenContext.getParsedAxiom(Constants.MESSAGES);
		if(msgPartition.getTag() == Formula.KPARTITION) {
			MultiplePredicate mp = (MultiplePredicate) msgPartition;
			for(Expression expr: Arrays.copyOfRange(mp.getChildren(), 1, mp.getChildren().length)) {
				this.msgConstructors.add(new DAMessageConstructor(expr, seenContext));
			}
		} else {
			// TODO Axiom with label Constants.MESSAGES should be a partition.
		}
	}
	
	/** 
	 * Sets the identifier of the set of all processes and sets the process classes with their names.
	 * @throws RodinDBException
	 * 		if there was a problem accessing the database
	 * @throws DAException 
	 * 		if LB constraints on the definition of process classes are not respected
	 */
	private List<ProcessClass> setProcessClasses(DAContext seenContext,DAMachine machine) throws RodinDBException, DAException{
		List<ProcessClass> pcs = new ArrayList<ProcessClass>();
		
		Predicate pred = seenContext.getParsedAxiom(Constants.NODES);
		if(pred == null) {
            throw new DAException("No process classes");
		}
		// Predicate should be a partition: partition(Nodes,PC1,PC2,...)
		if(pred.getTag() == Formula.KPARTITION){
			MultiplePredicate mp = (MultiplePredicate) pred;
			// There should be at least two arguments (ie one process class)
			if(mp.getChildCount() > 1) {
				// for each argument after the first
				for(int index = 1; index<mp.getChildCount(); index++) {
					if(mp.getChild(index).getTag() == Formula.FREE_IDENT) {
						String processName = mp.getChild(index).toString();
						ProcessClass pc = new ProcessClass(processName, this, machine);
						Predicate predProcess = seenContext.getParsedAxiom(processName);
						if(predProcess != null) { 
							pc.setProcesses(PatternMatcher.getEnumerationElements(predProcess, processName));
						}
						pcs.add(pc);
					} else {
			            throw new DAException("Process class declaration is not an identifier");
					}
				}
			} else {
	            throw new DAException("No process classes defined");
			}
		} else {
            throw new DAException("Process classes not defined as a partition");
		}
		return pcs;
	}


	private List<DAEnumSet> setEnumSets(DAContext seenContext, List<ProcessClass> processClasses) throws RodinDBException, DAException {
		List<DAEnumSet> enums = new ArrayList<DAEnumSet>();
		List<String> pclNames = new ArrayList<>();
		for(ProcessClass pc: processClasses) {
			pclNames.add(pc.getName());
		}
		for(IAxiom axiom : seenContext.getAxioms()) {
			Predicate pred = seenContext.getParser().parsePredicate(axiom.getPredicateString());
			if(pred.getTag() == Formula.KPARTITION
					&& !axiom.getLabel().equals(Constants.STATES)
					&& !axiom.getLabel().equals(Constants.NODES)
					&& !axiom.getLabel().equals(Constants.MESSAGES)
					&& !(pclNames.contains(axiom.getLabel()))) {
				// the name of the set is the name of the axiom
				String name = axiom.getLabel().substring(0, axiom.getLabel().length() - Constants.ENUM_SET_SUFFIX.length());
				// TODO: check that the name of the set and of the axiom are the same 
				// get the elements of the set
				List<String> elements = new ArrayList<String>(PatternMatcher.getEnumerationElements(pred,name));
				// get the locality (by default local to all PCs)
				List<ProcessClass> locality = processClasses;
				if(axiom.hasComment() && axiom.getComment().startsWith("@")) {
					locality = this.getLocalityFromComment(axiom.getComment(), processClasses);
				} 
				DAEnumSet es = new DAEnumSet(name, elements, locality);
				enums.add(es);
				System.out.println("Add enum: "+es);
			}
		}
		return enums;
	}

	private List<ProcessClass> getLocalityFromComment(String comment, List<ProcessClass> processClasses){
		// if comment we suppose it is of the form @P@Q...
		// TODO: check if different format 
		String[] loc = comment.split("@");
		// ignore the first element in loc which is the empty string if well-formed comment
		List<String> names = Arrays.stream(loc).skip(1).collect(Collectors.toList());
		return this.getProcessClassesFromStrings(names, processClasses);
	}

	private List<ProcessClass> getProcessClassesFromStrings(List<String> names, List<ProcessClass> processClasses){
		List<ProcessClass> pcs = new ArrayList<>();
		for(String pid:names) {
			// check if the id is Nodes
			if(pid.equals(Constants.NODES)) {
				return processClasses;	
			}
			// check if the id is a PC id
			for(ProcessClass pc : processClasses) {
				if(pc.getName().equals(pid)) {
					pcs.add(pc);
				}
			}
		}
		return pcs;
	}

	
	private void setConstants(DAContext seenContext, List<ProcessClass> processClasses) throws RodinDBException, DAException {
		for(IAxiom axiom : seenContext.getAxioms()) {
			if(axiom.getLabel().endsWith(Constants.CONSTANT_TYPING_SUFFIX) 
					|| axiom.getLabel().equals(Constants.NETWORK_TYPING)) {
				// get name of the constant
				// TODO: uniform treatement for neighbours 
				String cstName = "";
				if(axiom.getLabel().contentEquals(Constants.NETWORK_TYPING)) {
					cstName = this.getNames().getNeighbours();
				} else {
					cstName = axiom.getLabel().substring(0, axiom.getLabel().length() - Constants.CONSTANT_TYPING_SUFFIX.length());
				}
				// check if the constant is a msg constructor or not
				Boolean msgCstor = false;
				for(DAMessageConstructor mc : this.getMsgConstructors()) {
					if(mc.getName().equals(cstName)) {
						msgCstor = true;
						break;
					}
				}
				// if the constant is a message constructor, skip it
				if(!msgCstor) {
					// get locality (by default local to all PCs) and type of the constant
					List<ProcessClass> locality = processClasses;
					Expression cstType = null;
					Boolean cstIsFunction = false;
					Predicate typingPredicate = seenContext.parsePredicate(axiom.getPredicateString());
					Expression cstValue = null;
					// typing axiom is of the form left \in right
					if(typingPredicate.getTag() == Formula.IN) {
						RelationalPredicate rp = (RelationalPredicate) typingPredicate;
						// left hand side should be the same constant identifier as the one given in the label
						// or it can also be the neighbours identifier if the label is the network typing label
						List<ProcessClass> pcs = new ArrayList<>(); // PCs 4 locality
						if(cstName.equals(rp.getLeft().toString())) {
							// the type of the constant
							cstType = rp.getRight();
							if(rp.getRight().getTag() == Formula.TFUN) {
								BinaryExpression be = (BinaryExpression) rp.getRight();
								// TODO enable pcl1 \bunion pcl2 \bunion ... \bunion pclk \tfun T
								List<String> names = new ArrayList<>();
								names.add(be.getLeft().toString());
								pcs = this.getProcessClassesFromStrings(names, processClasses);
								if(!pcs.isEmpty()) {
									// if the domain corresponds to a process class assign it
									locality = pcs;
								}
								cstIsFunction = true;
							}
							// locality not defined by the domain of the function and
							// axiom is commented by @pcl1@pcl2@...
							// TODO: check that it shouldn't be isFunction
							if(pcs.isEmpty() && axiom.hasComment() && axiom.getComment().startsWith("@")) {
								locality = this.getLocalityFromComment(axiom.getComment(), processClasses);
								cstIsFunction = false;
							}
							// if no PCs in domain of function and no comment than locality = all PCs

							// TODO: change the defaut: local to all 4 local to none?
							// TODO: continue if the constant is local, else continue the for loop.
							// look for a value axiom
							Predicate valuePredicate;
							if(cstName.equals(this.getNames().getNeighbours())) {
								valuePredicate = seenContext.getParsedAxiom(Constants.NETWOK_VALUE);
							} else {
								valuePredicate = seenContext.getParsedAxiom(cstName+Constants.CONSTANT_VALUE_SUFFIX);
							}
							// valuePredicate should be an equality if it exists
							if(valuePredicate != null) {
								if(valuePredicate.getTag() == Formula.EQUAL) {
									RelationalPredicate valueRP = (RelationalPredicate) valuePredicate;
									if(valueRP.getLeft().toString().equals(cstName)) {
										cstValue = valueRP.getRight();
									} else {
										throw new DAException("Value axiom for constant "+ cstName + " does not define "+ cstName);
									}
								} else {
									throw new DAException("Value axiom for constant "+ cstName + " is not of the form cst = value");
								}
							}						
						} else {
							throw new DAException("Typing axiom for constant "+ cstName + " does not define "+ cstName);
						}
					} else {
						throw new DAException("Typing axiom for constant "+ cstName + " is not of the form cst \\in T");
					}

					// add the constant to the PCsin the locality
					for(ProcessClass pc:locality) {
						Expression localValue = cstValue;
						// set the specific value for the pc if defined as a union
						if(cstIsFunction && cstValue != null) {
							if(cstValue.getTag() == Formula.BUNION) {
								AssociativeExpression ae = (AssociativeExpression) cstValue;
								//List<Expression> aeChildren = Arrays.asList(ae.getChildren());
								for(Expression child : Arrays.asList(ae.getChildren())) {
									if(PatternMatcher.isProcessFunction(child, pc.getName())) {
										localValue = child;
										break;
									}
								}
							}
						}
						DAConstant lcst = new DAConstant(cstName, cstType, localValue, cstIsFunction, pc);
						pc.addConstant(lcst);
					}	
				}
			}
		}
	}

	public DAMachine getMachine() {
		return this.machine;
	}


	/**
	 * @return a joint list of the local variables of all process classes of this distributed algorithm.
	 *  Two variables of the list can have the same name,
	 *  but if this is the case, these two variables will not have the same locality.
	 */
	public List<DAVariable> getVariables() {
		List<DAVariable> variables = new ArrayList<DAVariable>();
		for(ProcessClass pcl : this.processClasses) {
			variables.addAll(pcl.getVariables());
		}
		return variables;
	}

	/**
	 * @return a joint list of the local constants of all process classes of this distributed algorithm.
	 *  Two constants of the list can have the same name,
	 *  but if this is the case, these two constants will not have the same locality.
	 */
	public List<DAConstant> getConstants() {
		return this.processClasses.stream().flatMap(pcl -> pcl.getConstants().stream()).collect(Collectors.toList());
	}
	
	/**
	 * @return a joint list of all the names of the local constants of the process classes of this distributed algorithm.
	 *  Any two elements of the returned list are distinct.
	 */
	public Set<String> getConstantNames() {
		return this.getConstants().stream().map(DAConstant::getName).collect(Collectors.toSet());
	}
	
	/**
	 * @param constant
	 * 	The name of the constant to look for
	 * @return A list of all local DAConstant of the processes of this distributed algorithm
	 * 	with name equal to parameter constant.
	 *  Returns an empty list if no local constant has this name.
	 */
	public List<DAConstant> getLocalConstantsWithName(String constant) {
		return this.getConstants().stream().filter(cst -> cst.getName().equals(constant)).collect(Collectors.toList());
	}
	
	/**
	 * @return a shallow copy of the list of process classes
	 */
	public List<ProcessClass> getProcessClasses() {
		return new ArrayList<ProcessClass>(this.processClasses);
	}
	
	public DANames getNames() {
		return this.names;
	}
	
	/**
	 * @return a shallow copy of the list of enumerated sets
	 */
	public List<DAEnumSet> getEnumSets() {
		return new ArrayList<DAEnumSet>(this.enumSets);
	}
	
	/**
	 * @return a shallow copy of the list of message constructors
	 */
	public List<DAMessageConstructor> getMsgConstructors() {
		return new ArrayList<DAMessageConstructor>(this.msgConstructors);
	}

	/**
	 * @return whether the communication channels are reliable or not for this distributed algorithm
	 */
	public Boolean isReliable() {
		return reliable;
	}
}