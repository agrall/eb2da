package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.RelationalPredicate;
import org.eventb.core.ast.SetExtension;
import org.eventb.core.ast.UnaryExpression;
import org.rodinp.core.RodinDBException;

import fr.loria.mosel.rodin.eb2da.utils.Constants;

public class DAMessageConstructor {

	private String name;
	private Boolean isFunction;
	private Expression domain;
	
	public DAMessageConstructor(Expression expr, DAContext context) throws RodinDBException, DAException {
		this.name = "";
		this.isFunction = false;
		// constant m of type message : {m} in the partition
		if(expr.getTag() == Formula.SETEXT) {
			this.isFunction = false;
			SetExtension setExt = (SetExtension) expr;
			if(setExt.getChildCount() == 1) {
				this.name = setExt.getChild(0).toString();
			}
		}
		// mapping function f from a type to MESSAGES : ran(f) in the partition
		else if(expr.getTag() == Formula.KRAN) {
			this.isFunction = true;
			UnaryExpression ranExpr = (UnaryExpression) expr;
			this.name = ranExpr.getChild().toString();
			Predicate type = context.getParsedAxiom(this.name + Constants.CONSTANT_TYPING_SUFFIX);
			if(type != null && type.getTag() == Formula.IN) {
				RelationalPredicate relPred = (RelationalPredicate) type;
				if(relPred.getLeft().toString().equals(this.name)
						&& relPred.getRight().getTag() == Formula.TINJ) {
					BinaryExpression binExpr = (BinaryExpression) relPred.getRight();
					this.domain = binExpr.getLeft();
				}
				else {
					String errMsg = "Ill-formed typing axiom for mapping function " + this.name;
					errMsg += ". Should be of the form : ";
					// \u2208 is IN, \u21a3 is TINJ
					errMsg += this.name + " \u2208 <type> \u21a3 Messages" ;
					throw new DAException(errMsg);
				}
			} else {
				String errMsg = "No typing axiom found for mapping function " + this.name;
				errMsg += " or ill-formed typing axiom";
				throw new DAException(errMsg);
			}
		}
	}

	public String getName() {
		return this.name;
	}
	
	public Boolean isFunction() {
		return this.isFunction;
	}
	
	public Expression getDomain() {
		if(this.isFunction) {
			return this.domain;
		} else {
			// TODO throw error
		}
		return null;
	}
}
