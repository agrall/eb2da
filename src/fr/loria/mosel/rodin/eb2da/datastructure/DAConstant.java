package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.List;
import java.util.stream.Collectors;

import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;

public class DAConstant {
	
	private String name;
	
	private Expression type;
	
	private Expression value;
	
	private Boolean isFunction;
	
	private ProcessClass local2pc;

	public String getLocality() {
		return local2pc.getName();
	}

	/**
	 * @param cstName
	 * 	the name of the constant
	 * @param cstType
	 * 	the type of the constant
	 * @param cstValue
	 *  the value of the constant
	 * @param cstIsFunction
	 *  true if the constant is a function on processClass
	 */
	public DAConstant(String cstName, Expression cstType, Expression cstValue, Boolean cstIsFunction) {
		this.name = cstName;
		this.type = cstType;
		this.value = cstValue;
		this.isFunction = cstIsFunction;
	}
	
	/**
	 * @param cstName
	 * 	the name of the constant
	 * @param cstType
	 * 	the type of the constant
	 * @param cstValue
	 *  the value of the constant in the PC
	 * @param globalValue
	 *  the value of the constant in all PCs
	 * @param cstIsFunction
	 *  true if the constant is a function on processClass
	 * @param local2pc
	 * 	the Process Class to which the constant is assigned
	 * @param locality
	 * 	the Process Classes to which the constant is local
	 */
	public DAConstant(String cstName, Expression cstType, Expression cstValue, 
			Boolean cstIsFunction, ProcessClass local2pc) {
		this.name = cstName;
		this.type = cstType;
		this.value = cstValue;
		this.isFunction = cstIsFunction;
		this.local2pc = local2pc;
	}

	public String getName() {
		return name;
	}

	public Expression getType() {
		return type;
	}
	
	public Expression getLocalType() {
		if(this.isFunction) {
			if(this.type.getTag() == Formula.TFUN) {
				BinaryExpression be = (BinaryExpression) this.type;
				return be.getRight();
			}
		}
		return this.type;
	}

	public Expression getValue() {
		return value;
	}

	public Boolean isFunction() {
		return isFunction;
	}

	@Override
	public String toString() {
		return "DAConstant[" + name + "=" + value + " in "+local2pc.getName() + ")]";
	}
		
}
