package fr.loria.mosel.rodin.eb2da.datastructure;

public class DAException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DAException(String message) {
		super(message);
	}

	public String getMessage() {
		return "DA exception: " + super.getMessage();
	}
}
