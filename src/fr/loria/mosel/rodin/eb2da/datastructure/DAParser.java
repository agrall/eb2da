package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.Assignment;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.FormulaFactory;
import org.eventb.core.ast.IParseResult;
import org.eventb.core.ast.ITypeEnvironment;
import org.eventb.core.ast.Predicate;

public class DAParser {

	private FormulaFactory ff;
	
	/**
	 * Environment for formulas
	 */
	private ITypeEnvironment env;
	
	public DAParser(FormulaFactory ff, ITypeEnvironment env) {
		this.ff = ff;
		this.env = env;
	}
	
	/**
	 * @param formula
	 * @return a Predicate corresponding to formula,
	 * 	 	parsed by the formula factory ff and type checked in the environment env
	 */
	public Predicate parsePredicate(String formula) {
		IParseResult res = ff.parsePredicate(formula, null);
		Predicate pred = res.getParsedPredicate();
		pred.typeCheck(env);
		pred.getSyntaxTree(); //required for instantiate quantifier (i.e., rewrite the boundIdentifier to boundIdDecl)
		return pred;
	}
	
	/**
	 * @param formula
	 * @return an Assignment corresponding to formula,
	 * 	 	parsed by the formula factory ff and type checked in the environment env
	 */
	public Assignment parseAssignment(String formula) {
		IParseResult res = ff.parseAssignment(formula, null);
		Assignment assign= res.getParsedAssignment();
		assign.typeCheck(env);
		assign.getSyntaxTree(); //required for instantiate quantifier (i.e., rewrite the boundIdentifier to boundIdDecl)
		return assign;
	}
	
	/**
	 * @param formula
	 * @return an Expression corresponding to formula,
	 * 	 	parsed by the formula factory ff and type checked in the environment env
	 */
	public Expression parseExpression(String formula) {
		IParseResult res = ff.parseExpression(formula, null);
		Expression expr = res.getParsedExpression();
		expr.typeCheck(env);
		expr.getSyntaxTree(); //required for instantiate quantifier (i.e., rewrite the boundIdentifier to boundIdDecl)
		return expr;
	}
}
