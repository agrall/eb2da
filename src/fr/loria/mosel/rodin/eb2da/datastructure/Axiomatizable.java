package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.Predicate;
import org.rodinp.core.RodinDBException;

public interface Axiomatizable {
	public Predicate getParsedAxiom(String label) throws RodinDBException;
}
