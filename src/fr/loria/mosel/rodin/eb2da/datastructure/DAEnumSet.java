package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.List;
import java.util.stream.Collectors;

public class DAEnumSet {
	
	private String name;
	private List<ProcessClass> locality;
	private List<String> elements;

	public DAEnumSet(String name, List<String> elements, List<ProcessClass> locality) {
		this.name = name;
		this.elements = elements;
		this.locality = locality;
	}

	public String getName() {
		return this.name;
	}
	
	public List<String> getElements() {
		return this.elements.stream() 
                .collect(Collectors.toList());
	}

	public Boolean isGlobal() {
		return locality.isEmpty();
	}

	public Boolean isLocal(String pcl) {
		for(ProcessClass pc : locality) {
			if(pc.getName().equals(pcl)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "DAEnumSet [name=" + name + ", locality=" + locality + ", elements=" + elements + "]";
	}
		
}
