package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.ArrayList;
import java.util.List;

import org.eventb.core.IAction;
import org.eventb.core.IEvent;
import org.eventb.core.IGuard;
import org.eventb.core.IParameter;
import org.eventb.core.ast.Assignment;
import org.eventb.core.ast.BecomesEqualTo;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.RelationalPredicate;
import org.rodinp.core.RodinDBException;



// TODO clean not used attributes and methods like acts, setReceive, setSend etc.
public class DAEvent {
	
	private String name;
	
	private IEvent event;
	
	/**
	 * process class of localProc
	 */
	private ProcessClass procClass;
	
	/**
	 * list of the parameters except localProc
	 */
	private List<DAConstant> parameters;
	
	/**
	 * list of the guards except for the locality guard (and the state guard?)
	 */
	private List<Predicate> guards;

	private List<DAAction> actions;
	
	/**
	 * process for which the event is observed, passed as a parameter
	 */
	private String localProc;

	/**
	 * state of localProc in which the event is observed
	 */
	private String state;
	
	private Boolean isReceiveEvent;
	
	private Boolean isSendEvent;

	/**
	 * @param evt
	 * @param pcls
	 * @throws RodinDBException
	 */
	public DAEvent(ProcessClass pcl, IEvent evt)
			throws RodinDBException {
		this.name = evt.getLabel();
		this.event = evt;
		this.procClass = pcl;
		this.localProc = "";
		this.state = "";
		this.parameters = new ArrayList<DAConstant>();
		this.guards = new ArrayList<Predicate>();
		this.actions = new ArrayList<DAAction>();
		this.isReceiveEvent = false;
		this.isSendEvent = false;
		
		setLocalProc();
		setState();
		setParameters();
		setGuards();
		setActions();
		setIsReceiveOrSend();
		// TODO setup isReceive and isSend
		//setReceive();
		//setSend();
		/*
		if(isReceiveEvent || isSendEvent) {
			removeCommunicationAction();
		}
		*/
	}
	
	
	
	private void setIsReceiveOrSend() {
		for(DAAction act : this.actions) {
			if(act.isReceive()) {
				this.isReceiveEvent = true;
			} else if(act.isSend()) {
				this.isSendEvent = true;
			}
		}
	}

	public ProcessClass getProcClass() {
		return procClass;
	}

	public String getLocalProc() {
		return localProc;
	}

	/**
	 * @return
	 * 	a shallow copy of the list of the actions of the event as a DAAction list
	 */
	public ArrayList<DAAction> getActions() {
		return new ArrayList<DAAction>(this.actions);
	}
	
	public IEvent getEvent() {
		return this.event;
	}
	
	/**
	 * @return
	 * 	a shallow copy of the list of the guards of the event as a Predicate list
	 */
	public ArrayList<Predicate> getGuards() {
		return new ArrayList<Predicate>(this.guards);
	}
	
	public String getLocalProcess() {
		return this.localProc;
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return
	 * 	a shallow copy of the list of the parameters of the event as a DAConstant list
	 */
	public List<DAConstant> getParameters() {
		return new ArrayList<DAConstant>(this.parameters);
	}
	
	public ProcessClass getProcessClass() {
		return this.procClass;
	}
	
	public String getState() {
		return this.state;
	}

	public Boolean isInternalEvent() {
		return !this.isReceiveEvent && !this.isSendEvent;
	}
	
	/**
	 * @param guard
	 * 		the guard to check
	 * @return whether the given guard is the locality guard of the event.
	 * @throws RodinDBException
	 * 	if there was a problem accessing the database.
	 */
	private Boolean isLocalityGuard(Predicate guard) throws RodinDBException {
		if(guard.getTag() == Formula.IN) {
			RelationalPredicate rp = (RelationalPredicate) guard;
			return (rp.getRight().getTag() == Formula.FREE_IDENT
					&& rp.getRight().toString().equals(this.procClass.getName()));
		}
		return false;
	}

	/**
	 * @return whether pcl is equal to this.procClass
	 */
	public Boolean isLocalTo(ProcessClass pcl) {
		return pcl.getName().equals(this.procClass.getName());
	}
	
	/**
	 * @return whether param is equal to this.localProc
	 */
	public Boolean isLocalTo(String param) {
		return param.equals(this.localProc);
	}
	
	public Boolean isReceiveEvent() {
		return this.isReceiveEvent;
	}

	public Boolean isSendEvent() {
		return this.isSendEvent;
	}
	
	private Boolean isStateGuard(Predicate grd) {
		String lmember = String.format("%s(%s)",
				this.procClass.getDistributedAlgorithm().getNames().getPc(),
				this.localProc);
		if(grd.getTag() == Formula.EQUAL) {
			RelationalPredicate rp = (RelationalPredicate) grd;
			return (rp.getLeft().toString().equals(lmember) && rp.getRight().getTag() == Formula.FREE_IDENT);
		}
		return false;
	}
	
	/**
	 * @param guard
	 * 		the guard to check
	 * @return
	 * 		whether the given guard is a typing guard for one of the parameters of the event
	 */
	private Boolean isTypingGuard(Predicate guard) {
		return this.parameters.stream().anyMatch(p -> isTypingGuard(guard, p));
	}
	
	/**
	 * @param guard
	 * 		the guard to check
	 * @param parameter
	 * 		the parameter for which the guard may be the typing guard
	 * @return whether the given guard is the typing guard of the given parameter
	 */
	private Boolean isTypingGuard(Predicate guard, DAConstant parameter) {
		if(guard.getTag() == Formula.IN) {
			RelationalPredicate rp = (RelationalPredicate) guard;
			return parameter.getName().equals(rp.getLeft().toString())
					&& parameter.getType().toString().equals(rp.getRight().toString());
		}
		return false;
	}
	
	/**
	 * @throws RodinDBException
	 * 	if there was a problem accessing the database
	 */
	private void setActions() throws RodinDBException {
		DANames names = this.procClass.getDistributedAlgorithm().getNames();
		DAParser parser = this.procClass.getDistributedAlgorithm().getMachine().getParser();
		for(IAction action : this.event.getActions()) {
			Assignment assign = parser.parseAssignment(action.getAssignmentString());
			if(assign.getTag() == Formula.BECOMES_EQUAL_TO) {
				BecomesEqualTo beq = (BecomesEqualTo) assign;
				FreeIdentifier[] fis = beq.getAssignedIdentifiers();
				Expression[] exprs = beq.getExpressions();
				if(fis.length == 1 && exprs.length == 1) {
					this.actions.add(new DAAction(fis[0].getName(), exprs[0], names, this.localProc));
				}
			}
		}
		//this.actions = PatternMatcher.getActions(this.acts, names);
		
	}
	
	/**
	 * Add the guards of the events in the list of guards as predicates
	 * except for the locality guard "'localProc' \in 'procClass'", the state guard "'pc'('localProc') = 'state'
	 * and the typing guards of the parameters.
	 * @throws RodinDBException
	 */
	private void setGuards() throws RodinDBException {
		DAParser parser = this.procClass.getDistributedAlgorithm().getMachine().getParser();
		for(IGuard guard : this.event.getGuards()) {
			Predicate content = parser.parsePredicate(guard.getPredicateString());
			if(!isLocalityGuard(content) && !isStateGuard(content) && !isTypingGuard(content)) {
				guards.add(content);
			}
		}
	}
	
	/**
	 * This method first find the locality guard by looking for the identifier of the local process class.
	 * It then sets the localProc accordingly.
	 * @throws RodinDBException 
	 * 		if there was a problem accessing the database.
	 */		
	private void setLocalProc() throws RodinDBException {
		// get parser for the machine
		DAParser parser = this.procClass.getDistributedAlgorithm().getMachine().getParser();
		// look for the locality guard
		for(IGuard grd : this.event.getGuards()) {
			Predicate content = parser.parsePredicate(grd.getPredicateString());
			// check if the current guard is the locality guard
			if(isLocalityGuard(content)) {
				RelationalPredicate rp = (RelationalPredicate) content;
				// check if left member is a parameter
				Boolean isParam = false;
				for(IParameter param : this.event.getParameters()) {
					if(rp.getLeft().toString().equals(param.getIdentifierString())) {
						isParam = true;
						this.localProc = rp.getLeft().toString();
						break;
					}
				}
				if(isParam) {
					break;
				} else {
					// TODO throw warning : local process of event this.event should be a parameter (got rp.getLeft().toString())
				}
				break;
			}
		}
		if(this.localProc.contentEquals("")) {
			// TODO throw warning : event this.getName() does not have a local process.
		}
	}
	
	private void setParameters() throws RodinDBException {
		DAParser parser = this.procClass.getDistributedAlgorithm().getMachine().getParser();
		// add parameters to the list of parameters
		for(IParameter param : this.event.getParameters()) {
			String id = param.getIdentifierString();
			// localProc and state are not added if they are parameters
			if(!id.equals(this.localProc)
					&& !id.equals(this.state)) {
				Expression paramType = null;
				// search for the typing guard of parameter id
				for(IGuard guard : this.event.getGuards()) {
					Predicate content = parser.parsePredicate(guard.getPredicateString());
					if(content.getTag() == Formula.IN) {
						RelationalPredicate rp = (RelationalPredicate) content;
						// stop at first guard giving the type of the parameter
						if(rp.getLeft().toString().equals(id)) {
							paramType = rp.getRight();
							break;
						}
					}
				}
				if(paramType != null) {
					this.parameters.add(new DAConstant(id, paramType, null, false));
				} else {
					// TODO throw error : parameter param.getName() does not have a typing guard.
				}
			}
		}
	}
	
	/**
	 * This method find the state in which the event is observed
	 * 	by looking for a guard of the form <Constants.PC>(<this.localProc>) = st.
	 * 	The attribute state is then set up to st.
	 * @throws RodinDBException 
	 * 	if there was a problem accessing the database.
	 */
	private void setState() throws RodinDBException {
		DAParser parser = this.procClass.getDistributedAlgorithm().getMachine().getParser();
		for(IGuard grd : this.event.getGuards()) {
			Predicate content = parser.parsePredicate(grd.getPredicateString());
			if(isStateGuard(content)) {
				RelationalPredicate rp = (RelationalPredicate) content;
				this.state = rp.getRight().toString();
			}
		}
		if(this.state.equals("")) {
			// TODO throw warning : event this.getName() does not have a local state.
		}
	}
}
