package fr.loria.mosel.rodin.eb2da.datastructure;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eventb.core.IAction;
import org.eventb.core.IEvent;
import org.eventb.core.IGuard;
import org.eventb.core.ast.Assignment;
import org.eventb.core.ast.AssociativeExpression;
import org.eventb.core.ast.BecomesEqualTo;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.QuantifiedExpression;
import org.eventb.core.ast.RelationalPredicate;
import org.rodinp.core.RodinDBException;

import fr.loria.mosel.rodin.eb2da.utils.Constants;

public class ProcessClass {
	private String name;
	private DistributedAlgorithm dAlgo;
	private List<String> processes;
	private List<DAConstant> constants;
	private List<DAVariable> variables;
	private List<DAEvent> events;
	// TODO : getter for receive events, send events, internal events.
	
	public ProcessClass(String pclName, DistributedAlgorithm distributedAlgorithm, DAMachine machine) throws RodinDBException {
		this.name = pclName;
		this.dAlgo = distributedAlgorithm;
		this.processes = new ArrayList<String>();
		this.constants = new ArrayList<DAConstant>();
		this.variables = new ArrayList<DAVariable>();
		setVariables(machine);
		this.events = new ArrayList<DAEvent>();
		setEvents(machine);
	}

	public void setProcesses(List<String> processes) throws RodinDBException {
		this.processes = processes;
	}
	
	public void addConstant(DAConstant cst) {
		this.constants.add(cst);
	}
	
	private void setEvents(DAMachine machine) throws RodinDBException {
		// get parser from the machine
		DAParser parser = machine.getParser();
		// look for local events
		for(IEvent evt : machine.getEvents()) {
			// look in guards if there is one of the form "p \in" + this.name
			for(IGuard grd : evt.getGuards()) {
				// get parsed predicate of the guard
				Predicate grdPred = parser.parsePredicate(grd.getPredicateString());
				// check if the predicate is of the form left \in right
				if(grdPred.getTag() == Formula.IN) {
					RelationalPredicate grdRP = (RelationalPredicate) grdPred;
					// check if the right hand side is this process class name
					if(grdRP.getRight().toString().equals(this.name)) {
						// construct the event and add it to the list of events
						this.events.add(new DAEvent(this, evt));
						break;
					}
				}
			}
		}
	}

	private void setVariables(DAMachine machine) throws RodinDBException {
		// get the variables of the machine
		List<String> vars = machine.getVariables();
		String channelsName = this.dAlgo.getNames().getChannels();
		String pcName = this.dAlgo.getNames().getPc();
		String nodesName = this.dAlgo.getNames().getNodes();
		// loop on each variable and decide if we add it to the list of local variables or not
		for(String var : vars) {
			// we don't add the variable channels
			if(!var.equals(channelsName)) {
				Predicate typing;
				// get the typing invariant by its name
				// name of the typing invariant differs if the variable is the pc variable
				// or an algorithm specific variable
				if(var.equals(pcName)) {
					typing = machine.getParsedAxiom(Constants.PC);
				} else {
					typing = machine.getParsedAxiom(var + Constants.VARIABLE_TYPING_SUFFIX);
				}
				// Check if the typing invariant has the right shape.
				// var \in Nodes \fun T
				// or var \in P1 \cup ... \cup Pk \fun T
				// where \fun is either \tfun or \pfun
				// first check if it is of the form e1 \in e2
				if (typing != null && typing.getTag() == Formula.IN) {
					RelationalPredicate rp = (RelationalPredicate) typing;
					// then check if e1 is vars and e2 is of the form dom \fun ran
					if (rp.getLeft().toString().equals(var) && 
							(rp.getRight().getTag() == Formula.TFUN || rp.getRight().getTag() == Formula.PFUN)) {
						BinaryExpression be = (BinaryExpression) rp.getRight();
						Boolean local = false;
						// if dom is either Nodes or this process class, then the variable is local
						if (be.getLeft().toString().equals(nodesName) || be.getLeft().toString().equals(this.name)) {
							local = true;
						}
						// if dom is an union of process class, we need to look for this process class in the union
						else if(be.getLeft().getTag() == Formula.BUNION) {
							AssociativeExpression ae = (AssociativeExpression) be.getLeft();
							for(Expression expr : ae.getChildren()) {
								if(expr.toString().equals(this.name)) {
									local = true;
									break;
								}
							}
						}
						if(local) {
							Expression type = be.getRight();
							Expression initValue = null;
							// get initial value
							for(IEvent evt : machine.getEvents()) {
								if(evt.getLabel().equals(Constants.INITIALISATION)) {
									for(IAction act : evt.getActions()) {
										Assignment assign = machine.getParser().parseAssignment(act.getAssignmentString());
										if(assign.getTag() == Formula.BECOMES_EQUAL_TO) { 
											BecomesEqualTo beq = (BecomesEqualTo) assign;
											if(beq.getAssignedIdentifiers().length == 1 
													&& beq.getAssignedIdentifiers()[0].getName().equals(var)) {
												Expression assignmentExpression = beq.getExpressions()[0];
												if(assignmentExpression.getTag() == Formula.BUNION) {
													AssociativeExpression union = (AssociativeExpression) assignmentExpression;
													for(Expression setExpr : union.getChildren()) {
														if(setExpr.getTag() == Formula.CSET) {
															QuantifiedExpression cset = (QuantifiedExpression) setExpr;
															if(cset.getBoundIdentDecls().length == 1 
																	&& cset.getPredicate().getTag() == Formula.IN) {
																RelationalPredicate inPred = (RelationalPredicate) cset.getPredicate();
																if(inPred.getRight().toString().equals(this.name)
																		|| inPred.getRight().toString().equals(nodesName)) {
																	initValue = setExpr;
																	break;
																}
															}
														}
													}
												} else if(assignmentExpression.getTag() == Formula.CSET) {
													initValue = assignmentExpression;
												}
												break;
											}
										}
									}
									break;
								}
							}
							// add the variable to the list of variables
							if(initValue != null) {
								this.variables.add(new DAVariable(var, type, initValue));
							} else {
								// TODO initialisation of var does not have the expected form
							}
						}
					} else {
						// TODO throw error
					}
				} else {
					// TODO throw error
				}
			}
		}
		// TODO finish
	}
	
	public List<String> getProcesses() {
		return new ArrayList<String>(this.processes);
	}

	public String getName() {
		return name;
	}

	public List<DAVariable> getVariables() {
		return new ArrayList<DAVariable>(this.variables);
	}

	public List<DAConstant> getConstants() {
		return new ArrayList<DAConstant>(this.constants);
	}

	public DistributedAlgorithm getDistributedAlgorithm() {
		return this.dAlgo;
	}

	public List<DAEvent> getEvents() {
		return new ArrayList<DAEvent>(this.events);
	}

	public List<String> getStates() {
		Set<String> res = new HashSet<String>();
		for(DAEvent e : this.getEvents()) {
			res.add(e.getState());
		}
		return new ArrayList<String>(res);
	}

	public List<DAEvent> getEvents(String st) {
		List<DAEvent> res = new ArrayList<DAEvent>();
		for(DAEvent evt : this.getEvents()) {
			if(evt.getState().equals(st)) {
				res.add(evt);
			}
		}
		return res;
	}
	
	public boolean isReceive(String st) {
		for(DAEvent evt : this.getEvents(st)) {
			if(evt.isReceiveEvent()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "ProcessClass [name=" + name +  "]";
	}

  
}
