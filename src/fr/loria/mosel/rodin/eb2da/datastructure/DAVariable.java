package fr.loria.mosel.rodin.eb2da.datastructure;

import org.eventb.core.ast.Expression;

public class DAVariable {

	private String name;
	
	private Expression type;
	
	private Expression initialValue;

	public DAVariable(String name, Expression type, Expression initValue) {
		this.name = name;
		this.type = type;
		this.initialValue = initValue;
	}
	
	public String getName() {
		return this.name;
	}

	public Expression getInitialValue() {
		return this.initialValue;
	}

	public Expression getType() {
		return this.type;
	}
	
}
