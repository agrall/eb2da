package fr.loria.mosel.rodin.eb2da.tom;

import fr.loria.mosel.rodin.eb2da.datastructure.DAAction;
import fr.loria.mosel.rodin.eb2da.datastructure.DANames;
import fr.loria.mosel.rodin.eb2da.datastructure.DAException;
import org.eventb.core.ast.BoundIdentDecl;
import fr.loria.mosel.rodin.eb2da.utils.Pair;
import java.util.List;
import java.math.BigInteger;
import java.util.ArrayList;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.SetExtension;
import org.eventb.core.ast.MultiplePredicate;
import org.eventb.core.ast.UnaryExpression;
import org.eventb.core.ast.BecomesEqualTo;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.RelationalPredicate;
import org.eventb.core.ast.QuantifiedExpression;
import org.eventb.core.ast.BoundIdentifier;
import java.util.Arrays;

public class PatternMatcher {
	%include {FormulaV3.tom}
	
	public static List<String> getEnumerationElements(Predicate partition, String set)  throws DAException {
		ArrayList<String> result = new ArrayList<String>();
		%match (Predicate partition) {
			!Partition(eList(FreeIdentifier(_), _*)) -> {
				String errMsg = "Ill-formed partition axiom of set " + set;
				throw new DAException(errMsg);
			}
			Partition(eList(FreeIdentifier(s), _*)) -> {
				if(! (`s).equals(set)) {
					String errMsg = "Ill-formed partition axiom of set " + set;
					throw new DAException(errMsg);
				}
			}	
			Partition(eList(_,_*,SetExtension(eList(FreeIdentifier(x))), _*)) -> {
				result.add(`x);
			}	
		}
		return result;
	}
	
	public static String getNeighboursIdentifier(Predicate networkTyping, String nodesName) {
		%match (Predicate networkTyping) {
			In(FreeIdentifier(neighbours),
			   Tfun(FreeIdentifier(nodes),
					Pow(FreeIdentifier(nodes)))) -> {
				if(`nodes.equals(nodesName)) {
					return `neighbours;
				} else {
					// TODO error while parsing network_typing axiom
					return null;
				}
			}
		}
		// TODO error while parsing network_typing axiom
		return null;
	}
	
	/**
	 * Looks for the first (if any) send operation in the given expression with the given source as emitter
	 * 	and returns the destination and the body of the message if found else returns null.
	 * @param expr
	 * 	the expression potentially containing a send operation.
	 * @param send
	 * 	the name of the send function
	 * @param source
	 * 	the name of the process potentially sending a message
	 * @param channels
	 * 	the name of the variable representing the state of the communication channels.
	 * @return a pair (dest, msg) of the destination process and the message sent.
	 * 	Return null if the send function is not used in the expression or not used properly.
	 */
	public static Pair<Expression, Expression> getSendBody(Expression expr, String send, String source) {
		// TODO tell explicitely the case in which the method returns null
		%match (Expression expr) {
			FunImage(FreeIdentifier(snd),
					 Mapsto(Mapsto(_, Mapsto(FreeIdentifier(src), dest)),
							message))
			&& snd << String send && src << String source -> {
				return new Pair<Expression, Expression>(`dest, `message);
			}
		}
		return null;
	}
	
	/**
	 * Checks if the given predicate is a readyForReception predicate
	 *  and if so returns the source and the body of the message to be received
	 *  else returns null.
	 * @param pred
	 * 	the predicate potentially being a readyForReception predicate
	 * @param rfr
	 * 	the name of the readyForReception function
	 * @param dest
	 * 	the name of the process potentially ready to receive a message
	 * @param channels
	 * 	the name of the variable representing the state of the communication channels.
	 * @return a pair (source, msg) of the destination process and the message sent.
	 * 	Return null if the send function is not used in the expression or not used properly.
	 */
	public static Pair<String, String> getReceiveBody(Predicate pred, String rfr, String dest, String channels) {
		// TODO give exactly the form of the readyForReception predicate
		// 'rfr'('channels' \mapsto (sourceExpression \mapsto 'dest') \mapsto messageExpression) = TRUE
		%match (Predicate pred) {
			Equal(FunImage(FreeIdentifier(fun),
					       Mapsto(Mapsto(FreeIdentifier(chan),
					    			     Mapsto(FreeIdentifier(source),
					    					    FreeIdentifier(dst))),
					    	      FreeIdentifier(message))),
			      TRUE())
			&& fun << String rfr && dst << String dest && chan << String channels -> {
				return new Pair<String, String>(`source, `message);
			}
		}
		return null;
	}
	
	// Returns whether the given expression is of the form {proc \cdot proc \in pcl | proc \mapsto expr}
	// with proc an identifier and expr an expression
	public static Boolean isProcessFunction(Expression expr, String pcl) {
		%match (Expression expr) {
			Cset(bidList(_),
					In(BoundIdentifier(index),FreeIdentifier(pclId)),
					Mapsto(BoundIdentifier(index), _))
			&& pclId << String pcl && index == 0 -> {
				return true;
			}
		}
		return false;
	}
}
