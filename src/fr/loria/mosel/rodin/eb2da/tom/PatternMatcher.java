package fr.loria.mosel.rodin.eb2da.tom;

import fr.loria.mosel.rodin.eb2da.datastructure.DAAction;
import fr.loria.mosel.rodin.eb2da.datastructure.DANames;
import fr.loria.mosel.rodin.eb2da.datastructure.DAException;
import org.eventb.core.ast.BoundIdentDecl;
import fr.loria.mosel.rodin.eb2da.utils.Pair;
import java.util.List;
import java.math.BigInteger;
import java.util.ArrayList;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.Expression;
import org.eventb.core.ast.SetExtension;
import org.eventb.core.ast.MultiplePredicate;
import org.eventb.core.ast.UnaryExpression;
import org.eventb.core.ast.BecomesEqualTo;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.RelationalPredicate;
import org.eventb.core.ast.QuantifiedExpression;
import org.eventb.core.ast.BoundIdentifier;
import java.util.Arrays;

public class PatternMatcher {

private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static boolean tom_equal_term_Predicate(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_Predicate(Object t) {
return  t instanceof Predicate ;
}
private static boolean tom_equal_term_Expression(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_Expression(Object t) {
return  t instanceof Expression ;
}
private static boolean tom_equal_term_BoundIdentDecl(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_BoundIdentDecl(Object t) {
return  t instanceof BoundIdentDecl ;
}
private static boolean tom_equal_term_BigInteger(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_BigInteger(Object t) {
return  t instanceof BigInteger ;
}
private static boolean tom_equal_term_PredicateList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((Predicate[]) t1, (Predicate[]) t2)
	;
}
private static boolean tom_is_sort_PredicateList(Object t) {
return  t instanceof Predicate[] ;
}
private static boolean tom_equal_term_ExpressionList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((Expression[]) t1, (Expression[]) t2)
	;
}
private static boolean tom_is_sort_ExpressionList(Object t) {
return  t instanceof Expression[] ;
}
private static boolean tom_equal_term_BoundIdentDeclList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((BoundIdentDecl[]) t1, (BoundIdentDecl[]) t2)
	;
}
private static boolean tom_is_sort_BoundIdentDeclList(Object t) {
return  t instanceof BoundIdentDecl[] ;
}




private static Expression[] exprAppend(Expression e,  Expression[] t) {
Expression[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}

private static Predicate[] predAppend(Predicate e, Predicate[] t) {
Predicate[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}

private static BoundIdentDecl[] bideclAppend(BoundIdentDecl e, BoundIdentDecl[] t) {
BoundIdentDecl[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}
private static boolean tom_is_fun_sym_eList( Expression[]  t) {
return  true ;
}
private static int tom_get_size_eList_ExpressionList( Expression[]  t) {
return  t.length ;
}
private static  Expression  tom_get_element_eList_ExpressionList( Expression[]  t, int n) {
return  t[n] ;
}
private static  Expression[]  tom_empty_array_eList(int t) { 
return  new Expression[0] ;
}
private static  Expression[]  tom_cons_array_eList( Expression  e,  Expression[]  t) { 
return  exprAppend(e,t) ;
}

  private static   Expression[]  tom_get_slice_eList( Expression[]  subject, int begin, int end) {
     Expression[]  result = tom_empty_array_eList(end-begin);
    while(begin!=end) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(subject,begin),result);
      begin++;
    }
    return result;
  }

  private static   Expression[]  tom_append_array_eList( Expression[]  l2,  Expression[]  l1) {
    int size1 = tom_get_size_eList_ExpressionList(l1);
    int size2 = tom_get_size_eList_ExpressionList(l2);
    int index;
     Expression[]  result = tom_empty_array_eList(size1+size2);
    index=size1;
    while(index >0) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(l1,size1-index),result);
      index--;
    }

    index=size2;
    while(index > 0) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(l2,size2-index),result);
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_bidList( BoundIdentDecl[]  t) {
return  true ;
}
private static int tom_get_size_bidList_BoundIdentDeclList( BoundIdentDecl[]  t) {
return  t.length ;
}
private static  BoundIdentDecl  tom_get_element_bidList_BoundIdentDeclList( BoundIdentDecl[]  t, int n) {
return  t[n] ;
}
private static  BoundIdentDecl[]  tom_empty_array_bidList(int t) { 
return  new BoundIdentDecl[0] ;
}
private static  BoundIdentDecl[]  tom_cons_array_bidList( BoundIdentDecl  e,  BoundIdentDecl[]  t) { 
return  bideclAppend(e,t) ;
}

  private static   BoundIdentDecl[]  tom_get_slice_bidList( BoundIdentDecl[]  subject, int begin, int end) {
     BoundIdentDecl[]  result = tom_empty_array_bidList(end-begin);
    while(begin!=end) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(subject,begin),result);
      begin++;
    }
    return result;
  }

  private static   BoundIdentDecl[]  tom_append_array_bidList( BoundIdentDecl[]  l2,  BoundIdentDecl[]  l1) {
    int size1 = tom_get_size_bidList_BoundIdentDeclList(l1);
    int size2 = tom_get_size_bidList_BoundIdentDeclList(l2);
    int index;
     BoundIdentDecl[]  result = tom_empty_array_bidList(size1+size2);
    index=size1;
    while(index >0) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(l1,size1-index),result);
      index--;
    }

    index=size2;
    while(index > 0) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(l2,size2-index),result);
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_Equal( Predicate  t) {
return  t != null && t.getTag() == Formula.EQUAL ;
}
private static  Expression  tom_get_slot_Equal_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Equal_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_In( Predicate  t) {
return  t != null && t.getTag() == Formula.IN ;
}
private static  Expression  tom_get_slot_In_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_In_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Partition( Predicate  t) {
return  t != null && t.getTag() == Formula.KPARTITION ;
}
private static  Expression[]  tom_get_slot_Partition_childExprs( Predicate  t) {
return  ((MultiplePredicate) t).getChildren() ;
}
private static boolean tom_is_fun_sym_TRUE( Expression  t) {
return  t != null && t.getTag() == Formula.TRUE ;
}
private static boolean tom_is_fun_sym_Mapsto( Expression  t) {
return  t != null && t.getTag() == Formula.MAPSTO ;
}
private static  Expression  tom_get_slot_Mapsto_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Mapsto_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Tfun( Expression  t) {
return  t != null && t.getTag() == Formula.TFUN ;
}
private static  Expression  tom_get_slot_Tfun_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Tfun_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_FunImage( Expression  t) {
return  t != null && t.getTag() == Formula.FUNIMAGE ;
}
private static  Expression  tom_get_slot_FunImage_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_FunImage_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_FreeIdentifier( Expression  t) {
return  t != null && t.getTag() == Formula.FREE_IDENT ;
}
private static  String  tom_get_slot_FreeIdentifier_name( Expression  t) {
return  ((FreeIdentifier)t).getName() ;
}
private static boolean tom_is_fun_sym_BoundIdentifier( Expression  t) {
return  t != null && t.getTag() == Formula.BOUND_IDENT ;
}
private static  int  tom_get_slot_BoundIdentifier_boundIndex( Expression  t) {
return  ((BoundIdentifier)t).getBoundIndex() ;
}
private static boolean tom_is_fun_sym_Cset( Expression  t) {
return  t != null && t.getTag() == Formula.CSET ;
}
private static  BoundIdentDecl[]  tom_get_slot_Cset_identifiers( Expression  t) {
return  ((QuantifiedExpression)t).getBoundIdentDecls() ;
}
private static  Predicate  tom_get_slot_Cset_predicate( Expression  t) {
return  ((QuantifiedExpression)t).getPredicate() ;
}
private static  Expression  tom_get_slot_Cset_expression( Expression  t) {
return  ((QuantifiedExpression)t).getExpression() ;
}
private static boolean tom_is_fun_sym_SetExtension( Expression  t) {
return  t != null && t.getTag() == Formula.SETEXT ;
}
private static  Expression[]  tom_get_slot_SetExtension_members( Expression  t) {
return  ((SetExtension)t).getMembers() ;
}
private static boolean tom_is_fun_sym_Pow( Expression  t) {
return  t != null && t.getTag() == Formula.POW ;
}
private static  Expression  tom_get_slot_Pow_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}


public static List<String> getEnumerationElements(Predicate partition, String set)  throws DAException {
ArrayList<String> result = new ArrayList<String>();

{
{
if (tom_is_sort_Predicate(partition)) {
boolean tomMatch15_11= false ;
if (tom_is_sort_Predicate((( Predicate )partition))) {
if (tom_is_fun_sym_Partition((( Predicate )(( Predicate )partition)))) {
 Expression[]  tomMatch15_1=tom_get_slot_Partition_childExprs((( Predicate )partition));
if (tom_is_fun_sym_eList((( Expression[] )tomMatch15_1))) {
int tomMatch15_5=0;
if (!(tomMatch15_5 >= tom_get_size_eList_ExpressionList(tomMatch15_1))) {
 Expression  tomMatch15_9=tom_get_element_eList_ExpressionList(tomMatch15_1,tomMatch15_5);
if (tom_is_sort_Expression(tomMatch15_9)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch15_9))) {
tomMatch15_11= true ;
}
}
}
}
}
}
if (!(tomMatch15_11)) {

String errMsg = "Ill-formed partition axiom of set " + set;
throw new DAException(errMsg);

}
}
}
{
if (tom_is_sort_Predicate(partition)) {
if (tom_is_sort_Predicate((( Predicate )partition))) {
if (tom_is_fun_sym_Partition((( Predicate )(( Predicate )partition)))) {
 Expression[]  tomMatch15_13=tom_get_slot_Partition_childExprs((( Predicate )partition));
if (tom_is_fun_sym_eList((( Expression[] )tomMatch15_13))) {
int tomMatch15_17=0;
if (!(tomMatch15_17 >= tom_get_size_eList_ExpressionList(tomMatch15_13))) {
 Expression  tomMatch15_21=tom_get_element_eList_ExpressionList(tomMatch15_13,tomMatch15_17);
if (tom_is_sort_Expression(tomMatch15_21)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch15_21))) {

if(! (
tom_get_slot_FreeIdentifier_name(tomMatch15_21)).equals(set)) {
String errMsg = "Ill-formed partition axiom of set " + set;
throw new DAException(errMsg);
}

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Predicate(partition)) {
if (tom_is_sort_Predicate((( Predicate )partition))) {
if (tom_is_fun_sym_Partition((( Predicate )(( Predicate )partition)))) {
 Expression[]  tomMatch15_24=tom_get_slot_Partition_childExprs((( Predicate )partition));
if (tom_is_fun_sym_eList((( Expression[] )tomMatch15_24))) {
int tomMatch15_28=0;
if (!(tomMatch15_28 >= tom_get_size_eList_ExpressionList(tomMatch15_24))) {
int tomMatch15_end_32=tomMatch15_28 + 1;
do {
{
if (!(tomMatch15_end_32 >= tom_get_size_eList_ExpressionList(tomMatch15_24))) {
 Expression  tomMatch15_36=tom_get_element_eList_ExpressionList(tomMatch15_24,tomMatch15_end_32);
if (tom_is_sort_Expression(tomMatch15_36)) {
if (tom_is_fun_sym_SetExtension((( Expression )tomMatch15_36))) {
 Expression[]  tomMatch15_35=tom_get_slot_SetExtension_members(tomMatch15_36);
if (tom_is_fun_sym_eList((( Expression[] )tomMatch15_35))) {
int tomMatch15_39=0;
if (!(tomMatch15_39 >= tom_get_size_eList_ExpressionList(tomMatch15_35))) {
 Expression  tomMatch15_42=tom_get_element_eList_ExpressionList(tomMatch15_35,tomMatch15_39);
if (tom_is_sort_Expression(tomMatch15_42)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch15_42))) {
if (tomMatch15_39 + 1 >= tom_get_size_eList_ExpressionList(tomMatch15_35)) {

result.add(
tom_get_slot_FreeIdentifier_name(tomMatch15_42));

}
}
}
}
}
}
}
}
tomMatch15_end_32=tomMatch15_end_32 + 1;
}
} while(!(tomMatch15_end_32 > tom_get_size_eList_ExpressionList(tomMatch15_24)));
}
}
}
}
}
}
}

return result;
}

public static String getNeighboursIdentifier(Predicate networkTyping, String nodesName) {

{
{
if (tom_is_sort_Predicate(networkTyping)) {
if (tom_is_sort_Predicate((( Predicate )networkTyping))) {
if (tom_is_fun_sym_In((( Predicate )(( Predicate )networkTyping)))) {
 Expression  tomMatch16_1=tom_get_slot_In_leftExpr((( Predicate )networkTyping));
 Expression  tomMatch16_2=tom_get_slot_In_rightExpr((( Predicate )networkTyping));
if (tom_is_sort_Expression(tomMatch16_1)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch16_1))) {
if (tom_is_sort_Expression(tomMatch16_2)) {
if (tom_is_fun_sym_Tfun((( Expression )tomMatch16_2))) {
 Expression  tomMatch16_8=tom_get_slot_Tfun_leftExpr(tomMatch16_2);
 Expression  tomMatch16_9=tom_get_slot_Tfun_rightExpr(tomMatch16_2);
if (tom_is_sort_Expression(tomMatch16_8)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch16_8))) {
 String  tom_nodes=tom_get_slot_FreeIdentifier_name(tomMatch16_8);
if (tom_is_sort_Expression(tomMatch16_9)) {
if (tom_is_fun_sym_Pow((( Expression )tomMatch16_9))) {
 Expression  tomMatch16_15=tom_get_slot_Pow_childExpr(tomMatch16_9);
if (tom_is_sort_Expression(tomMatch16_15)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch16_15))) {
if (tom_equal_term_String(tom_nodes, tom_get_slot_FreeIdentifier_name(tomMatch16_15))) {

if(
tom_nodes.equals(nodesName)) {
return 
tom_get_slot_FreeIdentifier_name(tomMatch16_1);
} else {
// TODO error while parsing network_typing axiom
return null;
}

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

// TODO error while parsing network_typing axiom
return null;
}

/**
* Looks for the first (if any) send operation in the given expression with the given source as emitter
* 	and returns the destination and the body of the message if found else returns null.
* @param expr
* 	the expression potentially containing a send operation.
* @param send
* 	the name of the send function
* @param source
* 	the name of the process potentially sending a message
* @param channels
* 	the name of the variable representing the state of the communication channels.
* @return a pair (dest, msg) of the destination process and the message sent.
* 	Return null if the send function is not used in the expression or not used properly.
*/
public static Pair<Expression, Expression> getSendBody(Expression expr, String send, String source) {
// TODO tell explicitely the case in which the method returns null

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FunImage((( Expression )(( Expression )expr)))) {
 Expression  tomMatch17_3=tom_get_slot_FunImage_leftExpr((( Expression )expr));
 Expression  tomMatch17_4=tom_get_slot_FunImage_rightExpr((( Expression )expr));
if (tom_is_sort_Expression(tomMatch17_3)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch17_3))) {
if (tom_is_sort_Expression(tomMatch17_4)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch17_4))) {
 Expression  tomMatch17_10=tom_get_slot_Mapsto_leftExpr(tomMatch17_4);
if (tom_is_sort_Expression(tomMatch17_10)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch17_10))) {
 Expression  tomMatch17_15=tom_get_slot_Mapsto_rightExpr(tomMatch17_10);
if (tom_is_sort_Expression(tomMatch17_15)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch17_15))) {
 Expression  tomMatch17_18=tom_get_slot_Mapsto_leftExpr(tomMatch17_15);
if (tom_is_sort_Expression(tomMatch17_18)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch17_18))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch17_3), (( String )send))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch17_18), (( String )source))) {

return new Pair<Expression, Expression>(
tom_get_slot_Mapsto_rightExpr(tomMatch17_15), 
tom_get_slot_Mapsto_rightExpr(tomMatch17_4));

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

return null;
}

/**
* Checks if the given predicate is a readyForReception predicate
*  and if so returns the source and the body of the message to be received
*  else returns null.
* @param pred
* 	the predicate potentially being a readyForReception predicate
* @param rfr
* 	the name of the readyForReception function
* @param dest
* 	the name of the process potentially ready to receive a message
* @param channels
* 	the name of the variable representing the state of the communication channels.
* @return a pair (source, msg) of the destination process and the message sent.
* 	Return null if the send function is not used in the expression or not used properly.
*/
public static Pair<String, String> getReceiveBody(Predicate pred, String rfr, String dest, String channels) {
// TODO give exactly the form of the readyForReception predicate
// 'rfr'('channels' \mapsto (sourceExpression \mapsto 'dest') \mapsto messageExpression) = TRUE

{
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Equal((( Predicate )(( Predicate )pred)))) {
 Expression  tomMatch18_4=tom_get_slot_Equal_leftExpr((( Predicate )pred));
 Expression  tomMatch18_5=tom_get_slot_Equal_rightExpr((( Predicate )pred));
if (tom_is_sort_Expression(tomMatch18_4)) {
if (tom_is_fun_sym_FunImage((( Expression )tomMatch18_4))) {
 Expression  tomMatch18_8=tom_get_slot_FunImage_leftExpr(tomMatch18_4);
 Expression  tomMatch18_9=tom_get_slot_FunImage_rightExpr(tomMatch18_4);
if (tom_is_sort_Expression(tomMatch18_8)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch18_8))) {
if (tom_is_sort_Expression(tomMatch18_9)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch18_9))) {
 Expression  tomMatch18_15=tom_get_slot_Mapsto_leftExpr(tomMatch18_9);
 Expression  tomMatch18_16=tom_get_slot_Mapsto_rightExpr(tomMatch18_9);
if (tom_is_sort_Expression(tomMatch18_15)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch18_15))) {
 Expression  tomMatch18_19=tom_get_slot_Mapsto_leftExpr(tomMatch18_15);
 Expression  tomMatch18_20=tom_get_slot_Mapsto_rightExpr(tomMatch18_15);
if (tom_is_sort_Expression(tomMatch18_19)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch18_19))) {
if (tom_is_sort_Expression(tomMatch18_20)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch18_20))) {
 Expression  tomMatch18_26=tom_get_slot_Mapsto_leftExpr(tomMatch18_20);
 Expression  tomMatch18_27=tom_get_slot_Mapsto_rightExpr(tomMatch18_20);
if (tom_is_sort_Expression(tomMatch18_26)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch18_26))) {
if (tom_is_sort_Expression(tomMatch18_27)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch18_27))) {
if (tom_is_sort_Expression(tomMatch18_16)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch18_16))) {
if (tom_is_sort_Expression(tomMatch18_5)) {
if (tom_is_fun_sym_TRUE((( Expression )tomMatch18_5))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch18_8), (( String )rfr))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch18_27), (( String )dest))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch18_19), (( String )channels))) {

return new Pair<String, String>(
tom_get_slot_FreeIdentifier_name(tomMatch18_26), 
tom_get_slot_FreeIdentifier_name(tomMatch18_16));

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

return null;
}

// Returns whether the given expression is of the form {proc \cdot proc \in pcl | proc \mapsto expr}
// with proc an identifier and expr an expression
public static Boolean isProcessFunction(Expression expr, String pcl) {

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch19_2=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch19_3=tom_get_slot_Cset_predicate((( Expression )expr));
 Expression  tomMatch19_4=tom_get_slot_Cset_expression((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch19_2))) {
int tomMatch19_25=0;
if (!(tomMatch19_25 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch19_2))) {
if (tomMatch19_25 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch19_2)) {
if (tom_is_sort_Predicate(tomMatch19_3)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch19_3))) {
 Expression  tomMatch19_7=tom_get_slot_In_leftExpr(tomMatch19_3);
 Expression  tomMatch19_8=tom_get_slot_In_rightExpr(tomMatch19_3);
if (tom_is_sort_Expression(tomMatch19_7)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch19_7))) {
 int  tom_index=tom_get_slot_BoundIdentifier_boundIndex(tomMatch19_7);
if (tom_is_sort_Expression(tomMatch19_8)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch19_8))) {
if (tom_is_sort_Expression(tomMatch19_4)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch19_4))) {
 Expression  tomMatch19_17=tom_get_slot_Mapsto_leftExpr(tomMatch19_4);
if (tom_is_sort_Expression(tomMatch19_17)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch19_17))) {
if (tom_equal_term_int(tom_index, tom_get_slot_BoundIdentifier_boundIndex(tomMatch19_17))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch19_8), (( String )pcl))) {
if (tom_equal_term_int(0, tom_index)) {

return true;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

return false;
}
}
