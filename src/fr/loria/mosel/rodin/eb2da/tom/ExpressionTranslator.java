package fr.loria.mosel.rodin.eb2da.tom;

import java.util.HashMap;
import java.math.BigInteger;
import fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.*;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Map.Entry;

import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.BinaryPredicate;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.BoundIdentDecl;
import org.eventb.core.ast.BoundIdentifier;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.QuantifiedExpression;
import org.eventb.core.ast.QuantifiedPredicate;
import org.eventb.core.ast.RelationalPredicate;
import org.eventb.core.ast.UnaryExpression;
import org.eventb.core.ast.UnaryPredicate;
import org.eventb.core.ast.SetExtension;
import org.eventb.core.ast.AssociativePredicate;
import org.eventb.core.ast.AssociativeExpression;
import org.eventb.core.ast.IntegerLiteral;

import fr.loria.mosel.rodin.eb2da.datastructure.DAAction;
import fr.loria.mosel.rodin.eb2da.datastructure.DAVariable;
import fr.loria.mosel.rodin.eb2da.datastructure.DAConstant;
import fr.loria.mosel.rodin.eb2da.datastructure.DAMessageConstructor;
import fr.loria.mosel.rodin.eb2da.datastructure.DANames;

public class ExpressionTranslator {

private static boolean tom_equal_term_int(int t1, int t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_int(int t) {
return  true ;
}
private static boolean tom_equal_term_char(char t1, char t2) {
return  t1==t2 ;
}
private static boolean tom_is_sort_char(char t) {
return  true ;
}
private static boolean tom_equal_term_String(String t1, String t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_String(String t) {
return  t instanceof String ;
}
private static boolean tom_equal_term_Predicate(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_Predicate(Object t) {
return  t instanceof Predicate ;
}
private static boolean tom_equal_term_Expression(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_Expression(Object t) {
return  t instanceof Expression ;
}
private static boolean tom_equal_term_BoundIdentDecl(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_BoundIdentDecl(Object t) {
return  t instanceof BoundIdentDecl ;
}
private static boolean tom_equal_term_BigInteger(Object t1, Object t2) {
return  t1.equals(t2) ;
}
private static boolean tom_is_sort_BigInteger(Object t) {
return  t instanceof BigInteger ;
}
private static boolean tom_equal_term_PredicateList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((Predicate[]) t1, (Predicate[]) t2)
	;
}
private static boolean tom_is_sort_PredicateList(Object t) {
return  t instanceof Predicate[] ;
}
private static boolean tom_equal_term_ExpressionList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((Expression[]) t1, (Expression[]) t2)
	;
}
private static boolean tom_is_sort_ExpressionList(Object t) {
return  t instanceof Expression[] ;
}
private static boolean tom_equal_term_BoundIdentDeclList(Object t1, Object t2) {
return 
		java.util.Arrays.equals((BoundIdentDecl[]) t1, (BoundIdentDecl[]) t2)
	;
}
private static boolean tom_is_sort_BoundIdentDeclList(Object t) {
return  t instanceof BoundIdentDecl[] ;
}




private static Expression[] exprAppend(Expression e,  Expression[] t) {
Expression[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}

private static Predicate[] predAppend(Predicate e, Predicate[] t) {
Predicate[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}

private static BoundIdentDecl[] bideclAppend(BoundIdentDecl e, BoundIdentDecl[] t) {
BoundIdentDecl[] res = Arrays.copyOf(t, t.length+1);
res[t.length] = e;
return res;
}
private static boolean tom_is_fun_sym_pList( Predicate[]  t) {
return  true ;
}
private static int tom_get_size_pList_PredicateList( Predicate[]  t) {
return  t.length ;
}
private static  Predicate  tom_get_element_pList_PredicateList( Predicate[]  t, int n) {
return  t[n] ;
}
private static  Predicate[]  tom_empty_array_pList(int t) { 
return  new Predicate[0] ;
}
private static  Predicate[]  tom_cons_array_pList( Predicate  e,  Predicate[]  t) { 
return  predAppend(e,t) ;
}

  private static   Predicate[]  tom_get_slice_pList( Predicate[]  subject, int begin, int end) {
     Predicate[]  result = tom_empty_array_pList(end-begin);
    while(begin!=end) {
      result = tom_cons_array_pList(tom_get_element_pList_PredicateList(subject,begin),result);
      begin++;
    }
    return result;
  }

  private static   Predicate[]  tom_append_array_pList( Predicate[]  l2,  Predicate[]  l1) {
    int size1 = tom_get_size_pList_PredicateList(l1);
    int size2 = tom_get_size_pList_PredicateList(l2);
    int index;
     Predicate[]  result = tom_empty_array_pList(size1+size2);
    index=size1;
    while(index >0) {
      result = tom_cons_array_pList(tom_get_element_pList_PredicateList(l1,size1-index),result);
      index--;
    }

    index=size2;
    while(index > 0) {
      result = tom_cons_array_pList(tom_get_element_pList_PredicateList(l2,size2-index),result);
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_eList( Expression[]  t) {
return  true ;
}
private static int tom_get_size_eList_ExpressionList( Expression[]  t) {
return  t.length ;
}
private static  Expression  tom_get_element_eList_ExpressionList( Expression[]  t, int n) {
return  t[n] ;
}
private static  Expression[]  tom_empty_array_eList(int t) { 
return  new Expression[0] ;
}
private static  Expression[]  tom_cons_array_eList( Expression  e,  Expression[]  t) { 
return  exprAppend(e,t) ;
}

  private static   Expression[]  tom_get_slice_eList( Expression[]  subject, int begin, int end) {
     Expression[]  result = tom_empty_array_eList(end-begin);
    while(begin!=end) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(subject,begin),result);
      begin++;
    }
    return result;
  }

  private static   Expression[]  tom_append_array_eList( Expression[]  l2,  Expression[]  l1) {
    int size1 = tom_get_size_eList_ExpressionList(l1);
    int size2 = tom_get_size_eList_ExpressionList(l2);
    int index;
     Expression[]  result = tom_empty_array_eList(size1+size2);
    index=size1;
    while(index >0) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(l1,size1-index),result);
      index--;
    }

    index=size2;
    while(index > 0) {
      result = tom_cons_array_eList(tom_get_element_eList_ExpressionList(l2,size2-index),result);
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_bidList( BoundIdentDecl[]  t) {
return  true ;
}
private static int tom_get_size_bidList_BoundIdentDeclList( BoundIdentDecl[]  t) {
return  t.length ;
}
private static  BoundIdentDecl  tom_get_element_bidList_BoundIdentDeclList( BoundIdentDecl[]  t, int n) {
return  t[n] ;
}
private static  BoundIdentDecl[]  tom_empty_array_bidList(int t) { 
return  new BoundIdentDecl[0] ;
}
private static  BoundIdentDecl[]  tom_cons_array_bidList( BoundIdentDecl  e,  BoundIdentDecl[]  t) { 
return  bideclAppend(e,t) ;
}

  private static   BoundIdentDecl[]  tom_get_slice_bidList( BoundIdentDecl[]  subject, int begin, int end) {
     BoundIdentDecl[]  result = tom_empty_array_bidList(end-begin);
    while(begin!=end) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(subject,begin),result);
      begin++;
    }
    return result;
  }

  private static   BoundIdentDecl[]  tom_append_array_bidList( BoundIdentDecl[]  l2,  BoundIdentDecl[]  l1) {
    int size1 = tom_get_size_bidList_BoundIdentDeclList(l1);
    int size2 = tom_get_size_bidList_BoundIdentDeclList(l2);
    int index;
     BoundIdentDecl[]  result = tom_empty_array_bidList(size1+size2);
    index=size1;
    while(index >0) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(l1,size1-index),result);
      index--;
    }

    index=size2;
    while(index > 0) {
      result = tom_cons_array_bidList(tom_get_element_bidList_BoundIdentDeclList(l2,size2-index),result);
      index--;
    }
    return result;
  }private static boolean tom_is_fun_sym_Land( Predicate  t) {
return  t != null && t.getTag() == Formula.LAND ;
}
private static  Predicate[]  tom_get_slot_Land_childPreds( Predicate  t) {
return  ((AssociativePredicate) t).getChildren() ;
}
private static boolean tom_is_fun_sym_Lor( Predicate  t) {
return  t != null && t.getTag() == Formula.LOR ;
}
private static  Predicate[]  tom_get_slot_Lor_childPreds( Predicate  t) {
return  ((AssociativePredicate) t).getChildren() ;
}
private static boolean tom_is_fun_sym_Limp( Predicate  t) {
return  t != null && t.getTag() == Formula.LIMP ;
}
private static  Predicate  tom_get_slot_Limp_leftPred( Predicate  t) {
return  ((BinaryPredicate) t).getLeft() ;
}
private static  Predicate  tom_get_slot_Limp_rightPred( Predicate  t) {
return  ((BinaryPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_NotB( Predicate  t) {
return  t != null && t.getTag() == Formula.NOT ;
}
private static  Predicate  tom_get_slot_NotB_childPred( Predicate  t) {
return  ((UnaryPredicate) t). getChild() ;
}
private static boolean tom_is_fun_sym_Equal( Predicate  t) {
return  t != null && t.getTag() == Formula.EQUAL ;
}
private static  Expression  tom_get_slot_Equal_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Equal_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_NotEqual( Predicate  t) {
return  t != null && t.getTag() == Formula.NOTEQUAL ;
}
private static  Expression  tom_get_slot_NotEqual_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_NotEqual_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Lt( Predicate  t) {
return  t != null && t.getTag() == Formula.LT ;
}
private static  Expression  tom_get_slot_Lt_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Lt_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Le( Predicate  t) {
return  t != null && t.getTag() == Formula.LE ;
}
private static  Expression  tom_get_slot_Le_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Le_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Gt( Predicate  t) {
return  t != null && t.getTag() == Formula.GT ;
}
private static  Expression  tom_get_slot_Gt_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Gt_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Ge( Predicate  t) {
return  t != null && t.getTag() == Formula.GE ;
}
private static  Expression  tom_get_slot_Ge_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Ge_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_In( Predicate  t) {
return  t != null && t.getTag() == Formula.IN ;
}
private static  Expression  tom_get_slot_In_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_In_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_NotIn( Predicate  t) {
return  t != null && t.getTag() == Formula.NOTIN ;
}
private static  Expression  tom_get_slot_NotIn_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_NotIn_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_Subset( Predicate  t) {
return  t != null && t.getTag() == Formula.SUBSET ;
}
private static  Expression  tom_get_slot_Subset_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_Subset_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_NotSubset( Predicate  t) {
return  t != null && t.getTag() == Formula.NOTSUBSET ;
}
private static  Expression  tom_get_slot_NotSubset_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_NotSubset_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_SubsetEq( Predicate  t) {
return  t != null && t.getTag() == Formula.SUBSETEQ ;
}
private static  Expression  tom_get_slot_SubsetEq_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_SubsetEq_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_NotSubsetEq( Predicate  t) {
return  t != null && t.getTag() == Formula.NOTSUBSETEQ ;
}
private static  Expression  tom_get_slot_NotSubsetEq_leftExpr( Predicate  t) {
return  ((RelationalPredicate) t). getLeft() ;
}
private static  Expression  tom_get_slot_NotSubsetEq_rightExpr( Predicate  t) {
return  ((RelationalPredicate) t).getRight() ;
}
private static boolean tom_is_fun_sym_ForAll( Predicate  t) {
return  t != null && t.getTag() == Formula.FORALL ;
}
private static  BoundIdentDecl[]  tom_get_slot_ForAll_identifiers( Predicate  t) {
return  ((QuantifiedPredicate)t).getBoundIdentDecls() ;
}
private static  Predicate  tom_get_slot_ForAll_predicate( Predicate  t) {
return  ((QuantifiedPredicate)t).getPredicate() ;
}
private static boolean tom_is_fun_sym_Exists( Predicate  t) {
return  t != null && t.getTag() == Formula.EXISTS ;
}
private static  BoundIdentDecl[]  tom_get_slot_Exists_identifiers( Predicate  t) {
return  ((QuantifiedPredicate)t).getBoundIdentDecls() ;
}
private static  Predicate  tom_get_slot_Exists_predicate( Predicate  t) {
return  ((QuantifiedPredicate)t).getPredicate() ;
}
private static boolean tom_is_fun_sym_BUnion( Expression  t) {
return  t != null && t.getTag() == Formula.BUNION ;
}
private static  Expression[]  tom_get_slot_BUnion_childExprs( Expression  t) {
return  ((AssociativeExpression) t).getChildren() ;
}
private static boolean tom_is_fun_sym_BInter( Expression  t) {
return  t != null && t.getTag() == Formula.BINTER ;
}
private static  Expression[]  tom_get_slot_BInter_childExprs( Expression  t) {
return  ((AssociativeExpression) t).getChildren() ;
}
private static boolean tom_is_fun_sym_Plus( Expression  t) {
return  t != null && t.getTag() == Formula.PLUS ;
}
private static  Expression[]  tom_get_slot_Plus_childExprs( Expression  t) {
return  ((AssociativeExpression) t).getChildren() ;
}
private static boolean tom_is_fun_sym_Mul( Expression  t) {
return  t != null && t.getTag() == Formula.MUL ;
}
private static  Expression[]  tom_get_slot_Mul_childExprs( Expression  t) {
return  ((AssociativeExpression) t).getChildren() ;
}
private static boolean tom_is_fun_sym_INTEGER( Expression  t) {
return  t != null && t.getTag() == Formula.INTEGER ;
}
private static boolean tom_is_fun_sym_TRUE( Expression  t) {
return  t != null && t.getTag() == Formula.TRUE ;
}
private static boolean tom_is_fun_sym_FALSE( Expression  t) {
return  t != null && t.getTag() == Formula.FALSE ;
}
private static boolean tom_is_fun_sym_EmptySet( Expression  t) {
return  t != null && t.getTag() == Formula.EMPTYSET ;
}
private static boolean tom_is_fun_sym_Mapsto( Expression  t) {
return  t != null && t.getTag() == Formula.MAPSTO ;
}
private static  Expression  tom_get_slot_Mapsto_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Mapsto_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Pfun( Expression  t) {
return  t != null && t.getTag() == Formula.PFUN ;
}
private static  Expression  tom_get_slot_Pfun_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Pfun_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Tfun( Expression  t) {
return  t != null && t.getTag() == Formula.TFUN ;
}
private static  Expression  tom_get_slot_Tfun_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Tfun_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Pinj( Expression  t) {
return  t != null && t.getTag() == Formula.PINJ ;
}
private static  Expression  tom_get_slot_Pinj_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Pinj_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Tinj( Expression  t) {
return  t != null && t.getTag() == Formula.TINJ ;
}
private static  Expression  tom_get_slot_Tinj_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Tinj_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Psur( Expression  t) {
return  t != null && t.getTag() == Formula.PSUR ;
}
private static  Expression  tom_get_slot_Psur_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Psur_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Tsur( Expression  t) {
return  t != null && t.getTag() == Formula.TSUR ;
}
private static  Expression  tom_get_slot_Tsur_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Tsur_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Tbij( Expression  t) {
return  t != null && t.getTag() == Formula.TBIJ ;
}
private static  Expression  tom_get_slot_Tbij_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Tbij_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_SetMinus( Expression  t) {
return  t != null && t.getTag() == Formula.SETMINUS ;
}
private static  Expression  tom_get_slot_SetMinus_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_SetMinus_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Cprod( Expression  t) {
return  t != null && t.getTag() == Formula.CPROD ;
}
private static  Expression  tom_get_slot_Cprod_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Cprod_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_UpTo( Expression  t) {
return  t != null && t.getTag() == Formula.UPTO ;
}
private static  Expression  tom_get_slot_UpTo_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_UpTo_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Minus( Expression  t) {
return  t != null && t.getTag() == Formula.MINUS ;
}
private static  Expression  tom_get_slot_Minus_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Minus_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Div( Expression  t) {
return  t != null && t.getTag() == Formula.DIV ;
}
private static  Expression  tom_get_slot_Div_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Div_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Mod( Expression  t) {
return  t != null && t.getTag() == Formula.MOD ;
}
private static  Expression  tom_get_slot_Mod_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Mod_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_Expn( Expression  t) {
return  t != null && t.getTag() == Formula.EXPN ;
}
private static  Expression  tom_get_slot_Expn_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_Expn_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_FunImage( Expression  t) {
return  t != null && t.getTag() == Formula.FUNIMAGE ;
}
private static  Expression  tom_get_slot_FunImage_leftExpr( Expression  t) {
return  ((BinaryExpression) t).getLeft() ;
}
private static  Expression  tom_get_slot_FunImage_rightExpr( Expression  t) {
return  ((BinaryExpression) t).getRight() ;
}
private static boolean tom_is_fun_sym_FreeIdentifier( Expression  t) {
return  t != null && t.getTag() == Formula.FREE_IDENT ;
}
private static  String  tom_get_slot_FreeIdentifier_name( Expression  t) {
return  ((FreeIdentifier)t).getName() ;
}
private static boolean tom_is_fun_sym_BoundIdentifier( Expression  t) {
return  t != null && t.getTag() == Formula.BOUND_IDENT ;
}
private static  int  tom_get_slot_BoundIdentifier_boundIndex( Expression  t) {
return  ((BoundIdentifier)t).getBoundIndex() ;
}
private static boolean tom_is_fun_sym_IntegerLiteral( Expression  t) {
return  t instanceof IntegerLiteral ;
}
private static  BigInteger  tom_get_slot_IntegerLiteral_value( Expression  t) {
return  ((IntegerLiteral) t).getValue() ;
}
private static boolean tom_is_fun_sym_Cset( Expression  t) {
return  t != null && t.getTag() == Formula.CSET ;
}
private static  BoundIdentDecl[]  tom_get_slot_Cset_identifiers( Expression  t) {
return  ((QuantifiedExpression)t).getBoundIdentDecls() ;
}
private static  Predicate  tom_get_slot_Cset_predicate( Expression  t) {
return  ((QuantifiedExpression)t).getPredicate() ;
}
private static  Expression  tom_get_slot_Cset_expression( Expression  t) {
return  ((QuantifiedExpression)t).getExpression() ;
}
private static boolean tom_is_fun_sym_SetExtension( Expression  t) {
return  t != null && t.getTag() == Formula.SETEXT ;
}
private static  Expression[]  tom_get_slot_SetExtension_members( Expression  t) {
return  ((SetExtension)t).getMembers() ;
}
private static boolean tom_is_fun_sym_Card( Expression  t) {
return  t != null && t.getTag() == Formula.KCARD ;
}
private static  Expression  tom_get_slot_Card_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_is_fun_sym_Pow( Expression  t) {
return  t != null && t.getTag() == Formula.POW ;
}
private static  Expression  tom_get_slot_Pow_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_is_fun_sym_Dom( Expression  t) {
return  t != null && t.getTag() == Formula.KDOM ;
}
private static  Expression  tom_get_slot_Dom_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_is_fun_sym_Ran( Expression  t) {
return  t != null && t.getTag() == Formula.KRAN ;
}
private static  Expression  tom_get_slot_Ran_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_is_fun_sym_Min( Expression  t) {
return  t != null && t.getTag() == Formula.KMIN ;
}
private static  Expression  tom_get_slot_Min_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_is_fun_sym_Max( Expression  t) {
return  t != null && t.getTag() == Formula.KMAX ;
}
private static  Expression  tom_get_slot_Max_childExpr( Expression  t) {
return  ((UnaryExpression) t).getChild() ;
}
private static boolean tom_equal_term_Type(Object t1, Object t2) {
return  (t1==t2) ;
}
private static boolean tom_is_sort_Type(Object t) {
return  (t instanceof fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type) ;
}
private static boolean tom_is_fun_sym_Function( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t) {
return  (t instanceof fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Function) ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_make_Function( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t0,  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t1) { 
return  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Function.make(t0, t1) ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_get_slot_Function_dom( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t) {
return  t.getdom() ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_get_slot_Function_ran( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t) {
return  t.getran() ;
}
private static boolean tom_is_fun_sym_PowSet( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t) {
return  (t instanceof fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.PowSet) ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_make_PowSet( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t0) { 
return  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.PowSet.make(t0) ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_get_slot_PowSet_t( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t) {
return  t.gett() ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_make_ProdSet( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t0,  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  t1) { 
return  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.ProdSet.make(t0, t1) ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_make_Int() { 
return  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Int.make() ;
}
private static  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_make_StaticSet( String  t0) { 
return  fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.StaticSet.make(t0) ;
}


private static Type exprToType(Expression expr) {

{
{
if (tom_is_sort_Expression(expr)) {
boolean tomMatch1_11= false ;
 Expression  tomMatch1_10= null ;
 Expression  tomMatch1_8= null ;
 Expression  tomMatch1_4= null ;
 Expression  tomMatch1_1= null ;
 Expression  tomMatch1_9= null ;
 Expression  tomMatch1_6= null ;
 Expression  tomMatch1_2= null ;
 Expression  tomMatch1_5= null ;
 Expression  tomMatch1_7= null ;
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Tfun((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_4=(( Expression )expr);
tomMatch1_1=tom_get_slot_Tfun_leftExpr(tomMatch1_4);
tomMatch1_2=tom_get_slot_Tfun_rightExpr(tomMatch1_4);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Pfun((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_5=(( Expression )expr);
tomMatch1_1=tom_get_slot_Pfun_leftExpr(tomMatch1_5);
tomMatch1_2=tom_get_slot_Pfun_rightExpr(tomMatch1_5);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Tinj((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_6=(( Expression )expr);
tomMatch1_1=tom_get_slot_Tinj_leftExpr(tomMatch1_6);
tomMatch1_2=tom_get_slot_Tinj_rightExpr(tomMatch1_6);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Pinj((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_7=(( Expression )expr);
tomMatch1_1=tom_get_slot_Pinj_leftExpr(tomMatch1_7);
tomMatch1_2=tom_get_slot_Pinj_rightExpr(tomMatch1_7);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Tsur((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_8=(( Expression )expr);
tomMatch1_1=tom_get_slot_Tsur_leftExpr(tomMatch1_8);
tomMatch1_2=tom_get_slot_Tsur_rightExpr(tomMatch1_8);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Psur((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_9=(( Expression )expr);
tomMatch1_1=tom_get_slot_Psur_leftExpr(tomMatch1_9);
tomMatch1_2=tom_get_slot_Psur_rightExpr(tomMatch1_9);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Tbij((( Expression )(( Expression )expr)))) {
{
tomMatch1_11= true ;
tomMatch1_10=(( Expression )expr);
tomMatch1_1=tom_get_slot_Tbij_leftExpr(tomMatch1_10);
tomMatch1_2=tom_get_slot_Tbij_rightExpr(tomMatch1_10);
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch1_11) {
return 
tom_make_Function(exprToType(tomMatch1_1),exprToType(tomMatch1_2)); 
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Pow((( Expression )(( Expression )expr)))) {
return 
tom_make_PowSet(exprToType(tom_get_slot_Pow_childExpr((( Expression )expr)))); 
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cprod((( Expression )(( Expression )expr)))) {
return 
tom_make_ProdSet(exprToType(tom_get_slot_Cprod_leftExpr((( Expression )expr))),exprToType(tom_get_slot_Cprod_rightExpr((( Expression )expr)))); 
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )(( Expression )expr)))) {
return 
tom_make_StaticSet(tom_get_slot_FreeIdentifier_name((( Expression )expr))); 
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_INTEGER((( Expression )(( Expression )expr)))) {
return 
tom_make_Int(); 
}
}
}
}
}

return 
tom_make_Int();
}

// Function to call for the translation of any expression
public static String translateExpression(Expression expr, Expression type,
HashMap<String,DAVariable> mapper, DANames dan,
List<DAMessageConstructor> msgConstructors) {
Type t = exprToType(type);
List<DAVariable> boundIds = new ArrayList<DAVariable>();
return translateExpression(expr, t, mapper, boundIds, dan, msgConstructors);
}

// Function to call for the translation of initialisation assignment.
public static String translateInitExpression(Expression expr, Expression type,
HashMap<String,DAVariable> mapper, String pcl, DANames dan,
List<DAMessageConstructor> msgConstructors) {

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch2_2=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch2_3=tom_get_slot_Cset_predicate((( Expression )expr));
 Expression  tomMatch2_4=tom_get_slot_Cset_expression((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch2_2))) {
int tomMatch2_25=0;
if (!(tomMatch2_25 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch2_2))) {
if (tomMatch2_25 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch2_2)) {
if (tom_is_sort_Predicate(tomMatch2_3)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch2_3))) {
 Expression  tomMatch2_7=tom_get_slot_In_leftExpr(tomMatch2_3);
 Expression  tomMatch2_8=tom_get_slot_In_rightExpr(tomMatch2_3);
if (tom_is_sort_Expression(tomMatch2_7)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch2_7))) {
 int  tom_bi=tom_get_slot_BoundIdentifier_boundIndex(tomMatch2_7);
if (tom_is_sort_Expression(tomMatch2_8)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch2_8))) {
if (tom_is_sort_Expression(tomMatch2_4)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch2_4))) {
 Expression  tomMatch2_17=tom_get_slot_Mapsto_leftExpr(tomMatch2_4);
if (tom_is_sort_Expression(tomMatch2_17)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch2_17))) {
if (tom_equal_term_int(tom_bi, tom_get_slot_BoundIdentifier_boundIndex(tomMatch2_17))) {
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch2_8), (( String )pcl))) {
if (tom_equal_term_int(0, tom_bi)) {

HashMap<String, DAVariable>	newMapper = new HashMap<String, DAVariable>(mapper);
List<DAVariable> boundIds = new ArrayList<DAVariable>();
boundIds.add(0, new DAVariable("self", 
tomMatch2_8, null));
return 
translateExpression(tom_get_slot_Mapsto_rightExpr(tomMatch2_4),exprToType(type),newMapper,boundIds,dan,msgConstructors);

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}

return translateExpression(expr, type, mapper, dan, msgConstructors);
}

private static String translateExpression(Expression expr,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
Type exprType = getType(expr, mapper, boundIds, dan, msgConstructors);
return translateExpression(expr, exprType, mapper, boundIds, dan, msgConstructors);
}

private static String translateExpression(Expression expr, Type type,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
if(type == 
tom_make_StaticSet(dan.getMessages())) {
return translateMessageExpression(expr, mapper, boundIds, dan, msgConstructors);
} else {
return translateNotMsgExpression(expr, type, mapper, boundIds, dan, msgConstructors);
}
}

// TODO add more types
// TODO complete
private static String translateNotMsgExpression(Expression expr, Type type,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {

if(mapper.containsKey(expr.toString())) {
return mapper.get(expr.toString()).getName();
}

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return boundIds.get(
tom_get_slot_BoundIdentifier_boundIndex((( Expression )expr))).getName();

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_TRUE((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return "True";

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FALSE((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return "False";

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FunImage((( Expression )(( Expression )expr)))) {
 Expression  tom_f=tom_get_slot_FunImage_leftExpr((( Expression )expr));
 Expression  tom_x=tom_get_slot_FunImage_rightExpr((( Expression )expr));
if (tom_is_sort_Type(type)) {

// in case f is a local constant and we are translating an initialisation expression
if(mapper.containsKey(
tom_f.toString()+"@")) {
return mapper.get(
tom_f.toString()+"@").getName();
}
// we know that x is not a set nor a function
// we can therefore get the LB type of x with the Event-B type of x.
// the domain of f is a subset of the type of x
Type dom = exprToType(
tom_x.getType().toExpression());
return String.format("%s[%s]",
translateExpression(
tom_f, 
tom_make_Function(dom,(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors),
translateExpression(
tom_x, dom, mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
boolean tomMatch3_28= false ;
 Expression  tomMatch3_23= null ;
 Expression[]  tomMatch3_21= null ;
 Expression  tomMatch3_24= null ;
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Plus((( Expression )(( Expression )expr)))) {
{
tomMatch3_28= true ;
tomMatch3_23=(( Expression )expr);
tomMatch3_21=tom_get_slot_Plus_childExprs(tomMatch3_23);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Mul((( Expression )(( Expression )expr)))) {
{
tomMatch3_28= true ;
tomMatch3_24=(( Expression )expr);
tomMatch3_21=tom_get_slot_Mul_childExprs(tomMatch3_24);
}
}
}
}
}
if (tomMatch3_28) {
if (tom_is_fun_sym_eList((( Expression[] )tomMatch3_21))) {
 Expression[]  tom_elements=tom_get_slice_eList(tomMatch3_21,0,tom_get_size_eList_ExpressionList(tomMatch3_21));
if (tom_is_sort_Type(type)) {

List<String> resList = new ArrayList<String>();
for(int i = 0; i < 
tom_elements.length; i++) {
resList.add(translateExpression(
tom_elements[i], 
tom_make_Int(), mapper, boundIds, dan,
msgConstructors));
}
String symbol = "";
switch(expr.getTag()) {
case Formula.PLUS :
symbol = " + ";
break;
case Formula.MUL :
symbol = " * ";
break;
}
return resList.stream().collect(Collectors.joining(symbol, "(", ")"));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
boolean tomMatch3_38= false ;
 Expression  tomMatch3_36= null ;
 Expression  tomMatch3_31= null ;
 Expression  tomMatch3_34= null ;
 Expression  tomMatch3_32= null ;
 Expression  tomMatch3_35= null ;
 Expression  tomMatch3_37= null ;
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Minus((( Expression )(( Expression )expr)))) {
{
tomMatch3_38= true ;
tomMatch3_34=(( Expression )expr);
tomMatch3_31=tom_get_slot_Minus_leftExpr(tomMatch3_34);
tomMatch3_32=tom_get_slot_Minus_rightExpr(tomMatch3_34);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Div((( Expression )(( Expression )expr)))) {
{
tomMatch3_38= true ;
tomMatch3_35=(( Expression )expr);
tomMatch3_31=tom_get_slot_Div_leftExpr(tomMatch3_35);
tomMatch3_32=tom_get_slot_Div_rightExpr(tomMatch3_35);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Mod((( Expression )(( Expression )expr)))) {
{
tomMatch3_38= true ;
tomMatch3_36=(( Expression )expr);
tomMatch3_31=tom_get_slot_Mod_leftExpr(tomMatch3_36);
tomMatch3_32=tom_get_slot_Mod_rightExpr(tomMatch3_36);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Expn((( Expression )(( Expression )expr)))) {
{
tomMatch3_38= true ;
tomMatch3_37=(( Expression )expr);
tomMatch3_31=tom_get_slot_Expn_leftExpr(tomMatch3_37);
tomMatch3_32=tom_get_slot_Expn_rightExpr(tomMatch3_37);
}
}
}
}
}
}
}
}
}
if (tomMatch3_38) {
if (tom_is_sort_Type(type)) {

String symbol = "";
String lTrans = translateNotMsgExpression(
tomMatch3_31, 
tom_make_Int(), mapper, boundIds, dan, msgConstructors);
String rTrans = translateNotMsgExpression(
tomMatch3_32, 
tom_make_Int(), mapper, boundIds, dan, msgConstructors);
switch(expr.getTag()) {
case Formula.MINUS :
symbol = "-";
break;
case Formula.DIV :
return String.format("int(%s / %s)",lTrans, rTrans);
case Formula.MOD:
symbol = "%";
break;
case Formula.EXPN:
symbol = "**";
break;
}
return String.format("(%s %s %s)", lTrans, symbol, rTrans);

}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
boolean tomMatch3_45= false ;
 Expression  tomMatch3_44= null ;
 Expression  tomMatch3_43= null ;
 Expression  tomMatch3_41= null ;
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Min((( Expression )(( Expression )expr)))) {
{
tomMatch3_45= true ;
tomMatch3_43=(( Expression )expr);
tomMatch3_41=tom_get_slot_Min_childExpr(tomMatch3_43);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Max((( Expression )(( Expression )expr)))) {
{
tomMatch3_45= true ;
tomMatch3_44=(( Expression )expr);
tomMatch3_41=tom_get_slot_Max_childExpr(tomMatch3_44);
}
}
}
}
}
if (tomMatch3_45) {
if (tom_is_sort_Type(type)) {

String symbol = "";
switch(expr.getTag()) {
case Formula.KMIN :
symbol = "min";
break;
case Formula.KMAX :
symbol = "max";
break;
}
return String.format("%s(%s)", symbol, translateNotMsgExpression(
tomMatch3_41, 
tom_make_PowSet(tom_make_Int()),
mapper, boundIds, dan, msgConstructors));

}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Card((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return String.format("len(%s)",
translateExpression(
tom_get_slot_Card_childExpr((( Expression )expr)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

return 
translateFunctionExpr((( Expression )expr),tom_make_Function(tom_get_slot_Function_dom((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)),tom_get_slot_Function_ran((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))),mapper,boundIds,dan,msgConstructors);

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Type(type)) {

return 
translateSetExpr((( Expression )expr),(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type),mapper,boundIds,dan,msgConstructors);

}
}
}
}

return expr.toString();
}

// TODO complete
private static String translateFunctionExpr(Expression expr, Type type,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
if(mapper.containsKey(expr.toString())) {
return mapper.get(expr.toString()).getName();
}

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return boundIds.get(
tom_get_slot_BoundIdentifier_boundIndex((( Expression )expr))).getName();

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_EmptySet((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return "{}";

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch4_11=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch4_12=tom_get_slot_Cset_predicate((( Expression )expr));
 Expression  tomMatch4_13=tom_get_slot_Cset_expression((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch4_11))) {
int tomMatch4_32=0;
if (!(tomMatch4_32 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch4_11))) {
 BoundIdentDecl  tom_x=tom_get_element_bidList_BoundIdentDeclList(tomMatch4_11,tomMatch4_32);
if (tomMatch4_32 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch4_11)) {
if (tom_is_sort_Predicate(tomMatch4_12)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch4_12))) {
 Expression  tomMatch4_16=tom_get_slot_In_leftExpr(tomMatch4_12);
if (tom_is_sort_Expression(tomMatch4_16)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch4_16))) {
 Expression  tom_s=tom_get_slot_In_rightExpr(tomMatch4_12);
if (tom_is_sort_Expression(tomMatch4_13)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch4_13))) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {
 fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_dom=tom_get_slot_Function_dom((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type));
if (tom_equal_term_int(0, tom_get_slot_BoundIdentifier_boundIndex(tomMatch4_16))) {

// HashMap<String, DAVariable>	newMapper = new HashMap<String, DAVariable>(mapper);
// newMapper.put(`x.toString(), new DAVariable(`x.getName(), `s, null));
List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
newBoundIds.add(0, new DAVariable(
tom_x.getName(), 
tom_s, null));
return String.format("{%s:%s for %s in %s}",

translateExpression(tom_get_slot_Mapsto_leftExpr(tomMatch4_13),tom_dom,mapper,newBoundIds,dan,msgConstructors),

translateExpression(tom_get_slot_Mapsto_rightExpr(tomMatch4_13),tom_get_slot_Function_ran((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)),mapper,newBoundIds,dan,msgConstructors),

tom_x.getName(),

translateExpression(tom_s,tom_make_PowSet(tom_dom),mapper,newBoundIds,dan,msgConstructors));

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch4_36=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch4_37=tom_get_slot_Cset_predicate((( Expression )expr));
 Expression  tomMatch4_38=tom_get_slot_Cset_expression((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch4_36))) {
 BoundIdentDecl[]  tom_x=tom_get_slice_bidList(tomMatch4_36,0,tom_get_size_bidList_BoundIdentDeclList(tomMatch4_36));
if (tom_is_sort_Predicate(tomMatch4_37)) {
if (tom_is_fun_sym_Land((( Predicate )tomMatch4_37))) {
 Predicate[]  tomMatch4_41=tom_get_slot_Land_childPreds(tomMatch4_37);
if (tom_is_fun_sym_pList((( Predicate[] )tomMatch4_41))) {
 Predicate[]  tom_predl=tom_get_slice_pList(tomMatch4_41,0,tom_get_size_pList_PredicateList(tomMatch4_41));
if (tom_is_sort_Expression(tomMatch4_38)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch4_38))) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

Predicate[] bidDecl;
bidDecl = new Predicate[
tom_x.length];
System.arraycopy(
tom_predl, 0, bidDecl, 0, 
tom_x.length);
List<Predicate> otherPreds = new ArrayList<Predicate>();
for(int i = 
tom_x.length; i < 
tom_predl.length; i++) {
otherPreds.add(
tom_predl[i]);
}
List<DAVariable> newBoundIds = getBoundIdentifiers(
tom_x, bidDecl);
List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
for(DAVariable bid : newBoundIds) {
allBoundIds.add(0, bid);
}
return String.format("{%s:%s %s if %s}",

translateExpression(tom_get_slot_Mapsto_leftExpr(tomMatch4_38),tom_get_slot_Function_dom((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)),mapper,allBoundIds,dan,msgConstructors),

translateExpression(tom_get_slot_Mapsto_rightExpr(tomMatch4_38),tom_get_slot_Function_ran((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)),mapper,allBoundIds,dan,msgConstructors),
newBoundIds.stream()
.map(bid -> String.format("for %s in %s", bid.getName(),
translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
.collect(Collectors.joining(" ")),
translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));

}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_SetExtension((( Expression )(( Expression )expr)))) {
 Expression[]  tomMatch4_60=tom_get_slot_SetExtension_members((( Expression )expr));
if (tom_is_fun_sym_eList((( Expression[] )tomMatch4_60))) {
 Expression[]  tom_elements=tom_get_slice_eList(tomMatch4_60,0,tom_get_size_eList_ExpressionList(tomMatch4_60));
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

List<String> resList = new ArrayList<String>();
for(int i = 0; i < 
tom_elements.length; i++) {
if(
tom_elements[i].getTag() == Formula.MAPSTO) {
BinaryExpression bExpr = (BinaryExpression) 
tom_elements[i];
resList.add(String.format("%s:%s",
translateExpression(bExpr.getLeft(), 
tom_get_slot_Function_dom((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors),
translateExpression(bExpr.getRight(), 
tom_get_slot_Function_ran((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors)));
} else {
// TODO throw syntax error
return expr.toString();
}
}
return resList.stream().collect(Collectors.joining(", ", "{", "}"));

}
}
}
}
}
}
}
}
}

return expr.toString();
}

private static String translateSetExpr(Expression expr, Type type,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
if(mapper.containsKey(expr.toString())) {
return mapper.get(expr.toString()).getName();
}

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return boundIds.get(
tom_get_slot_BoundIdentifier_boundIndex((( Expression )expr))).getName();

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_EmptySet((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return "set()";

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch5_11=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch5_12=tom_get_slot_Cset_predicate((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch5_11))) {
int tomMatch5_27=0;
if (!(tomMatch5_27 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch5_11))) {
 BoundIdentDecl  tom_x=tom_get_element_bidList_BoundIdentDeclList(tomMatch5_11,tomMatch5_27);
if (tomMatch5_27 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch5_11)) {
if (tom_is_sort_Predicate(tomMatch5_12)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch5_12))) {
 Expression  tomMatch5_16=tom_get_slot_In_leftExpr(tomMatch5_12);
if (tom_is_sort_Expression(tomMatch5_16)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch5_16))) {
 Expression  tom_s=tom_get_slot_In_rightExpr(tomMatch5_12);
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {
if (tom_equal_term_int(0, tom_get_slot_BoundIdentifier_boundIndex(tomMatch5_16))) {

List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
newBoundIds.add(0, new DAVariable(
tom_x.getName(), 
tom_s, null));
Type sType = getType(
tom_s, mapper, boundIds, dan, msgConstructors);
return String.format("setof(%s, %s in %s)",
translateExpression(
tom_get_slot_Cset_expression((( Expression )expr)), 
tom_get_slot_PowSet_t((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, newBoundIds, dan, msgConstructors),

tom_x.getName(),
translateExpression(
tom_s, sType, mapper, newBoundIds, dan, msgConstructors)
);

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cset((( Expression )(( Expression )expr)))) {
 BoundIdentDecl[]  tomMatch5_31=tom_get_slot_Cset_identifiers((( Expression )expr));
 Predicate  tomMatch5_32=tom_get_slot_Cset_predicate((( Expression )expr));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch5_31))) {
 BoundIdentDecl[]  tom_x=tom_get_slice_bidList(tomMatch5_31,0,tom_get_size_bidList_BoundIdentDeclList(tomMatch5_31));
if (tom_is_sort_Predicate(tomMatch5_32)) {
if (tom_is_fun_sym_Land((( Predicate )tomMatch5_32))) {
 Predicate[]  tomMatch5_36=tom_get_slot_Land_childPreds(tomMatch5_32);
if (tom_is_fun_sym_pList((( Predicate[] )tomMatch5_36))) {
 Predicate[]  tom_predl=tom_get_slice_pList(tomMatch5_36,0,tom_get_size_pList_PredicateList(tomMatch5_36));
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

Predicate[] bidDecl;
bidDecl = new Predicate[
tom_x.length];
System.arraycopy(
tom_predl, 0, bidDecl, 0, 
tom_x.length);
List<Predicate> otherPreds = new ArrayList<Predicate>();
for(int i = 
tom_x.length; i < 
tom_predl.length; i++) {
otherPreds.add(
tom_predl[i]);
}
List<DAVariable> newBoundIds = getBoundIdentifiers(
tom_x, bidDecl);
List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
for(DAVariable bid : newBoundIds) {
allBoundIds.add(0, bid);
}
return String.format("setof(%s, %s, %s)",
translateExpression(
tom_get_slot_Cset_expression((( Expression )expr)), 
tom_get_slot_PowSet_t((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, allBoundIds, dan, msgConstructors),
newBoundIds.stream()
.map(bid -> String.format("%s in %s", bid.getName(),
translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
.collect(Collectors.joining(", "))
, translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));

}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_SetExtension((( Expression )(( Expression )expr)))) {
 Expression[]  tomMatch5_50=tom_get_slot_SetExtension_members((( Expression )expr));
if (tom_is_fun_sym_eList((( Expression[] )tomMatch5_50))) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

List<Expression> elements = Arrays.asList(
tom_get_slice_eList(tomMatch5_50,0,tom_get_size_eList_ExpressionList(tomMatch5_50)));
// return "{'translate(elmt[0])', 'translate(elmt[1])', ..., 'translate(elmt[last])'} 
return elements.stream()
.map(e -> translateExpression(e,
tom_get_slot_PowSet_t((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors))
.collect(Collectors.joining(", ", "{", "}"));

}
}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Dom((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

return String.format("set(%s.keys())",
translateExpression(
tom_get_slot_Dom_childExpr((( Expression )expr)), 
tom_get_slot_PowSet_t((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Ran((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)))) {

return String.format("set(%s.values())",
translateExpression(
tom_get_slot_Ran_childExpr((( Expression )expr)), 
tom_get_slot_PowSet_t((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
boolean tomMatch5_84= false ;
 Expression  tomMatch5_79= null ;
 Expression  tomMatch5_80= null ;
 Expression[]  tomMatch5_77= null ;
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BUnion((( Expression )(( Expression )expr)))) {
{
tomMatch5_84= true ;
tomMatch5_79=(( Expression )expr);
tomMatch5_77=tom_get_slot_BUnion_childExprs(tomMatch5_79);
}
} else {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BInter((( Expression )(( Expression )expr)))) {
{
tomMatch5_84= true ;
tomMatch5_80=(( Expression )expr);
tomMatch5_77=tom_get_slot_BInter_childExprs(tomMatch5_80);
}
}
}
}
}
if (tomMatch5_84) {
if (tom_is_fun_sym_eList((( Expression[] )tomMatch5_77))) {
if (tom_is_sort_Type(type)) {

List<Expression> setList = Arrays.asList(
tom_get_slice_eList(tomMatch5_77,0,tom_get_size_eList_ExpressionList(tomMatch5_77)));
String symbol = "";
switch(expr.getTag()) {
case Formula.BUNION :
symbol = " | ";
break;
case Formula.BINTER :
symbol = " & ";
break;
}
return setList.stream()
.map(s -> translateExpression(s, 
(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type), mapper, boundIds, dan, msgConstructors))
.collect(Collectors.joining(symbol, "(", ")"));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_SetMinus((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {
 fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_t=(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type);

return String.format("(%s - %s)",
translateExpression(
tom_get_slot_SetMinus_leftExpr((( Expression )expr)), 
tom_t, mapper, boundIds, dan, msgConstructors),
translateExpression(
tom_get_slot_SetMinus_rightExpr((( Expression )expr)), 
tom_t, mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Cprod((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {
 fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type  tom_t=(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )type);

return String.format("product(%s, %s)",
translateExpression(
tom_get_slot_Cprod_leftExpr((( Expression )expr)), 
tom_t, mapper, boundIds, dan, msgConstructors),
translateExpression(
tom_get_slot_Cprod_rightExpr((( Expression )expr)), 
tom_t, mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_UpTo((( Expression )(( Expression )expr)))) {
if (tom_is_sort_Type(type)) {

return String.format("set(range(%s, %s+1))",
translateExpression(
tom_get_slot_UpTo_leftExpr((( Expression )expr)), 
tom_make_Int(), mapper, boundIds, dan, msgConstructors),
translateExpression(
tom_get_slot_UpTo_rightExpr((( Expression )expr)), 
tom_make_Int(), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
}

return expr.toString();
}

public static String translateMessageExpression(Expression expr,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
if(mapper.containsKey(expr.toString())) {
return mapper.get(expr.toString()).getName();
}

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )(( Expression )expr)))) {

return boundIds.get(
tom_get_slot_BoundIdentifier_boundIndex((( Expression )expr))).getName();

}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FunImage((( Expression )(( Expression )expr)))) {
 Expression  tomMatch6_5=tom_get_slot_FunImage_leftExpr((( Expression )expr));
if (tom_is_sort_Expression(tomMatch6_5)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch6_5))) {

for(DAMessageConstructor msgCst : msgConstructors) {
if(msgCst.getName().equals(
tom_get_slot_FreeIdentifier_name(tomMatch6_5).toString())) {
return String.format("(\"%s\", %s)", msgCst.getName(),
translateTuple(
tom_get_slot_FunImage_rightExpr((( Expression )expr)), mapper, boundIds, dan, msgConstructors));
}
}

}
}
}
}
}
}
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )(( Expression )expr)))) {

return String.format("(\"%s\",)", 
tom_get_slot_FreeIdentifier_name((( Expression )expr)).toString());

}
}
}
}
}

return translateNotMsgExpression(expr, 
tom_make_StaticSet(dan.getMessages()), mapper,
boundIds, dan, msgConstructors);
}

private static String translateTuple(Expression expr,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_Mapsto((( Expression )(( Expression )expr)))) {

return String.format("%s, %s",
translateTuple(
tom_get_slot_Mapsto_leftExpr((( Expression )expr)), mapper, boundIds, dan, msgConstructors),
translateExpression(
tom_get_slot_Mapsto_rightExpr((( Expression )expr)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}

return translateExpression(expr, mapper, boundIds, dan, msgConstructors);
}

public static String translatePredicate(Predicate pred, HashMap<String, DAVariable> mapper,
List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {

{
{
if (tom_is_sort_Predicate(pred)) {
boolean tomMatch8_8= false ;
 Predicate[]  tomMatch8_1= null ;
 Predicate  tomMatch8_4= null ;
 Predicate  tomMatch8_3= null ;
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Land((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_8= true ;
tomMatch8_3=(( Predicate )pred);
tomMatch8_1=tom_get_slot_Land_childPreds(tomMatch8_3);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Lor((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_8= true ;
tomMatch8_4=(( Predicate )pred);
tomMatch8_1=tom_get_slot_Lor_childPreds(tomMatch8_4);
}
}
}
}
}
if (tomMatch8_8) {
if (tom_is_fun_sym_pList((( Predicate[] )tomMatch8_1))) {

List<Predicate> predList = Arrays.asList(
tom_get_slice_pList(tomMatch8_1,0,tom_get_size_pList_PredicateList(tomMatch8_1)));
return predList.stream()
.map(p -> translatePredicate(p, mapper, boundIds, dan, msgConstructors))
.collect(Collectors.joining(" and ", "(", ")"));

}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_NotB((( Predicate )(( Predicate )pred)))) {

return String.format("not(%s)", translatePredicate(
tom_get_slot_NotB_childPred((( Predicate )pred)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Limp((( Predicate )(( Predicate )pred)))) {

return String.format("(not(%s) or %s)",
translatePredicate(
tom_get_slot_Limp_leftPred((( Predicate )pred)), mapper, boundIds, dan, msgConstructors),
translatePredicate(
tom_get_slot_Limp_rightPred((( Predicate )pred)), mapper, boundIds, dan, msgConstructors));

}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
boolean tomMatch8_53= false ;
 Expression  tomMatch8_22= null ;
 Expression  tomMatch8_21= null ;
 Predicate  tomMatch8_24= null ;
 Predicate  tomMatch8_25= null ;
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Equal((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_53= true ;
tomMatch8_24=(( Predicate )pred);
tomMatch8_21=tom_get_slot_Equal_leftExpr(tomMatch8_24);
tomMatch8_22=tom_get_slot_Equal_rightExpr(tomMatch8_24);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Gt((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_53= true ;
tomMatch8_25=(( Predicate )pred);
tomMatch8_21=tom_get_slot_Gt_leftExpr(tomMatch8_25);
tomMatch8_22=tom_get_slot_Gt_rightExpr(tomMatch8_25);
}
}
}
}
}
if (tomMatch8_53) {
if (tom_is_sort_Expression(tomMatch8_21)) {
if (tom_is_fun_sym_FunImage((( Expression )tomMatch8_21))) {
 Expression  tomMatch8_26=tom_get_slot_FunImage_leftExpr(tomMatch8_21);
 Expression  tomMatch8_27=tom_get_slot_FunImage_rightExpr(tomMatch8_21);
if (tom_is_sort_Expression(tomMatch8_26)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch8_26))) {
 String  tom_comFunction=tom_get_slot_FreeIdentifier_name(tomMatch8_26);
if (tom_is_sort_Expression(tomMatch8_27)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch8_27))) {
 Expression  tomMatch8_33=tom_get_slot_Mapsto_leftExpr(tomMatch8_27);
if (tom_is_sort_Expression(tomMatch8_33)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch8_33))) {
 Expression  tomMatch8_37=tom_get_slot_Mapsto_leftExpr(tomMatch8_33);
 Expression  tomMatch8_38=tom_get_slot_Mapsto_rightExpr(tomMatch8_33);
if (tom_is_sort_Expression(tomMatch8_37)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch8_37))) {
if (tom_is_sort_Expression(tomMatch8_38)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch8_38))) {
 Expression  tom_p=tom_get_slot_Mapsto_leftExpr(tomMatch8_38);
 Expression  tom_q=tom_get_slot_Mapsto_rightExpr(tomMatch8_38);
 Expression  tom_msg=tom_get_slot_Mapsto_rightExpr(tomMatch8_27);
if (tom_is_sort_Expression(tomMatch8_22)) {
if (tom_is_fun_sym_IntegerLiteral((( Expression )tomMatch8_22))) {
 String  tomMatch8_19=dan.getChannels();
if ( true ) {
if (tom_equal_term_String(tom_get_slot_FreeIdentifier_name(tomMatch8_37), (( String )tomMatch8_19))) {
if (tom_is_sort_BigInteger(BigInteger.ZERO)) {
if (tom_equal_term_BigInteger(tom_get_slot_IntegerLiteral_value(tomMatch8_22), (( BigInteger )BigInteger.ZERO))) {


HashMap<String, DAVariable> newMapper = new HashMap<String, DAVariable>(mapper);
for(Entry<String, DAVariable> var : newMapper.entrySet()) {
if(!var.getValue().getName().contains(".")) { 
var.setValue(new DAVariable("_" + var.getValue().getName(),
var.getValue().getType(), var.getValue().getInitialValue()));
}
}
List<DAVariable> newBoundIds = boundIds.stream().map(v -> new DAVariable("_" + v.getName(), v.getType(), null))
.collect(Collectors.toList());
// case sent
String res = "";
if(
tom_comFunction.equals(dan.getSent())) {
res = String.format("some(sent(%s, to=%s))",
translateMessageExpression(
tom_msg, newMapper, newBoundIds, dan, msgConstructors),
translateExpression(
tom_q, getType(
tom_q, newMapper, newBoundIds, dan, msgConstructors), newMapper, newBoundIds, dan, msgConstructors));
}
// case received
else if(
tom_comFunction.equals(dan.getReceived())) {
res = String.format("some(received(%s, _from=%s))",
translateMessageExpression(
tom_msg, newMapper, newBoundIds, dan, msgConstructors),
translateExpression(
tom_p, getType(
tom_p, newMapper, newBoundIds, dan, msgConstructors),
newMapper, newBoundIds, dan, msgConstructors));
}
if(pred.getTag() == Formula.EQUAL) {
res = "not(" + res + ")";
}
return res;

}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
boolean tomMatch8_60= false ;
 Expression  tomMatch8_56= null ;
 Predicate  tomMatch8_59= null ;
 Expression  tomMatch8_55= null ;
 Predicate  tomMatch8_58= null ;
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Equal((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_60= true ;
tomMatch8_58=(( Predicate )pred);
tomMatch8_55=tom_get_slot_Equal_leftExpr(tomMatch8_58);
tomMatch8_56=tom_get_slot_Equal_rightExpr(tomMatch8_58);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_NotEqual((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_60= true ;
tomMatch8_59=(( Predicate )pred);
tomMatch8_55=tom_get_slot_NotEqual_leftExpr(tomMatch8_59);
tomMatch8_56=tom_get_slot_NotEqual_rightExpr(tomMatch8_59);
}
}
}
}
}
if (tomMatch8_60) {
 Expression  tom_l=tomMatch8_55;
 Expression  tom_r=tomMatch8_56;

String symbol = "";
switch(pred.getTag()) {
case Formula.EQUAL :
symbol = "==";
break;
case Formula.NOTEQUAL :
symbol = "!=";
break;
}
Type lt = getType(
tom_l, mapper, boundIds, dan, msgConstructors);
Type rt = getType(
tom_r, mapper, boundIds, dan, msgConstructors);
String rexp = "";
String lexp = "";

{
{
if (tom_is_sort_Type(lt)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )lt))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )lt)))) {
if (tom_is_sort_Type(rt)) {

lexp = translateExpression(
tom_l, lt, mapper, boundIds, dan, msgConstructors);
rexp = translateExpression(
tom_r, lt, mapper, boundIds, dan, msgConstructors);
return String.format("%s %s %s", lexp, symbol, rexp);

}
}
}
}
}
{
if (tom_is_sort_Type(lt)) {
if (tom_is_sort_Type(rt)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )rt))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )rt)))) {

lexp = translateExpression(
tom_l, rt, mapper, boundIds, dan, msgConstructors);
rexp = translateExpression(
tom_r, rt, mapper, boundIds, dan, msgConstructors);
return String.format("%s %s %s", lexp, symbol, rexp);

}
}
}
}
}
{
if (tom_is_sort_Type(lt)) {
if (tom_is_sort_Type(rt)) {

lexp = translateExpression(
tom_l, lt, mapper, boundIds, dan, msgConstructors);
rexp = translateExpression(
tom_r, rt, mapper, boundIds, dan, msgConstructors);
return String.format("%s %s %s", lexp, symbol, rexp);

}
}
}
}


}
}
}
{
if (tom_is_sort_Predicate(pred)) {
boolean tomMatch8_69= false ;
 Expression  tomMatch8_62= null ;
 Expression  tomMatch8_63= null ;
 Predicate  tomMatch8_68= null ;
 Predicate  tomMatch8_65= null ;
 Predicate  tomMatch8_67= null ;
 Predicate  tomMatch8_66= null ;
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Lt((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_69= true ;
tomMatch8_65=(( Predicate )pred);
tomMatch8_62=tom_get_slot_Lt_leftExpr(tomMatch8_65);
tomMatch8_63=tom_get_slot_Lt_rightExpr(tomMatch8_65);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Le((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_69= true ;
tomMatch8_66=(( Predicate )pred);
tomMatch8_62=tom_get_slot_Le_leftExpr(tomMatch8_66);
tomMatch8_63=tom_get_slot_Le_rightExpr(tomMatch8_66);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Gt((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_69= true ;
tomMatch8_67=(( Predicate )pred);
tomMatch8_62=tom_get_slot_Gt_leftExpr(tomMatch8_67);
tomMatch8_63=tom_get_slot_Gt_rightExpr(tomMatch8_67);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Ge((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_69= true ;
tomMatch8_68=(( Predicate )pred);
tomMatch8_62=tom_get_slot_Ge_leftExpr(tomMatch8_68);
tomMatch8_63=tom_get_slot_Ge_rightExpr(tomMatch8_68);
}
}
}
}
}
}
}
}
}
if (tomMatch8_69) {

String symbol = "";
switch(pred.getTag()) {
case Formula.LT:
symbol = "<";
break;
case Formula.LE:
symbol = "<=";
break;
case Formula.GT:
symbol = ">";
break;
case Formula.GE:
symbol = ">=";
break;
}
return String.format("%s %s %s",
translateNotMsgExpression(
tomMatch8_62, 
tom_make_Int(), mapper, boundIds, dan, msgConstructors),
symbol,
translateNotMsgExpression(
tomMatch8_63, 
tom_make_Int(), mapper, boundIds, dan, msgConstructors));

}
}
}
{
if (tom_is_sort_Predicate(pred)) {
boolean tomMatch8_80= false ;
 Predicate  tomMatch8_79= null ;
 Predicate  tomMatch8_75= null ;
 Predicate  tomMatch8_76= null ;
 Predicate  tomMatch8_77= null ;
 Expression  tomMatch8_71= null ;
 Predicate  tomMatch8_78= null ;
 Expression  tomMatch8_72= null ;
 Predicate  tomMatch8_74= null ;
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_In((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_74=(( Predicate )pred);
tomMatch8_71=tom_get_slot_In_leftExpr(tomMatch8_74);
tomMatch8_72=tom_get_slot_In_rightExpr(tomMatch8_74);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_NotIn((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_75=(( Predicate )pred);
tomMatch8_71=tom_get_slot_NotIn_leftExpr(tomMatch8_75);
tomMatch8_72=tom_get_slot_NotIn_rightExpr(tomMatch8_75);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Subset((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_76=(( Predicate )pred);
tomMatch8_71=tom_get_slot_Subset_leftExpr(tomMatch8_76);
tomMatch8_72=tom_get_slot_Subset_rightExpr(tomMatch8_76);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_NotSubset((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_77=(( Predicate )pred);
tomMatch8_71=tom_get_slot_NotSubset_leftExpr(tomMatch8_77);
tomMatch8_72=tom_get_slot_NotSubset_rightExpr(tomMatch8_77);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_SubsetEq((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_78=(( Predicate )pred);
tomMatch8_71=tom_get_slot_SubsetEq_leftExpr(tomMatch8_78);
tomMatch8_72=tom_get_slot_SubsetEq_rightExpr(tomMatch8_78);
}
} else {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_NotSubsetEq((( Predicate )(( Predicate )pred)))) {
{
tomMatch8_80= true ;
tomMatch8_79=(( Predicate )pred);
tomMatch8_71=tom_get_slot_NotSubsetEq_leftExpr(tomMatch8_79);
tomMatch8_72=tom_get_slot_NotSubsetEq_rightExpr(tomMatch8_79);
}
}
}
}
}
}
}
}
}
}
}
}
}
if (tomMatch8_80) {

String leTrans = translateExpression(
tomMatch8_71, mapper, boundIds, dan, msgConstructors);
String reTrans = translateExpression(
tomMatch8_72, mapper, boundIds, dan, msgConstructors);
String symbol = "";
Boolean negation = true;
switch(pred.getTag()) {
case Formula.IN :
negation = false;
case Formula.NOTIN :
symbol = "in";
break;
case Formula.SUBSET :
negation = false;
case Formula.NOTSUBSET :
symbol = "<";
break;
case Formula.SUBSETEQ :
negation = false;
case Formula.NOTSUBSETEQ :
symbol = "<=";
break;
}
String code = String.format("%s %s %s", leTrans, symbol, reTrans);
if(negation) {
code = "not("+code+")";
}
return code;

}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_ForAll((( Predicate )(( Predicate )pred)))) {
 BoundIdentDecl[]  tomMatch8_82=tom_get_slot_ForAll_identifiers((( Predicate )pred));
 Predicate  tomMatch8_83=tom_get_slot_ForAll_predicate((( Predicate )pred));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch8_82))) {
int tomMatch8_98=0;
if (!(tomMatch8_98 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch8_82))) {
 BoundIdentDecl  tom_x=tom_get_element_bidList_BoundIdentDeclList(tomMatch8_82,tomMatch8_98);
if (tomMatch8_98 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch8_82)) {
if (tom_is_sort_Predicate(tomMatch8_83)) {
if (tom_is_fun_sym_Limp((( Predicate )tomMatch8_83))) {
 Predicate  tomMatch8_86=tom_get_slot_Limp_leftPred(tomMatch8_83);
if (tom_is_sort_Predicate(tomMatch8_86)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch8_86))) {
 Expression  tomMatch8_90=tom_get_slot_In_leftExpr(tomMatch8_86);
if (tom_is_sort_Expression(tomMatch8_90)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch8_90))) {
 Expression  tom_s=tom_get_slot_In_rightExpr(tomMatch8_86);
if (tom_equal_term_int(0, tom_get_slot_BoundIdentifier_boundIndex(tomMatch8_90))) {

List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
newBoundIds.add(0, new DAVariable(
tom_x.getName(), 
tom_s, null));
return String.format("each(%s in %s, has=%s)", 
tom_x.getName()
, translateExpression(
tom_s, mapper, newBoundIds, dan, msgConstructors)
, translatePredicate(
tom_get_slot_Limp_rightPred(tomMatch8_83), mapper, newBoundIds, dan, msgConstructors));


}
}
}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_ForAll((( Predicate )(( Predicate )pred)))) {
 BoundIdentDecl[]  tomMatch8_101=tom_get_slot_ForAll_identifiers((( Predicate )pred));
 Predicate  tomMatch8_102=tom_get_slot_ForAll_predicate((( Predicate )pred));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch8_101))) {
 BoundIdentDecl[]  tom_x=tom_get_slice_bidList(tomMatch8_101,0,tom_get_size_bidList_BoundIdentDeclList(tomMatch8_101));
if (tom_is_sort_Predicate(tomMatch8_102)) {
if (tom_is_fun_sym_Limp((( Predicate )tomMatch8_102))) {
 Predicate  tomMatch8_105=tom_get_slot_Limp_leftPred(tomMatch8_102);
if (tom_is_sort_Predicate(tomMatch8_105)) {
if (tom_is_fun_sym_Land((( Predicate )tomMatch8_105))) {
 Predicate[]  tomMatch8_109=tom_get_slot_Land_childPreds(tomMatch8_105);
if (tom_is_fun_sym_pList((( Predicate[] )tomMatch8_109))) {
 Predicate[]  tom_predl=tom_get_slice_pList(tomMatch8_109,0,tom_get_size_pList_PredicateList(tomMatch8_109));

if(
tom_predl.length != 
tom_x.length) {
// TODO throw syntax error
return pred.toString();
}
List<DAVariable> newBoundIds = getBoundIdentifiers(
tom_x, 
tom_predl);
List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
for(DAVariable bid : newBoundIds) {
allBoundIds.add(0, bid);
}
return String.format("each(%s, has=%s)",
newBoundIds.stream()
.map(bid -> String.format("%s in %s", bid.getName(),
translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
.collect(Collectors.joining(", ")),
translatePredicate(
tom_get_slot_Limp_rightPred(tomMatch8_102), mapper, allBoundIds, dan, msgConstructors));				

}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Exists((( Predicate )(( Predicate )pred)))) {
 BoundIdentDecl[]  tomMatch8_119=tom_get_slot_Exists_identifiers((( Predicate )pred));
 Predicate  tomMatch8_120=tom_get_slot_Exists_predicate((( Predicate )pred));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch8_119))) {
int tomMatch8_131=0;
if (!(tomMatch8_131 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch8_119))) {
if (tomMatch8_131 + 1 >= tom_get_size_bidList_BoundIdentDeclList(tomMatch8_119)) {
if (tom_is_sort_Predicate(tomMatch8_120)) {
if (tom_is_fun_sym_In((( Predicate )tomMatch8_120))) {
 Expression  tomMatch8_123=tom_get_slot_In_leftExpr(tomMatch8_120);
if (tom_is_sort_Expression(tomMatch8_123)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch8_123))) {
if (tom_equal_term_int(0, tom_get_slot_BoundIdentifier_boundIndex(tomMatch8_123))) {

return String.format("some(%s in %s)", 
tom_get_element_bidList_BoundIdentDeclList(tomMatch8_119,tomMatch8_131).getName()
, translateExpression(
tom_get_slot_In_rightExpr(tomMatch8_120), mapper, boundIds, dan, msgConstructors));

}
}
}
}
}
}
}
}
}
}
}
}
{
if (tom_is_sort_Predicate(pred)) {
if (tom_is_sort_Predicate((( Predicate )pred))) {
if (tom_is_fun_sym_Exists((( Predicate )(( Predicate )pred)))) {
 BoundIdentDecl[]  tomMatch8_134=tom_get_slot_Exists_identifiers((( Predicate )pred));
 Predicate  tomMatch8_135=tom_get_slot_Exists_predicate((( Predicate )pred));
if (tom_is_fun_sym_bidList((( BoundIdentDecl[] )tomMatch8_134))) {
 BoundIdentDecl[]  tom_x=tom_get_slice_bidList(tomMatch8_134,0,tom_get_size_bidList_BoundIdentDeclList(tomMatch8_134));
if (tom_is_sort_Predicate(tomMatch8_135)) {
if (tom_is_fun_sym_Land((( Predicate )tomMatch8_135))) {
 Predicate[]  tomMatch8_138=tom_get_slot_Land_childPreds(tomMatch8_135);
if (tom_is_fun_sym_pList((( Predicate[] )tomMatch8_138))) {
 Predicate[]  tom_predl=tom_get_slice_pList(tomMatch8_138,0,tom_get_size_pList_PredicateList(tomMatch8_138));

Predicate[] bidDecl;
bidDecl = new Predicate[
tom_x.length];
System.arraycopy(
tom_predl, 0, bidDecl, 0, 
tom_x.length);
List<Predicate> otherPreds = new ArrayList<Predicate>();
for(int i = 
tom_x.length; i < 
tom_predl.length; i++) {
otherPreds.add(
tom_predl[i]);
}
List<DAVariable> newBoundIds = getBoundIdentifiers(
tom_x, bidDecl);
List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
for(DAVariable bid : newBoundIds) {
allBoundIds.add(0, bid);
}
return String.format("some(%s, has=%s)",
newBoundIds.stream()
.map(bid -> String.format("%s in %s", bid.getName(),
translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
.collect(Collectors.joining(", "))
, translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));	

}
}
}
}
}
}
}
}
}

return pred.toString();
}

private static List<DAVariable> getBoundIdentifiers(BoundIdentDecl[] x, Predicate[] pred) {
List<DAVariable> newBoundIds = new ArrayList<DAVariable>();
if(pred.length != x.length) {
// TODO throw syntax error
return newBoundIds;
}
for(int i = 0; i < x.length; i++) {
Predicate predi = pred[i];

{
{
if (tom_is_sort_Predicate(predi)) {
if (tom_is_sort_Predicate((( Predicate )predi))) {
if (tom_is_fun_sym_In((( Predicate )(( Predicate )predi)))) {
 Expression  tomMatch10_1=tom_get_slot_In_leftExpr((( Predicate )predi));
if (tom_is_sort_Expression(tomMatch10_1)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch10_1))) {

if(
tom_get_slot_BoundIdentifier_boundIndex(tomMatch10_1)== x.length-i-1) {
newBoundIds.add(new DAVariable(x[i].getName(), 
tom_get_slot_In_rightExpr((( Predicate )predi)), null));
} else {
// TODO throw syntax error
return newBoundIds;
}

}
}
}
}
}
}
{
if (tom_is_sort_Predicate(predi)) {
boolean tomMatch10_16= false ;
if (tom_is_sort_Predicate((( Predicate )predi))) {
if (tom_is_fun_sym_In((( Predicate )(( Predicate )predi)))) {
 Expression  tomMatch10_9=tom_get_slot_In_leftExpr((( Predicate )predi));
if (tom_is_sort_Expression(tomMatch10_9)) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )tomMatch10_9))) {
tomMatch10_16= true ;
}
}
}
}
if (!(tomMatch10_16)) {

// TODO throw syntax error
return newBoundIds;

}
}
}
}

}
return newBoundIds;
}

public static String translatePredicateList(List<Predicate> plist,
HashMap<String, DAVariable> mapper, DANames dan,
List<DAMessageConstructor> msgConstructors) {
return translatePredicateList(plist, mapper, new ArrayList<DAVariable>(), dan, msgConstructors);
}

private static String translatePredicateList(List<Predicate> plist,
HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
List<DAMessageConstructor> msgConstructors) {
if(plist.isEmpty()) {
return "True";
}
return plist.stream().
map(p -> translatePredicate(p, mapper, boundIds, dan, msgConstructors))
.collect(Collectors.joining(") and (", "(", ")"));
}

private static List<String> translateCommunicationActions(Expression comExpr, List<String> res,
HashMap<String, DAVariable> mapper,	DANames dan, List<DAMessageConstructor> msgConstructors) {

{
{
if (tom_is_sort_Expression(comExpr)) {
if (tom_is_sort_Expression((( Expression )comExpr))) {
if (tom_is_fun_sym_FunImage((( Expression )(( Expression )comExpr)))) {
 Expression  tomMatch11_1=tom_get_slot_FunImage_leftExpr((( Expression )comExpr));
 Expression  tomMatch11_2=tom_get_slot_FunImage_rightExpr((( Expression )comExpr));
if (tom_is_sort_Expression(tomMatch11_1)) {
if (tom_is_fun_sym_FreeIdentifier((( Expression )tomMatch11_1))) {
if (tom_is_sort_Expression(tomMatch11_2)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch11_2))) {
 Expression  tomMatch11_8=tom_get_slot_Mapsto_leftExpr(tomMatch11_2);
if (tom_is_sort_Expression(tomMatch11_8)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch11_8))) {
 Expression  tomMatch11_13=tom_get_slot_Mapsto_rightExpr(tomMatch11_8);
if (tom_is_sort_Expression(tomMatch11_13)) {
if (tom_is_fun_sym_Mapsto((( Expression )tomMatch11_13))) {
 Expression  tom_dest=tom_get_slot_Mapsto_rightExpr(tomMatch11_13);

res = translateCommunicationActions(
tom_get_slot_Mapsto_leftExpr(tomMatch11_8), res, mapper, dan, msgConstructors);
if(
tom_get_slot_FreeIdentifier_name(tomMatch11_1).equals(dan.getSend())) {
List<DAVariable> bids = new ArrayList<DAVariable>();
res.add(String.format("send(%s, to=%s)",
translateMessageExpression(
tom_get_slot_Mapsto_rightExpr(tomMatch11_2), mapper, bids, dan, msgConstructors),
translateExpression(
tom_dest, getType(
tom_dest, mapper, bids, dan, msgConstructors),
mapper, bids, dan, msgConstructors)));
}

}
}
}
}
}
}
}
}
}
}
}
}
}

return res;
}

public static List<String> translateActionList(List<DAAction> actList, HashMap<String, DAVariable> mapper,
DANames dan, List<DAMessageConstructor> msgConstructors) {
List<String> res = new ArrayList<String>();
DAAction comAction = null;
List<DAVariable> boundIds = new ArrayList<DAVariable>();
HashMap<String, DAVariable> newMapper = new HashMap<String, DAVariable>(mapper);
for(DAAction act : actList) {
// Communication action case
if(act.getAssignedVariable().equals(dan.getChannels())) {
comAction = act;
}
// case var(proc) := var(proc) \ovr g
else if(act.getAssignmentExpression().getTag() == Formula.OVR) {
// translation into self.var.updates(deepcopy(g))
AssociativeExpression assExpr = (AssociativeExpression) act.getAssignmentExpression();
if(assExpr.getChildren().length == 2
&& assExpr.getChildren()[0].toString().equals(act.getAssignedVariable())) {
// TODO what if the key is not in the mapper?
String tmp_var = mapper.get(act.getAssignedVariable()).getName() + "_tmp_lbda";
if(tmp_var.startsWith("self.")) {
tmp_var = tmp_var.substring(5);
}
res.add(String.format("%s = deepcopy(%s)", tmp_var,
translateExpression(assExpr.getChildren()[0], mapper, boundIds,
dan, msgConstructors)));
newMapper.replace(assExpr.getChildren()[0].toString(),
new DAVariable(tmp_var, mapper.get(assExpr.getChildren()[0].toString()).getType(), null));
res.add(String.format("%s.update(deepcopy(%s))",
translateExpression(assExpr.getChildren()[0], mapper, boundIds,
dan, msgConstructors),
translateFunctionExpr(assExpr.getChildren()[1],
getFunctionType(assExpr.getChildren()[1]),
newMapper, boundIds, dan, msgConstructors)));
} else {
// TODO throw syntax error
res.add(act.getAssignedVariable() + "=" + act.getAssignmentExpression());
}
} else if(mapper.containsKey(act.getAssignedVariable())) {
Type varType = exprToType(mapper.get(act.getAssignedVariable()).getType());
String tmp_var = mapper.get(act.getAssignedVariable()).getName() + "_tmp_lbda";
if(tmp_var.startsWith("self.")) {
tmp_var = tmp_var.substring(5);
}
newMapper.replace(act.getAssignedVariable(),
new DAVariable(tmp_var, mapper.get(act.getAssignedVariable()).getType(), null));
res.add(String.format("%s = deepcopy(%s)", tmp_var,
mapper.get(act.getAssignedVariable()).getName()));
String assignmentString = translateExpression(act.getAssignmentExpression(),
varType, newMapper, boundIds, dan, msgConstructors);

{
{
if (tom_is_sort_Type(varType)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )varType))) {
if (tom_is_fun_sym_Function((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )varType)))) {

assignmentString = String.format("deepcopy(%s)", assignmentString);

}
}
}
}
{
if (tom_is_sort_Type(varType)) {
if (tom_is_sort_Type((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )varType))) {
if (tom_is_fun_sym_PowSet((( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )(( fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type )varType)))) {

assignmentString = String.format("set(%s)", assignmentString);

}
}
}
}
}

res.add(String.format("%s = %s", mapper.get(act.getAssignedVariable()).getName(),
assignmentString));
} else {
// TODO throw syntax error
res.add(act.getAssignedVariable() + " = " + act.getAssignmentExpression());
}
}
if(comAction != null) {
res.addAll(translateCommunicationActions(comAction.getAssignmentExpression(), new ArrayList<String>(), newMapper,
dan, msgConstructors));
}
return res;
}

private static Type getType(Expression expr, HashMap<String, DAVariable> mapper,
List<DAVariable> boundIds, DANames dan, List<DAMessageConstructor> msgConstructors) {
// TODO complete
if(mapper.containsKey(expr.toString())) {
return exprToType(mapper.get(expr.toString()).getType());
}

{
{
if (tom_is_sort_Expression(expr)) {
if (tom_is_sort_Expression((( Expression )expr))) {
if (tom_is_fun_sym_BoundIdentifier((( Expression )(( Expression )expr)))) {

return exprToType(boundIds.get(
tom_get_slot_BoundIdentifier_boundIndex((( Expression )expr))).getType());

}
}
}
}
}

return exprToType(expr.getType().toExpression());
}

private static Type getFunctionType(Expression f) {
Expression fType = f.getType().toExpression();

{
{
if (tom_is_sort_Expression(fType)) {
if (tom_is_sort_Expression((( Expression )fType))) {
if (tom_is_fun_sym_Pow((( Expression )(( Expression )fType)))) {
 Expression  tomMatch14_1=tom_get_slot_Pow_childExpr((( Expression )fType));
if (tom_is_sort_Expression(tomMatch14_1)) {
if (tom_is_fun_sym_Cprod((( Expression )tomMatch14_1))) {

return 
tom_make_Function(exprToType(tom_get_slot_Cprod_leftExpr(tomMatch14_1)),exprToType(tom_get_slot_Cprod_rightExpr(tomMatch14_1)));

}
}
}
}
}
}
}

return exprToType(fType);
}
}
