
package fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types;


public abstract class Type extends fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.TypesAbstractType  {
  /**
   * Sole constructor.  (For invocation by subclass
   * constructors, typically implicit.)
   */
  protected Type() {}



  /**
   * Returns true if the term is rooted by the symbol Function
   *
   * @return true if the term is rooted by the symbol Function
   */
  public boolean isFunction() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol PowSet
   *
   * @return true if the term is rooted by the symbol PowSet
   */
  public boolean isPowSet() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol ProdSet
   *
   * @return true if the term is rooted by the symbol ProdSet
   */
  public boolean isProdSet() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol Int
   *
   * @return true if the term is rooted by the symbol Int
   */
  public boolean isInt() {
    return false;
  }

  /**
   * Returns true if the term is rooted by the symbol StaticSet
   *
   * @return true if the term is rooted by the symbol StaticSet
   */
  public boolean isStaticSet() {
    return false;
  }

  /**
   * Returns the subterm corresponding to the slot t
   *
   * @return the subterm corresponding to the slot t
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type gett() {
    throw new UnsupportedOperationException("This Type has no t");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot t
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot t is replaced by _arg
   */
  public Type sett(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no t");
  }

  /**
   * Returns the subterm corresponding to the slot rt
   *
   * @return the subterm corresponding to the slot rt
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type getrt() {
    throw new UnsupportedOperationException("This Type has no rt");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot rt
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot rt is replaced by _arg
   */
  public Type setrt(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no rt");
  }

  /**
   * Returns the subterm corresponding to the slot dom
   *
   * @return the subterm corresponding to the slot dom
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type getdom() {
    throw new UnsupportedOperationException("This Type has no dom");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot dom
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot dom is replaced by _arg
   */
  public Type setdom(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no dom");
  }

  /**
   * Returns the subterm corresponding to the slot ran
   *
   * @return the subterm corresponding to the slot ran
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type getran() {
    throw new UnsupportedOperationException("This Type has no ran");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot ran
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot ran is replaced by _arg
   */
  public Type setran(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no ran");
  }

  /**
   * Returns the subterm corresponding to the slot lt
   *
   * @return the subterm corresponding to the slot lt
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type getlt() {
    throw new UnsupportedOperationException("This Type has no lt");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot lt
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot lt is replaced by _arg
   */
  public Type setlt(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type _arg) {
    throw new UnsupportedOperationException("This Type has no lt");
  }

  /**
   * Returns the subterm corresponding to the slot id
   *
   * @return the subterm corresponding to the slot id
   */
  public String getid() {
    throw new UnsupportedOperationException("This Type has no id");
  }

  /**
   * Returns a new term where the subterm corresponding to the slot id
   * is replaced by the term given in argument.
   * Note that there is no side-effect: a new term is returned and the original term is left unchanged
   *
   * @param _arg the value of the new subterm
   * @return a new term where the subterm corresponding to the slot id is replaced by _arg
   */
  public Type setid(String _arg) {
    throw new UnsupportedOperationException("This Type has no id");
  }

  protected static tom.library.utils.IdConverter idConv = new tom.library.utils.IdConverter();

  /**
   * Returns an ATerm representation of this term.
   *
   * @return null to indicate to sub-classes that they have to work
   */
  public aterm.ATerm toATerm() {
    // returns null to indicate sub-classes that they have to work
    return null;
  }

  /**
   * Returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type from an ATerm without any conversion
   *
   * @param trm ATerm to handle to retrieve a Gom term
   * @return the term from the ATerm
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromTerm(aterm.ATerm trm) {
    return fromTerm(trm,idConv);
  }

  /**
   * Returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type from a String without any conversion
   *
   * @param s String containing the ATerm
   * @return the term from the String
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromString(String s) {
    return fromTerm(atermFactory.parse(s),idConv);
  }

  /**
   * Returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type from a Stream without any conversion
   *
   * @param stream stream containing the ATerm
   * @return the term from the Stream
   * @throws java.io.IOException if a problem occurs with the stream
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromStream(java.io.InputStream stream) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),idConv);
  }

  /**
   * Apply a conversion on the ATerm and returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type
   *
   * @param trm ATerm to convert into a Gom term
   * @param atConv ATermConverter used to convert the ATerm
   * @return the Gom term
   * @throws IllegalArgumentException
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromTerm(aterm.ATerm trm, tom.library.utils.ATermConverter atConv) {
    aterm.ATerm convertedTerm = atConv.convert(trm);
    fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type tmp;
    java.util.ArrayList<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type> results = new java.util.ArrayList<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type>();

    tmp = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Function.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.PowSet.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.ProdSet.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Int.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    tmp = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.StaticSet.fromTerm(convertedTerm,atConv);
    if(tmp!=null) {
      results.add(tmp);
    }
    switch(results.size()) {
      case 0:
        throw new IllegalArgumentException(trm + " is not a Type");
      case 1:
        return results.get(0);
      default:
        java.util.logging.Logger.getLogger("Type").log(java.util.logging.Level.WARNING,"There were many possibilities ({0}) in {1} but the first one was chosen: {2}",new Object[] {results.toString(), "fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type", results.get(0).toString()});
        return results.get(0);
    }
  }

  /**
   * Apply a conversion on the ATerm contained in the String and returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type from it
   *
   * @param s String containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromString(String s, tom.library.utils.ATermConverter atConv) {
    return fromTerm(atermFactory.parse(s),atConv);
  }

  /**
   * Apply a conversion on the ATerm contained in the Stream and returns a fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type from it
   *
   * @param stream stream containing the ATerm
   * @param atConv ATerm Converter used to convert the ATerm
   * @return the Gom term
   */
  public static fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type fromStream(java.io.InputStream stream, tom.library.utils.ATermConverter atConv) throws java.io.IOException {
    return fromTerm(atermFactory.readFromFile(stream),atConv);
  }

  /**
   * Returns the length of the list
   *
   * @return the length of the list
   * @throws IllegalArgumentException if the term is not a list
   */
  public int length() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  /**
   * Returns an inverted term
   *
   * @return the inverted list
   * @throws IllegalArgumentException if the term is not a list
   */
  public fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type reverse() {
    throw new IllegalArgumentException(
      "This "+this.getClass().getName()+" is not a list");
  }

  
  /*
   * Initialize the (cyclic) data-structure
   * in order to generate/enumerate terms
   */

  protected static tom.library.enumerator.Enumeration<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type> enumType = null;
  public static final tom.library.enumerator.Enumeration<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type> tmpenumType = new tom.library.enumerator.Enumeration<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type>((tom.library.enumerator.LazyList<tom.library.enumerator.Finite<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type>>) null);

  public static tom.library.enumerator.Enumeration<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type> getEnumeration() {
    if(enumType == null) { 
      enumType = fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Function.funMake().apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType).apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType)
        .plus(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.PowSet.funMake().apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType))
        .plus(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.ProdSet.funMake().apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType).apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType))
        .plus(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.Int.funMake().apply(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type.tmpenumType))
        .plus(fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.type.StaticSet.funMake().apply(tom.library.enumerator.Combinators.makeString()));


      tmpenumType.p1 = new tom.library.enumerator.P1<tom.library.enumerator.LazyList<tom.library.enumerator.Finite<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type>>>() {
        public tom.library.enumerator.LazyList<tom.library.enumerator.Finite<fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.Type>> _1() { return enumType.parts(); }
      };

    }
    return enumType;
  }

}
