package fr.loria.mosel.rodin.eb2da.tom;

import java.util.HashMap;
import java.math.BigInteger;
import fr.loria.mosel.rodin.eb2da.tom.expressiontranslator.types.types.*;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.Map.Entry;

import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.BinaryPredicate;
import org.eventb.core.ast.BinaryExpression;
import org.eventb.core.ast.BoundIdentDecl;
import org.eventb.core.ast.BoundIdentifier;
import org.eventb.core.ast.FreeIdentifier;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.QuantifiedExpression;
import org.eventb.core.ast.QuantifiedPredicate;
import org.eventb.core.ast.RelationalPredicate;
import org.eventb.core.ast.UnaryExpression;
import org.eventb.core.ast.UnaryPredicate;
import org.eventb.core.ast.SetExtension;
import org.eventb.core.ast.AssociativePredicate;
import org.eventb.core.ast.AssociativeExpression;
import org.eventb.core.ast.IntegerLiteral;

import fr.loria.mosel.rodin.eb2da.datastructure.DAAction;
import fr.loria.mosel.rodin.eb2da.datastructure.DAVariable;
import fr.loria.mosel.rodin.eb2da.datastructure.DAConstant;
import fr.loria.mosel.rodin.eb2da.datastructure.DAMessageConstructor;
import fr.loria.mosel.rodin.eb2da.datastructure.DANames;

public class ExpressionTranslator {
	%include {FormulaV3.tom}
	
	%gom{
		module Types
		imports String
		abstract syntax
		Type = Function(dom:Type, ran:Type)
			| PowSet(t:Type)
			| ProdSet(lt:Type, rt:Type)
			| Int()
			| StaticSet(id:String)
	}
	
	private static Type exprToType(Expression expr) {
		%match(Expression expr) {
			// TODO some other cases like comprehensive sets, enumerated sets etc.
			// TODO what about parameters of event with ambiguous typing
			(Tfun | Pfun | Tinj | Pinj | Tsur | Psur | Tbij)(l, r) -> { return `Function(exprToType(l), exprToType(r)); }
			Pow(s) -> { return `PowSet(exprToType(s)); }
			Cprod(l, r) -> { return `ProdSet(exprToType(l), exprToType(r)); }
			FreeIdentifier(id) -> { return `StaticSet(id); }
			INTEGER() -> { return `Int(); }
		}
		return `Int();
	}
	
	// Function to call for the translation of any expression
	public static String translateExpression(Expression expr, Expression type,
			HashMap<String,DAVariable> mapper, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		Type t = exprToType(type);
		List<DAVariable> boundIds = new ArrayList<DAVariable>();
		return translateExpression(expr, t, mapper, boundIds, dan, msgConstructors);
	}
	
	// Function to call for the translation of initialisation assignment.
	public static String translateInitExpression(Expression expr, Expression type,
			HashMap<String,DAVariable> mapper, String pcl, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		%match(Expression expr, String pcl){
			Cset(bidList(_), In(BoundIdentifier(bi), t@FreeIdentifier(s)),
					Mapsto(BoundIdentifier(bi), e)), s
			&& bi == 0 -> {
				HashMap<String, DAVariable>	newMapper = new HashMap<String, DAVariable>(mapper);
				List<DAVariable> boundIds = new ArrayList<DAVariable>();
				boundIds.add(0, new DAVariable("self", `t, null));
				return `translateExpression(e, exprToType(type), newMapper, boundIds, dan, msgConstructors);
			}
		}
		return translateExpression(expr, type, mapper, dan, msgConstructors);
	}
	
	private static String translateExpression(Expression expr,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		Type exprType = getType(expr, mapper, boundIds, dan, msgConstructors);
		return translateExpression(expr, exprType, mapper, boundIds, dan, msgConstructors);
	}
	
	private static String translateExpression(Expression expr, Type type,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		if(type == `StaticSet(dan.getMessages())) {
			return translateMessageExpression(expr, mapper, boundIds, dan, msgConstructors);
		} else {
			return translateNotMsgExpression(expr, type, mapper, boundIds, dan, msgConstructors);
		}
	}
	
	// TODO add more types
	// TODO complete
	private static String translateNotMsgExpression(Expression expr, Type type,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		
		if(mapper.containsKey(expr.toString())) {
			return mapper.get(expr.toString()).getName();
		}
		%match(Expression expr, Type type) {
			BoundIdentifier(x), _ -> {
				return boundIds.get(`x).getName();
			}
			TRUE(), _ -> {
				return "True";
			}
			FALSE(), _ -> {
				return "False";
			}
			FunImage(f, x), ran -> {
				// in case f is a local constant and we are translating an initialisation expression
				if(mapper.containsKey(`f.toString()+"@")) {
					return mapper.get(`f.toString()+"@").getName();
				}
				// we know that x is not a set nor a function
				// we can therefore get the LB type of x with the Event-B type of x.
				// the domain of f is a subset of the type of x
				Type dom = exprToType(`x.getType().toExpression());
				return String.format("%s[%s]",
						translateExpression(`f, `Function(dom, ran), mapper, boundIds, dan, msgConstructors),
						translateExpression(`x, dom, mapper, boundIds, dan, msgConstructors));
			}
			// arithmetic associative expression : addition and multiplication
			(Plus|Mul)(eList(elements*)), _ -> {
				List<String> resList = new ArrayList<String>();
				for(int i = 0; i < `elements*.length; i++) {
					resList.add(translateExpression(`elements*[i], `Int(), mapper, boundIds, dan,
							msgConstructors));
				}
				String symbol = "";
				switch(expr.getTag()) {
				case Formula.PLUS :
					symbol = " + ";
					break;
				case Formula.MUL :
					symbol = " * ";
					break;
				}
				return resList.stream().collect(Collectors.joining(symbol, "(", ")"));
			}
			// arithmetic binary expression : substraction, division, modulo and exponentiation
			(Minus|Div|Mod|Expn)(le, re), _ -> {
				String symbol = "";
				String lTrans = translateNotMsgExpression(`le, `Int(), mapper, boundIds, dan, msgConstructors);
				String rTrans = translateNotMsgExpression(`re, `Int(), mapper, boundIds, dan, msgConstructors);
				switch(expr.getTag()) {
				case Formula.MINUS :
					symbol = "-";
					break;
				case Formula.DIV :
					return String.format("int(%s / %s)",lTrans, rTrans);
				case Formula.MOD:
					symbol = "%";
					break;
				case Formula.EXPN:
					symbol = "**";
					break;
				}
				return String.format("(%s %s %s)", lTrans, symbol, rTrans);
			}
			// arithmetic unary expression : min, max
			(Min|Max)(s), _ -> {
				String symbol = "";
				switch(expr.getTag()) {
				case Formula.KMIN :
					symbol = "min";
					break;
				case Formula.KMAX :
					symbol = "max";
					break;
				}
				return String.format("%s(%s)", symbol, translateNotMsgExpression(`s, `PowSet(Int()),
						mapper, boundIds, dan, msgConstructors));
			}
			// unary expression : card
			Card(s), _ -> {
				return String.format("len(%s)",
						translateExpression(`s, mapper, boundIds, dan, msgConstructors));
			}
			x, Function(dom, ran) -> {
				return `translateFunctionExpr(x, Function(dom, ran), mapper, boundIds, dan, msgConstructors);
			}
			// last option : x must be a set expression
			x, t -> {
				return `translateSetExpr(x, t, mapper, boundIds, dan, msgConstructors);
			}
		}
		return expr.toString();
	}
	
	// TODO complete
	private static String translateFunctionExpr(Expression expr, Type type,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		if(mapper.containsKey(expr.toString())) {
			return mapper.get(expr.toString()).getName();
		}
		%match(Expression expr, Type type) {
			BoundIdentifier(x), _ -> {
				return boundIds.get(`x).getName();
			}
			EmptySet(), _ -> {
				return "{}";
			}
			// form {x . x : s | x,,e}
			Cset(bidList(x), In(BoundIdentifier(bi), s), Mapsto(e1,e2)), Function(dom, ran)
			&& bi == 0 -> {
				// HashMap<String, DAVariable>	newMapper = new HashMap<String, DAVariable>(mapper);
				// newMapper.put(`x.toString(), new DAVariable(`x.getName(), `s, null));
				List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
				newBoundIds.add(0, new DAVariable(`x.getName(), `s, null));
				return String.format("{%s:%s for %s in %s}",
						`translateExpression(e1, dom, mapper, newBoundIds, dan, msgConstructors),
						`translateExpression(e2, ran, mapper, newBoundIds, dan, msgConstructors),
						`x.getName(),
						`translateExpression(s, PowSet(dom), mapper, newBoundIds, dan, msgConstructors));
			}
			Cset(bidList(x*), Land(pList(predl*)), Mapsto(e1, e2)), Function(dom, ran) -> {
				Predicate[] bidDecl;
				bidDecl = new Predicate[`x*.length];
				System.arraycopy(`predl*, 0, bidDecl, 0, `x*.length);
				List<Predicate> otherPreds = new ArrayList<Predicate>();
				for(int i = `x*.length; i < `predl*.length; i++) {
					otherPreds.add(`predl*[i]);
				}
				List<DAVariable> newBoundIds = getBoundIdentifiers(`x*, bidDecl);
				List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
				for(DAVariable bid : newBoundIds) {
					allBoundIds.add(0, bid);
				}
				return String.format("{%s:%s %s if %s}",
						`translateExpression(e1, dom, mapper, allBoundIds, dan, msgConstructors),
						`translateExpression(e2, ran, mapper, allBoundIds, dan, msgConstructors),
						newBoundIds.stream()
						.map(bid -> String.format("for %s in %s", bid.getName(),
								translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
						.collect(Collectors.joining(" ")),
						translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));
			}
			SetExtension(eList(elements*)), Function(dom, ran) -> {
				List<String> resList = new ArrayList<String>();
				for(int i = 0; i < `elements*.length; i++) {
					if(`elements*[i].getTag() == Formula.MAPSTO) {
						BinaryExpression bExpr = (BinaryExpression) `elements*[i];
						resList.add(String.format("%s:%s",
								translateExpression(bExpr.getLeft(), `dom, mapper, boundIds, dan, msgConstructors),
								translateExpression(bExpr.getRight(), `ran, mapper, boundIds, dan, msgConstructors)));
					} else {
						// TODO throw syntax error
						return expr.toString();
					}
				}
				return resList.stream().collect(Collectors.joining(", ", "{", "}"));
			}
			// autres formes pour {...}
			// autres constructeurs pour les fonctions.
		}
		return expr.toString();
	}
	
	private static String translateSetExpr(Expression expr, Type type,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		if(mapper.containsKey(expr.toString())) {
			return mapper.get(expr.toString()).getName();
		}
		%match(Expression expr, Type type) {
			BoundIdentifier(x), _ -> {
				return boundIds.get(`x).getName();
			}			
			EmptySet() , _ -> {
				return "set()";
			}
			Cset(bidList(x), In(BoundIdentifier(bi), s), e), PowSet(t) && bi == 0 -> {
				List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
				newBoundIds.add(0, new DAVariable(`x.getName(), `s, null));
				Type sType = getType(`s, mapper, boundIds, dan, msgConstructors);
				return String.format("setof(%s, %s in %s)",
						translateExpression(`e, `t, mapper, newBoundIds, dan, msgConstructors),
						`x.getName(),
						translateExpression(`s, sType, mapper, newBoundIds, dan, msgConstructors)
						);
			}
			Cset(bidList(x*), Land(pList(predl*)), e), PowSet(t) -> { 
				Predicate[] bidDecl;
				bidDecl = new Predicate[`x*.length];
				System.arraycopy(`predl*, 0, bidDecl, 0, `x*.length);
				List<Predicate> otherPreds = new ArrayList<Predicate>();
				for(int i = `x*.length; i < `predl*.length; i++) {
					otherPreds.add(`predl*[i]);
				}
				List<DAVariable> newBoundIds = getBoundIdentifiers(`x*, bidDecl);
				List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
				for(DAVariable bid : newBoundIds) {
					allBoundIds.add(0, bid);
				}
				return String.format("setof(%s, %s, %s)",
						translateExpression(`e, `t, mapper, allBoundIds, dan, msgConstructors),
						newBoundIds.stream()
						.map(bid -> String.format("%s in %s", bid.getName(),
								translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
						.collect(Collectors.joining(", "))
						, translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));
			}
			SetExtension(eList(elmt*)), PowSet(s) -> {
				List<Expression> elements = Arrays.asList(`elmt*);
				// return "{'translate(elmt[0])', 'translate(elmt[1])', ..., 'translate(elmt[last])'} 
				return elements.stream()
						.map(e -> translateExpression(e,`s, mapper, boundIds, dan, msgConstructors))
						.collect(Collectors.joining(", ", "{", "}"));
			}
			Dom(f), PowSet(s) -> {
				return String.format("set(%s.keys())",
						translateExpression(`f, `s, mapper, boundIds, dan, msgConstructors));
			}
			Ran(f), PowSet(s) -> {
				return String.format("set(%s.values())",
						translateExpression(`f, `s, mapper, boundIds, dan, msgConstructors));
			}
			// associative set expressions : union, intersection
			(BUnion|BInter)(eList(sets*)), t -> {
				List<Expression> setList = Arrays.asList(`sets*);
				String symbol = "";
				switch(expr.getTag()) {
				case Formula.BUNION :
					symbol = " | ";
					break;
				case Formula.BINTER :
					symbol = " & ";
					break;
				}
				return setList.stream()
						.map(s -> translateExpression(s, `t, mapper, boundIds, dan, msgConstructors))
						.collect(Collectors.joining(symbol, "(", ")"));
			}
			// binary set expression : set minus
			SetMinus(s1, s2), t -> {
				return String.format("(%s - %s)",
						translateExpression(`s1, `t, mapper, boundIds, dan, msgConstructors),
						translateExpression(`s2, `t, mapper, boundIds, dan, msgConstructors));
			}
			// Binary set expression : product
			Cprod(s1, s2), t -> {
				return String.format("product(%s, %s)",
						translateExpression(`s1, `t, mapper, boundIds, dan, msgConstructors),
						translateExpression(`s2, `t, mapper, boundIds, dan, msgConstructors));
			}
			UpTo(e1, e2), _ -> {
				return String.format("set(range(%s, %s+1))",
						translateExpression(`e1, `Int(), mapper, boundIds, dan, msgConstructors),
						translateExpression(`e2, `Int(), mapper, boundIds, dan, msgConstructors));
			}
		}
		return expr.toString();
	}
	
	public static String translateMessageExpression(Expression expr,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		if(mapper.containsKey(expr.toString())) {
			return mapper.get(expr.toString()).getName();
		}
		%match(Expression expr) {
			BoundIdentifier(x) -> {
				return boundIds.get(`x).getName();
			}
			FunImage(FreeIdentifier(f),x) -> {
				for(DAMessageConstructor msgCst : msgConstructors) {
					if(msgCst.getName().equals(`f.toString())) {
						return String.format("(\"%s\", %s)", msgCst.getName(),
								translateTuple(`x, mapper, boundIds, dan, msgConstructors));
					}
				}
			}
			FreeIdentifier(x) -> {
				return String.format("(\"%s\",)", `x.toString());
			}
		}
		return translateNotMsgExpression(expr, `StaticSet(dan.getMessages()), mapper,
				boundIds, dan, msgConstructors);
	}
	
	private static String translateTuple(Expression expr,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		%match(Expression expr) {
			Mapsto(x, y) -> {
				return String.format("%s, %s",
						translateTuple(`x, mapper, boundIds, dan, msgConstructors),
						translateExpression(`y, mapper, boundIds, dan, msgConstructors));
			}
		}
		return translateExpression(expr, mapper, boundIds, dan, msgConstructors);
	}
	
	public static String translatePredicate(Predicate pred, HashMap<String, DAVariable> mapper,
			List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		%match(Predicate pred) {
			// associative predicates : and, or
			(Land|Lor)(pList(predl*)) -> {
				List<Predicate> predList = Arrays.asList(`predl*);
				return predList.stream()
						.map(p -> translatePredicate(p, mapper, boundIds, dan, msgConstructors))
						.collect(Collectors.joining(" and ", "(", ")"));
			}
			NotB(p) -> {
				return String.format("not(%s)", translatePredicate(`p, mapper, boundIds, dan, msgConstructors));
			}
			Limp(p1, p2) -> {
				return String.format("(not(%s) or %s)",
						translatePredicate(`p1, mapper, boundIds, dan, msgConstructors),
						translatePredicate(`p2, mapper, boundIds, dan, msgConstructors));
			}
			// sent or received = 0
			(Equal|Gt)(FunImage(
					FreeIdentifier(comFunction),
					Mapsto(Mapsto(FreeIdentifier(chan),Mapsto(p,q)), msg)),
					IntegerLiteral(x))
			 && chan << String dan.getChannels() && x << BigInteger.ZERO -> {
				
				HashMap<String, DAVariable> newMapper = new HashMap<String, DAVariable>(mapper);
				for(Entry<String, DAVariable> var : newMapper.entrySet()) {
				    if(!var.getValue().getName().contains(".")) { 
				    	var.setValue(new DAVariable("_" + var.getValue().getName(),
				    			var.getValue().getType(), var.getValue().getInitialValue()));
				    }
				}
				List<DAVariable> newBoundIds = boundIds.stream().map(v -> new DAVariable("_" + v.getName(), v.getType(), null))
					    .collect(Collectors.toList());
				// case sent
				String res = "";
				if(`comFunction.equals(dan.getSent())) {
					res = String.format("some(sent(%s, to=%s))",
							translateMessageExpression(`msg, newMapper, newBoundIds, dan, msgConstructors),
							translateExpression(`q, getType(`q, newMapper, newBoundIds, dan, msgConstructors), newMapper, newBoundIds, dan, msgConstructors));
				}
				// case received
				else if(`comFunction.equals(dan.getReceived())) {
					res = String.format("some(received(%s, _from=%s))",
							translateMessageExpression(`msg, newMapper, newBoundIds, dan, msgConstructors),
							translateExpression(`p, getType(`p, newMapper, newBoundIds, dan, msgConstructors),
									newMapper, newBoundIds, dan, msgConstructors));
				}
				if(pred.getTag() == Formula.EQUAL) {
					res = "not(" + res + ")";
				}
				return res;
			}
			(Equal|NotEqual)(l,r) -> {
				String symbol = "";
				switch(pred.getTag()) {
				case Formula.EQUAL :
					symbol = "==";
					break;
				case Formula.NOTEQUAL :
					symbol = "!=";
					break;
				}
				Type lt = getType(`l, mapper, boundIds, dan, msgConstructors);
				Type rt = getType(`r, mapper, boundIds, dan, msgConstructors);
				String rexp = "";
				String lexp = "";
				%match (Type lt, Type rt) {
					Function(_,_), _ -> {
						lexp = translateExpression(`l, lt, mapper, boundIds, dan, msgConstructors);
						rexp = translateExpression(`r, lt, mapper, boundIds, dan, msgConstructors);
						return String.format("%s %s %s", lexp, symbol, rexp);
					}
					_, Function(_, _) -> {
						lexp = translateExpression(`l, rt, mapper, boundIds, dan, msgConstructors);
						rexp = translateExpression(`r, rt, mapper, boundIds, dan, msgConstructors);
						return String.format("%s %s %s", lexp, symbol, rexp);
					}
					_, _ -> {
						lexp = translateExpression(`l, lt, mapper, boundIds, dan, msgConstructors);
						rexp = translateExpression(`r, rt, mapper, boundIds, dan, msgConstructors);
						return String.format("%s %s %s", lexp, symbol, rexp);
					}
				}
			}
			// arithmetic relational predicate
			(Lt|Le|Gt|Ge)(e1, e2) -> {
				String symbol = "";
				switch(pred.getTag()) {
				case Formula.LT:
					symbol = "<";
					break;
				case Formula.LE:
					symbol = "<=";
					break;
				case Formula.GT:
					symbol = ">";
					break;
				case Formula.GE:
					symbol = ">=";
					break;
				}
				return String.format("%s %s %s",
						translateNotMsgExpression(`e1, `Int(), mapper, boundIds, dan, msgConstructors),
						symbol,
						translateNotMsgExpression(`e2, `Int(), mapper, boundIds, dan, msgConstructors));
			}
			// other relational predicates : inclusions, in
			(In|NotIn|Subset|NotSubset|SubsetEq|NotSubsetEq)(le, re) -> {
				String leTrans = translateExpression(`le, mapper, boundIds, dan, msgConstructors);
				String reTrans = translateExpression(`re, mapper, boundIds, dan, msgConstructors);
				String symbol = "";
				Boolean negation = true;
				switch(pred.getTag()) {
				case Formula.IN :
					negation = false;
				case Formula.NOTIN :
					symbol = "in";
					break;
				case Formula.SUBSET :
					negation = false;
				case Formula.NOTSUBSET :
					symbol = "<";
					break;
				case Formula.SUBSETEQ :
					negation = false;
				case Formula.NOTSUBSETEQ :
					symbol = "<=";
					break;
				}
				String code = String.format("%s %s %s", leTrans, symbol, reTrans);
				if(negation) {
					code = "not("+code+")";
				}
				return code;
			}
			ForAll(bidList(x), Limp(In(BoundIdentifier(bi), s), predr)) && bi == 0 -> {
				List<DAVariable> newBoundIds = new ArrayList<DAVariable>(boundIds);
				newBoundIds.add(0, new DAVariable(`x.getName(), `s, null));
				return String.format("each(%s in %s, has=%s)", `x.getName()
						, translateExpression(`s, mapper, newBoundIds, dan, msgConstructors)
						, translatePredicate(`predr, mapper, newBoundIds, dan, msgConstructors));
				
			}
			ForAll(bidList(x*), Limp(Land(pList(predl*)), predr)) -> {
				if(`predl*.length != `x*.length) {
					// TODO throw syntax error
					return pred.toString();
				}
				List<DAVariable> newBoundIds = getBoundIdentifiers(`x*, `predl*);
				List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
				for(DAVariable bid : newBoundIds) {
					allBoundIds.add(0, bid);
				}
				return String.format("each(%s, has=%s)",
						newBoundIds.stream()
						.map(bid -> String.format("%s in %s", bid.getName(),
								translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
						.collect(Collectors.joining(", ")),
						translatePredicate(`predr, mapper, allBoundIds, dan, msgConstructors));				
			}
			Exists(bidList(x), In(BoundIdentifier(bi), s)) && bi == 0 -> {
				return String.format("some(%s in %s)", `x.getName()
						, translateExpression(`s, mapper, boundIds, dan, msgConstructors));
			}
			Exists(bidList(x*), Land(pList(predl*))) -> {
				Predicate[] bidDecl;
				bidDecl = new Predicate[`x*.length];
				System.arraycopy(`predl*, 0, bidDecl, 0, `x*.length);
				List<Predicate> otherPreds = new ArrayList<Predicate>();
				for(int i = `x*.length; i < `predl*.length; i++) {
					otherPreds.add(`predl*[i]);
				}
				List<DAVariable> newBoundIds = getBoundIdentifiers(`x*, bidDecl);
				List<DAVariable> allBoundIds = new ArrayList<DAVariable>(boundIds);
				for(DAVariable bid : newBoundIds) {
					allBoundIds.add(0, bid);
				}
				return String.format("some(%s, has=%s)",
						newBoundIds.stream()
						.map(bid -> String.format("%s in %s", bid.getName(),
								translateExpression(bid.getType(), mapper, allBoundIds, dan, msgConstructors)))
						.collect(Collectors.joining(", "))
						, translatePredicateList(otherPreds, mapper, allBoundIds, dan, msgConstructors));	
			}
		}
		return pred.toString();
	}
	
	private static List<DAVariable> getBoundIdentifiers(BoundIdentDecl[] x, Predicate[] pred) {
		List<DAVariable> newBoundIds = new ArrayList<DAVariable>();
		if(pred.length != x.length) {
			// TODO throw syntax error
			return newBoundIds;
		}
		for(int i = 0; i < x.length; i++) {
			Predicate predi = pred[i];
			%match(Predicate predi) {
				In(BoundIdentifier(bi), si) -> {
					if(`bi == x.length-i-1) {
						newBoundIds.add(new DAVariable(x[i].getName(), `si, null));
					} else {
						// TODO throw syntax error
						return newBoundIds;
					}
				}
				!In(BoundIdentifier(_), _) -> {
					// TODO throw syntax error
					return newBoundIds;
				}
			}
		}
		return newBoundIds;
	}
	
	public static String translatePredicateList(List<Predicate> plist,
			HashMap<String, DAVariable> mapper, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		return translatePredicateList(plist, mapper, new ArrayList<DAVariable>(), dan, msgConstructors);
	}
	
	private static String translatePredicateList(List<Predicate> plist,
			HashMap<String, DAVariable> mapper, List<DAVariable> boundIds, DANames dan,
			List<DAMessageConstructor> msgConstructors) {
		if(plist.isEmpty()) {
			return "True";
		}
		return plist.stream().
				map(p -> translatePredicate(p, mapper, boundIds, dan, msgConstructors))
				.collect(Collectors.joining(") and (", "(", ")"));
	}
	
	private static List<String> translateCommunicationActions(Expression comExpr, List<String> res,
			HashMap<String, DAVariable> mapper,	DANames dan, List<DAMessageConstructor> msgConstructors) {
		%match(Expression comExpr) {
			FunImage(FreeIdentifier(comFunction), Mapsto(Mapsto(channels, Mapsto(_, dest)), msg)) -> {
				res = translateCommunicationActions(`channels, res, mapper, dan, msgConstructors);
				if(`comFunction.equals(dan.getSend())) {
					List<DAVariable> bids = new ArrayList<DAVariable>();
					res.add(String.format("send(%s, to=%s)",
							translateMessageExpression(`msg, mapper, bids, dan, msgConstructors),
							translateExpression(`dest, getType(`dest, mapper, bids, dan, msgConstructors),
									mapper, bids, dan, msgConstructors)));
				}
			}
		}
		return res;
	}
	
	public static List<String> translateActionList(List<DAAction> actList, HashMap<String, DAVariable> mapper,
			DANames dan, List<DAMessageConstructor> msgConstructors) {
		List<String> res = new ArrayList<String>();
		DAAction comAction = null;
		List<DAVariable> boundIds = new ArrayList<DAVariable>();
		HashMap<String, DAVariable> newMapper = new HashMap<String, DAVariable>(mapper);
		for(DAAction act : actList) {
			// Communication action case
			if(act.getAssignedVariable().equals(dan.getChannels())) {
				comAction = act;
			}
			// case var(proc) := var(proc) \ovr g
			else if(act.getAssignmentExpression().getTag() == Formula.OVR) {
				// translation into self.var.updates(deepcopy(g))
				AssociativeExpression assExpr = (AssociativeExpression) act.getAssignmentExpression();
				if(assExpr.getChildren().length == 2
						&& assExpr.getChildren()[0].toString().equals(act.getAssignedVariable())) {
					// TODO what if the key is not in the mapper?
					String tmp_var = mapper.get(act.getAssignedVariable()).getName() + "_tmp_lbda";
					if(tmp_var.startsWith("self.")) {
						tmp_var = tmp_var.substring(5);
					}
					res.add(String.format("%s = deepcopy(%s)", tmp_var,
							translateExpression(assExpr.getChildren()[0], mapper, boundIds,
									dan, msgConstructors)));
					newMapper.replace(assExpr.getChildren()[0].toString(),
							new DAVariable(tmp_var, mapper.get(assExpr.getChildren()[0].toString()).getType(), null));
					res.add(String.format("%s.update(deepcopy(%s))",
							translateExpression(assExpr.getChildren()[0], mapper, boundIds,
									dan, msgConstructors),
							translateFunctionExpr(assExpr.getChildren()[1],
									getFunctionType(assExpr.getChildren()[1]),
									newMapper, boundIds, dan, msgConstructors)));
				} else {
					// TODO throw syntax error
					res.add(act.getAssignedVariable() + "=" + act.getAssignmentExpression());
				}
			} else if(mapper.containsKey(act.getAssignedVariable())) {
				Type varType = exprToType(mapper.get(act.getAssignedVariable()).getType());
				String tmp_var = mapper.get(act.getAssignedVariable()).getName() + "_tmp_lbda";
				if(tmp_var.startsWith("self.")) {
					tmp_var = tmp_var.substring(5);
				}
				newMapper.replace(act.getAssignedVariable(),
						new DAVariable(tmp_var, mapper.get(act.getAssignedVariable()).getType(), null));
				res.add(String.format("%s = deepcopy(%s)", tmp_var,
						mapper.get(act.getAssignedVariable()).getName()));
				String assignmentString = translateExpression(act.getAssignmentExpression(),
						varType, newMapper, boundIds, dan, msgConstructors);
				%match(Type varType) {
					Function(_,_) -> {
						assignmentString = String.format("deepcopy(%s)", assignmentString);
					}
					PowSet(_) -> {
						assignmentString = String.format("set(%s)", assignmentString);
					}
				}
				res.add(String.format("%s = %s", mapper.get(act.getAssignedVariable()).getName(),
						assignmentString));
			} else {
				// TODO throw syntax error
				res.add(act.getAssignedVariable() + " = " + act.getAssignmentExpression());
			}
		}
		if(comAction != null) {
			res.addAll(translateCommunicationActions(comAction.getAssignmentExpression(), new ArrayList<String>(), newMapper,
					dan, msgConstructors));
		}
		return res;
	}
	
	private static Type getType(Expression expr, HashMap<String, DAVariable> mapper,
			List<DAVariable> boundIds, DANames dan, List<DAMessageConstructor> msgConstructors) {
		// TODO complete
		if(mapper.containsKey(expr.toString())) {
			return exprToType(mapper.get(expr.toString()).getType());
		}
		%match(Expression expr) {
			BoundIdentifier(x) -> {
				return exprToType(boundIds.get(`x).getType());
			}
		}
		return exprToType(expr.getType().toExpression());
	}
	
	private static Type getFunctionType(Expression f) {
		Expression fType = f.getType().toExpression();
		%match(Expression fType) {
			Pow(Cprod(dom, ran)) -> {
				return `Function(exprToType(dom), exprToType(ran));
			}
		}
		return exprToType(fType);
	}
}