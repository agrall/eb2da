package fr.loria.mosel.rodin.eb2da.printer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.eventb.core.ast.Expression;
import org.eventb.core.ast.Formula;
import org.eventb.core.ast.Predicate;
import org.eventb.core.ast.RelationalPredicate;

import fr.loria.mosel.rodin.eb2da.datastructure.DAConstant;
import fr.loria.mosel.rodin.eb2da.datastructure.DAEnumSet;
import fr.loria.mosel.rodin.eb2da.datastructure.DAEvent;
import fr.loria.mosel.rodin.eb2da.datastructure.DAMessageConstructor;
import fr.loria.mosel.rodin.eb2da.datastructure.DANames;
import fr.loria.mosel.rodin.eb2da.datastructure.DAParser;
import fr.loria.mosel.rodin.eb2da.datastructure.DAVariable;
import fr.loria.mosel.rodin.eb2da.datastructure.DistributedAlgorithm;
import fr.loria.mosel.rodin.eb2da.datastructure.ProcessClass;
import fr.loria.mosel.rodin.eb2da.tom.ExpressionTranslator;
import fr.loria.mosel.rodin.eb2da.tom.PatternMatcher;
import fr.loria.mosel.rodin.eb2da.utils.Constants;
import fr.loria.mosel.rodin.eb2da.utils.Pair;

/**
 * This class purpose is to generate strings corresponding to DistAlgo code
 * from a DistributedAlgorithm object.
 * Use methods generateMainCode, generateProcessClasses and generateEnumeratedSets of this class.
 * 
 * @author Alexis Grall
 *
 */
public class DistAlgoPrinter {

	/**
	 * @param lvl
	 * 		level of indentation
	 * @return
	 * 		a String of offset * lvl blank spaces.
	 */
	private static String indent(int lvl) {
		String result = "";
		for(int i = 0; i<lvl; i++) {
			result += Constants.INDENT_OFFSET;
		}
		return result;
	}
	
	public static String classFileName(String pclName) {
		return classModuleName(pclName) + ".da";
	}
	
	private static String classModuleName(String pclName) {
		return pclName + "Class";
	}
	
	private static String className(String pclName) {
		return pclName;
	}
	
	public static String enumSetFileName(String enumSetName) {
		return enumSetModuleName(enumSetName) + ".py";
	}
	
	private static String enumSetModuleName(String enumSetName) {
		return enumSetName;
	}
	
	private static String enumSetClassName(String enumSetName) {
		return enumSetName;
	}
	
	private static String getPclSizeName(String pclName) {
		return "N"+pclName;
	}
	
	public static String generateConfigcode(DistributedAlgorithm dAlgo) {
		// TODO complete configurations
		
		// code to return
		StringBuilder code = new StringBuilder();
		
		// setup regular imports
		code.append("from itertools import product\n");
		code.append("from copy import deepcopy\n");
		code.append("\n");
		
		// configure channels
		// it doesn't work currently, I didn't find a way to configure the channels outside the main.da file
		// the statement config doesn't work in a if branch or in another file
		// the config(channel=x) statement doesn't work if x is a variable
		// in fact anything after the channel= is parsed as a string.
//		if(dAlgo.isReliable()) {
//			code.append("config(channel=reliable)\n");
//		}
		// TODO configure fifo
		code.append("\n");
		
		// setup number of processes
		for(ProcessClass pcl : dAlgo.getProcessClasses()) {
			code.append("# number of processes in class ").append(pcl.getName()).append("\n");
			code.append(getPclSizeName(pcl.getName())).append(" = ");
			// TODO : generate fresh name for Npcl if the name is not available
			if(pcl.getProcesses().isEmpty()) {
				code.append("2 #to be configured\n");
			} else {
				code.append(pcl.getProcesses().size()).append("\n");
			}
		}
		code.append("\n");
		
		return code.toString();
	}
	
	private static String generateConstants(List<DAConstant> localCsts,
			String cst, HashMap<String, DAVariable> pclSetNames, int idtLvl,
			DANames dan, List<DAMessageConstructor> msgConstructors) {
		StringBuilder code = new StringBuilder();
		if(!localCsts.isEmpty()) {
			// Stack<String> st = new Stack<String>();
			// print "    cst = "
			code.append(String.format("%s%s = ",  indent(idtLvl), cst));
			if(localCsts.get(0).isFunction()) {
				if(localCsts.get(0).getValue() != null) {
					code.append(String.format("deepcopy(%s)\n",
							ExpressionTranslator.translateExpression(
									localCsts.get(0).getValue(),
									localCsts.get(0).getType(), pclSetNames, dan, msgConstructors)));
				} else {
					code.append(genericValue(localCsts.get(0), pclSetNames));
					code.append("\n");
				}
				for(int cstIndex=1; cstIndex<localCsts.size(); cstIndex++) {
					if(localCsts.get(cstIndex).getValue() != null) {
						code.append(String.format("%s%s.update(deepcopy(%s))\n",
								indent(idtLvl), cst,
								ExpressionTranslator.translateExpression(
										localCsts.get(cstIndex).getValue(), localCsts.get(cstIndex).getType(),
										pclSetNames, dan, msgConstructors)));
					} else {
						code.append(String.format("%s%s.update(deepcopy(%s))\n",
								indent(1), cst,
								genericValue(localCsts.get(cstIndex), pclSetNames)));
					}
				}
			} else {
				// print directly cst = value
				if(localCsts.get(0).getValue() != null) {
					code.append(ExpressionTranslator.translateExpression(
							localCsts.get(0).getValue(),
							localCsts.get(0).getType(), pclSetNames, dan, msgConstructors));
				} else {
					code.append(genericValue(localCsts.get(0), pclSetNames));
				}
				code.append("\n");
			}
		}
		return code.toString();
	}
	
	/**
	 * @return a String corresponding to a DistAlgo main function corresponding to a DistributedAlgorithm
	 */
	public static String generateMainCode(DistributedAlgorithm dAlgo) {
		StringBuilder code = new StringBuilder();
		DAParser parser = dAlgo.getMachine().getParser();
		DANames dan = dAlgo.getNames();
		List<DAMessageConstructor> msgConstructors = dAlgo.getMsgConstructors();
		int idtLvl = 0;
		
		// import config file
		code.append("import config as cfg\n");
		
		// setup regular imports
		code.append("from itertools import product\n");
		code.append("from copy import deepcopy\n");
		
		// setup enumerated sets imports
		for(ProcessClass pcl : dAlgo.getProcessClasses()) {
			//
			code.append(String.format("from %s import %s\n",
					classModuleName(pcl.getName()), className(pcl.getName())));
		}
		for(DAEnumSet enumSet : dAlgo.getEnumSets()) {
			code.append(String.format("from %s import %s\n",
					enumSetModuleName(enumSet.getName()), enumSetClassName(enumSet.getName())));
		}
		code.append("\n");
		
		// definition of main + configurations of channels
		code.append("def main():\n");
		idtLvl += 1;
		code.append(indent(idtLvl)).append("config(handle=all)\n");
		code.append(indent(idtLvl)).append("config(channel=reliable)\n");
		code.append("\n");
		
		// creation of processes
		HashMap<String, DAVariable> pclSetNames = new HashMap<String, DAVariable>();
		for(ProcessClass pcl : dAlgo.getProcessClasses()) {
			Expression pclType = parser.parseExpression("\u2119(" + dan.getNodes() + ")");
			pclSetNames.put(pcl.getName(), new DAVariable(pcl.getName() + "Set", pclType, null));
			code.append(String.format("%s%s = new(%s,num=cfg.%s)\n",
					indent(idtLvl), pclSetNames.get(pcl.getName()).getName(),
					pcl.getName(), getPclSizeName(pcl.getName())));
			// TODO : generate fresh name for pclSet if the name is not available
			/* How to check if a name is available ?
			 * 	1. check if it is a Python or DistAlgo keyword (compare with a list of keywords)
			 * 	2. Check if any other variable or constant has the same name in the generated program
			 * 
			 * How to generate fresh name if first one was not available ?
			 * 	1. Add a random number as a suffix of the name.
			 * 	2. If new name is available, proceed, else go back to step 1.
			 * 
			 *  String generateFreshName(String name) {
			 *  	while(!isAvailable(name)) {
			 *  		name = addRandomSuffix(name);
			 *  	}
			 *  	return name;
			 *  }
			 */
			if(!pcl.getProcesses().isEmpty()) {
				code.append(indent(idtLvl));
				if(pcl.getProcesses().size() == 1) {
					 code.append(String.format("(%s,)", pcl.getProcesses().get(0)));
				} else {
					code.append(pcl.getProcesses().stream().collect(Collectors.joining(", ", "(", ")")));
				}
				code.append(String.format(" = list(%s)\n",
						pclSetNames.get(pcl.getName()).getName()));
			}
			code.append("\n");
		}
		
		// creation of set nodes
		code.append(String.format("%s%s = set.union(%s)\n\n", indent(idtLvl),
				dAlgo.getNames().getNodes(),
				dAlgo.getProcessClasses().stream()
				.map(pcl -> pclSetNames.get(pcl.getName()).getName())
				.collect(Collectors.joining(", "))));
		
		// constants
		for(String cst : dAlgo.getConstantNames()) {
			List<DAConstant> localCsts = dAlgo.getLocalConstantsWithName(cst);
			code.append(generateConstants(localCsts, cst, pclSetNames, idtLvl, dan, msgConstructors));
		}
		
		code.append("\n");

		// Setup of the processes
		// TODO fresh name for proc
		String procId = "proc";
		// One setup for loop for each process class
		for(ProcessClass pcl : dAlgo.getProcessClasses()) {
			// loop through processes in DistAlgo
			code.append(String.format("%sfor %s in %s :\n", indent(idtLvl), procId,
					pclSetNames.get(pcl.getName()).getName()));
			idtLvl += 1;
			// build the String List of local constants of proc 
			List<String> cstList = new ArrayList<String>();
			for(DAConstant cst : pcl.getConstants()) {
				// if cst is a function then add cst[proc] else add cst
				if(cst.isFunction()) {
					cstList.add(String.format("%s[%s]", cst.getName(), procId));
				} else {
					cstList.add(cst.getName());
				}
			}
			// build the DistAlgo String of the list
			String localConstantsList = "";
			if(cstList.size() == 1) {
				localConstantsList = "(" + cstList.get(0) + ",)";
			} else {
				localConstantsList = cstList.stream()
						.collect(Collectors.joining(", ", "(", ")"));
			}
			// Setup proc with the built DistAlgo list of constants
			code.append(String.format("%ssetup({%s}, %s)\n", indent(idtLvl), procId, 
					localConstantsList));
			idtLvl -= 1;
		}
		code.append("\n");
		
		// start(Nodes)
		code.append(String.format("%sstart(%s)", indent(idtLvl), dAlgo.getNames().getNodes()));
		
		return code.toString();
	}
	
	public static String generateProcessClassCode(ProcessClass pcl) {
		DANames dan = pcl.getDistributedAlgorithm().getNames();
		StringBuilder code = new StringBuilder();
		List<DAEvent> rcvEvents = new ArrayList<DAEvent>();
		DAParser parser = pcl.getDistributedAlgorithm().getMachine().getParser();
		List<DAMessageConstructor> msgConstructors = pcl.getDistributedAlgorithm().getMsgConstructors();
		
		// mapper for the translation of non function constants, parameters, free identifiers etc
		HashMap<String, DAVariable> mapper = new HashMap<String, DAVariable>();
		// add elements of States to the mapper
		for(String st : pcl.getStates()) {
			mapper.put(st,
					new DAVariable("\""+st+"\"",
							parser.parseExpression(dan.getStates()), null));
		}
		mapper.put(dan.getDone(), new DAVariable("\""+dan.getDone()+"\"",
				parser.parseExpression(dan.getStates()), null));
		
		// indentation level
		int idtLvl = 0;
		// imports
		code.append("from itertools import product\n");
		code.append("from copy import deepcopy\n");
		for(DAEnumSet daes : pcl.getDistributedAlgorithm().getEnumSets()) {
			if(daes.isLocal(pcl.getName())) {
				code.append(String.format("from %s import %s\n", enumSetModuleName(daes.getName()),
						enumSetClassName(daes.getName())));
			}
			for(String elmt : daes.getElements()) {
				mapper.put(elmt, new DAVariable(String.format("%s.%s", enumSetClassName(daes.getName()), elmt)
						, parser.parseExpression(daes.getName()), null));
			}
			 // POW = \u2119
			 mapper.put(daes.getName(),
					 new DAVariable("{member for name, member in "+enumSetClassName(daes.getName())+".__members__.items()"
			 		, parser.parseExpression("\u2119("+ daes.getName() +")"), null));
		}
		code.append("\n");
		
		// class def
		code.append(String.format("%sclass %s(process):\n", indent(0), className(pcl.getName())));
		idtLvl += 1;
		
		// setup method
		code.append(String.format("%sdef setup(%s):\n", indent(idtLvl),
				pcl.getConstants().stream().map(DAConstant::getName).collect(Collectors.joining(","))));

		List<DAConstant> csts = new ArrayList<DAConstant>();
		// add non function constant to the mapper
		// if the constant is a function it is added to a list of constants
		for(DAConstant cst : pcl.getConstants()) {
			if(!cst.isFunction()) {
				mapper.put(cst.getName(), new DAVariable("self."+cst.getName(), cst.getLocalType(), null));
			} else {
				mapper.put(cst.getName()+"@",
						new DAVariable("self."+cst.getName(), cst.getLocalType(), null));
				csts.add(cst);			
			}
		}
		idtLvl += 1;
		// translate initialisation of each variable
		for(DAVariable var : pcl.getVariables()) {
			code.append(indent(idtLvl));
			code.append(String.format("self.%s = %s\n", var.getName(),
					ExpressionTranslator.translateInitExpression(var.getInitialValue(),
							var.getType(), mapper, pcl.getName(), dan, msgConstructors)));
		}
		code.append("\n");
		idtLvl -= 1;
		
		// run method
		code.append(String.format("%sdef run():\n", indent(idtLvl)));
		idtLvl += 1;
		// state function
		code.append(indent(idtLvl)).append("stateFunctions = ");
		code.append(pcl.getStates().stream().map(s -> "\""+s+"\":"+s)
				.collect(Collectors.joining(", ", "{", "}")));
		code.append("\n");
		// while loop
		code.append(String.format("%swhile(%s != \"%s\"):\n", indent(idtLvl), dan.getPc(), dan.getDone()));
		idtLvl += 1;
		code.append(String.format("%sstateFunctions[self.%s]()\n", indent(idtLvl), dan.getPc()));
		idtLvl -= 1;
		code.append("\n");
		idtLvl -= 1;
		
		// state methods
		for(String st : pcl.getStates()) {
			code.append(String.format("%s# state %s\n", indent(idtLvl), st));
			code.append(String.format("%sdef %s():\n", indent(idtLvl), st));
			idtLvl += 1;
			if(pcl.isReceive(st)) {
				code.append(String.format("%s--%s\n", indent(idtLvl), st));
			}
			boolean firstEvent = true;
			for(DAEvent evt : pcl.getEvents(st)) {
				if(evt.isReceiveEvent()) {
					rcvEvents.add(evt);
				} else {
					code.append(String.format("%s# event %s\n", indent(idtLvl), evt.getName()));
					// eventMapper is the map used for this event
					// parameters, variables and constants are added to the map
					HashMap<String, DAVariable> eventMapper = new HashMap<String, DAVariable>(mapper);
					eventMapper.put(evt.getLocalProc(), new DAVariable("self", null, null));
					// add to eventMapper all the translation of all function constants and variables.
					for(DAConstant cst : csts) {
						eventMapper.put(cst.getName()+"("+evt.getLocalProc()+")",
								new DAVariable("self."+cst.getName(), cst.getLocalType(), null));
					}
					for(DAVariable var : pcl.getVariables()) {
						eventMapper.put(var.getName()+"("+evt.getLocalProc()+")",
								new DAVariable("self."+var.getName(), var.getType(), null));
					}
					
					// translate event guards
					code.append(indent(idtLvl));
					if(firstEvent) {
						if(pcl.isReceive(st)) {
							code.append("if await ");
						} else {
							code.append("if ");
						}
						firstEvent = false;
					} else {
						code.append("elif ");
					}
					// TODO translate guards
					// (self.pc == st) :
					// (self.pc == st and Guards):
					// (self.pc == st and some(param in S, has=Guards)):
					code.append(String.format("(self.%s == \"%s\"", dan.getPc(), st));
					boolean firstParam = true;
					for(DAConstant param : evt.getParameters()) {
						if(firstParam) {
							code.append(" and some(");
						} else {
							code.append(", ");
						}
						Expression paramTypeType = parser.parseExpression(String.format("\u2119(%s)",
								param.getType()));
						code.append(String.format("%s in %s", param.getName(),
								ExpressionTranslator.translateExpression(
										param.getType(), paramTypeType,
										eventMapper, dan, msgConstructors)));
						eventMapper.put(param.getName(), new DAVariable(param.getName(), param.getType(), null));
					}
					if(!evt.getGuards().isEmpty()) {
						if(!evt.getParameters().isEmpty()) {
							code.append(", has=").append(ExpressionTranslator.
									translatePredicateList(evt.getGuards(), eventMapper, dan, msgConstructors));
						} else {
							code.append(" and ").append(ExpressionTranslator.translatePredicateList(
									evt.getGuards(), eventMapper, dan, msgConstructors));
						}
					}
					if(!evt.getParameters().isEmpty()) {
						code.append(")");
					}
					code.append("):\n");
					idtLvl += 1;
				
					code.append(ExpressionTranslator
							.translateActionList(evt.getActions(), eventMapper, dan, msgConstructors)
							.stream().collect(Collectors.joining("\n"+indent(idtLvl), indent(idtLvl), "\n")));
					idtLvl -= 1;
				}
			}
			idtLvl -= 1;
			code.append("\n");
		}
		
		// translate receive events
		for(DAEvent evt  : rcvEvents) {
			// eventMapper is the map used for this event
			// parameters, variables and constants are added to the map
			HashMap<String, DAVariable> eventMapper = new HashMap<String, DAVariable>(mapper);
			// add to eventMapper all the translation of all function constants and variables.
			for(DAConstant cst : csts) {
				eventMapper.put(cst.getName()+"("+evt.getLocalProc()+")",
						new DAVariable("self."+cst.getName(), cst.getLocalType(), null));
			}
			for(DAVariable var : pcl.getVariables()) {
				eventMapper.put(var.getName()+"("+evt.getLocalProc()+")",
						new DAVariable("self."+var.getName(), var.getType(), null));
			}
			// get message pattern and source pattern			
			DAConstant msgParameter = null;
			DAConstant srcParameter = null;
			Expression msgParameterValue = null;
			Expression srcParameterValue = null;
			// look for readyForReception guard
			for(Predicate grd : evt.getGuards()) {
				Pair<String, String> res = PatternMatcher.getReceiveBody(grd, dan.getReadyForReception(),
						evt.getLocalProc(), dan.getChannels());
				if(res != null) {
					for(Predicate grd2 : evt.getGuards()) {
						if(grd2.getTag() ==  Formula.EQUAL) {
							RelationalPredicate rp = (RelationalPredicate) grd2;
							if(rp.getLeft().toString().equals(res.first)) {
								srcParameterValue = rp.getRight();
							} else if(rp.getLeft().toString().equals(res.second)) {
								msgParameterValue = rp.getRight();
							}
						}
					}
					for(DAConstant param : evt.getParameters()) {
						if(param.getName().equals(res.first)) {
							srcParameter = new DAConstant(param.getName(), param.getType(), srcParameterValue, false);
						} else if(param.getName().equals(res.second)) {
							msgParameter = new DAConstant(param.getName(), param.getType(), msgParameterValue, false);
						}
					}
					break;
				}
			}
			if(msgParameter == null || srcParameter == null) {
				// TODO throw error
			}
			
			// Definition of receive method
			code.append(indent(idtLvl)).append("def receive(msg=");
			// empty bound identifier list
			List<DAVariable> boundIds = new ArrayList<DAVariable>();
			if(msgParameter.getValue() != null) {
				code.append(ExpressionTranslator.translateMessageExpression(msgParameter.getValue(), eventMapper, boundIds,
						dan, msgConstructors));
			} else {
				code.append(msgParameter.getName());
			}
			code.append(", from_=");
			if(srcParameter.getValue() != null) {
				code.append(ExpressionTranslator.translateExpression(srcParameter.getValue(),
						srcParameter.getType(), eventMapper, dan, msgConstructors));
			} else {
				code.append(srcParameter.getName());
			}
			code.append(", at=(").append(evt.getState()).append(",)):\n");
			// Body of receive
			idtLvl+=1;
			List<String> actCode = ExpressionTranslator
					.translateActionList(evt.getActions(), eventMapper, dan, msgConstructors);
			code.append(actCode.stream().collect(Collectors.joining("\n" + indent(idtLvl), indent(idtLvl), "\n")));
			idtLvl-=1;
		}
		return code.toString();
	}
	
	public static String generateEnumSetCode(DAEnumSet enumSet) {
		StringBuilder code = new StringBuilder();
		
		code.append("from enum import Enum\n\n");
		code.append(String.format("class %s(Enum):\n", enumSet.getName()));
		int i = 0;
		for(String elmt : enumSet.getElements()) {
			code.append(String.format("%s%s = %d\n", indent(1), elmt, i));
			i = i + 1;
		}
		return code.toString();
	}

	private static String genericValue(DAConstant daConstant,
			HashMap<String,DAVariable> pclSetNames) {
		// TODO change what is generic according to the type of the constant
		// TODO find a value satisfying all the axioms in all the contexts.
		if(daConstant.isFunction()) {
			// TODO change proc for a fresh variable
			return String.format("{%s:0 for %s in %s}",
					"proc", "proc", pclSetNames.get(daConstant.getLocality()).getName());
		}
		else {
			return "0";
		}
	}
	
}
