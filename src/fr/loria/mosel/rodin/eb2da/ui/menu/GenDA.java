package fr.loria.mosel.rodin.eb2da.ui.menu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eventb.core.IMachineRoot;
import org.eventb.core.ast.Predicate;
import fr.loria.mosel.rodin.eb2da.datastructure.DAAction;
import fr.loria.mosel.rodin.eb2da.datastructure.DAConstant;
import fr.loria.mosel.rodin.eb2da.datastructure.DAEnumSet;
import fr.loria.mosel.rodin.eb2da.datastructure.DAEvent;
import fr.loria.mosel.rodin.eb2da.datastructure.DAException;
import fr.loria.mosel.rodin.eb2da.datastructure.DAMessageConstructor;
import fr.loria.mosel.rodin.eb2da.datastructure.DAVariable;
import fr.loria.mosel.rodin.eb2da.datastructure.DistributedAlgorithm;
import fr.loria.mosel.rodin.eb2da.datastructure.ProcessClass;
import fr.loria.mosel.rodin.eb2da.printer.DistAlgoPrinter;

public class GenDA implements IObjectActionDelegate{

	private Shell shell;
	private IMachineRoot selectedMachine;
	private DistributedAlgorithm dAlgo;
	
	@Override
	public void run(IAction action){
		// construct DistributedAlgorithm object from the machine
		try {
			dAlgo = new DistributedAlgorithm(selectedMachine);
		} catch (CoreException e) {
			MessageDialog.openInformation(shell, "Info", e.toString());
			e.printStackTrace();
		} catch (DAException e) {
			MessageDialog.openInformation(shell, "Info", e.toString());
			e.printStackTrace();
		}
		
		// Print code to file
		// get path of machine file
		IPath projectPath = selectedMachine.getRodinProject().getProject().getLocation();
		
		// config file code
		String configCode = DistAlgoPrinter.generateConfigcode(dAlgo);
		IPath configPath = projectPath.addTrailingSeparator()
	             .append("DistAlgo").addTrailingSeparator() // TODO : "DistAlgo" should be a constant
	             .append(selectedMachine.getComponentName()).addTrailingSeparator()
	             .append("config.da"); // TODO : "config.da" should be a constant
		writeToFile(configPath, configCode);
		
		// build the code as a string
		String mainCode = DistAlgoPrinter.generateMainCode(dAlgo);
		
		// create path where code is generated
		IPath mainPath = projectPath.addTrailingSeparator()
	             .append("DistAlgo").addTrailingSeparator() // TODO : "DistAlgo" should be a constant
	             .append(selectedMachine.getComponentName()).addTrailingSeparator()
	             .append("main.da"); // TODO : "main.da" should be a constant
		File mainFile = new File(mainPath.toOSString());
		// create directories if necessary
		mainFile.getParentFile().mkdirs();
		// TODO factoriser avec la fonction writeToFile
		FileWriter writer;
		try {
			// write code to file
			writer = new  FileWriter(mainFile);
			writer.write(mainCode);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			MessageDialog.openInformation(shell, "Info", e.toString());
			e.printStackTrace();
		}
		
		for(ProcessClass pcl : dAlgo.getProcessClasses()) {
			String pclCode = DistAlgoPrinter.generateProcessClassCode(pcl);
			IPath pclPath = projectPath.addTrailingSeparator()
		             .append("DistAlgo").addTrailingSeparator() // TODO : "DistAlgo" should be a constant
		             .append(selectedMachine.getComponentName()).addTrailingSeparator()
		             .append(DistAlgoPrinter.classFileName(pcl.getName()));
			File pclFile = new File(pclPath.toOSString());
			// create directories if necessary
			pclFile.getParentFile().mkdirs();
			try {
				// write code to file
				writer = new  FileWriter(pclFile);
				writer.write(pclCode);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				MessageDialog.openInformation(shell, "Info", e.toString());
				e.printStackTrace();
			}
		}
		
		for(DAEnumSet enumSet : dAlgo.getEnumSets()) {
			String enumSetCode = DistAlgoPrinter.generateEnumSetCode(enumSet);
			IPath enumPath = projectPath.addTrailingSeparator()
		             .append("DistAlgo").addTrailingSeparator() // TODO : "DistAlgo" should be a constant
		             .append(selectedMachine.getComponentName()).addTrailingSeparator()
		             .append(DistAlgoPrinter.enumSetFileName(enumSet.getName()));
			writeToFile(enumPath, enumSetCode);
			/*
			File enumFile = new File(enumPath.toOSString());
			// create directories if necessary
			enumFile.getParentFile().mkdirs();
			try {
				// write code to file
				writer = new  FileWriter(enumFile);
				writer.write(enumSetCode);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				MessageDialog.openInformation(shell, "Info", e.toString());
				e.printStackTrace();
			}
			*/
		}
	}
	
	private void writeToFile(IPath path, String code) {
		File file = new File(path.toOSString());
		// create directories if necessary
		file.getParentFile().mkdirs();
		FileWriter writer;
		try {
			// write code to file
			writer = new  FileWriter(file);
			writer.write(code);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			MessageDialog.openInformation(shell, "Info", e.toString());
			e.printStackTrace();
		}
	}
	
	private static Path createFileWithDir(String directory, String filename) {
        File dir = new File(directory);
        if (!dir.exists()) dir.mkdirs();
        return Paths.get(directory + File.separatorChar + filename);
    }
	
	public static void write(IPath dir, String filename, String s) throws IOException {
		Path p = createFileWithDir(dir.toOSString(), filename);
		Files.write(p, s.getBytes("utf-8"), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		if(selection instanceof IStructuredSelection) {    
	        Object element = ((IStructuredSelection)selection).getFirstElement();    
        
	        if (element instanceof IMachineRoot) {    
	        	selectedMachine = ((IMachineRoot) element);    
	        }  
	    }
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
		
	}

}
