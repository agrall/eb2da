package fr.loria.mosel.rodin.eb2da.utils;

public class Constants {

	public static final String NODES = "NODES";
	public static final String MESSAGES = "MESSAGES";
	public static final String STATES = "STATES";
	public static final String NETWORK_TYPING= "network_typing";
	public static final String NETWOK_VALUE = "network_value";
	public static final String CONSTANT_TYPING_SUFFIX = "_typing";
	public static final String VARIABLE_TYPING_SUFFIX = "_typing";
	public static final String CONSTANT_VALUE_SUFFIX = "_value";
	public static final String CHANNELS_SET = "CHANNELS";
	public static final String SEND = "send";
	public static final String RECEIVE = "receive";
	public static final String LOSE = "lose";
	public static final String SENT = "sent";
	public static final String RECEIVED = "received";
	public static final String INCHANNEL = "inchannel";
	public static final String READY_FOR_RECEPTION = "readyForReception";
	public static final String EMPTY_CHANNEL = "emptyChannel";
	public static final String RELIABLE = "reliable";
	public static final String CHANNELS = "channels_typing";
	public static final String PC = "pc_typing";
	public static final String ENUM_SET_SUFFIX = "_enum";
	public static final String INDENT_OFFSET = "    ";
	public static final String DONE = "done";
	public static final String INITIALISATION = "INITIALISATION";
	public static final String RAN = "ran";
}
