# EB2DA

## Installation Procedure

### Installing Eclipse and eGit (eGit is optionnal)

1.  Download and install Eclipse from https://www.eclipse.org/downloads/.
2.  During the installation, select the JEE version.
3.  Install [eGit](https://www.eclipse.org/egit/) : Help > Install New Software > click Add > copy paste this link https://download.eclipse.org/egit/updates/ > Select Git Integration for Eclipse and install it.

### Importing EB2DA project from eGit

1. File > Import > Git > Projects from Git > click Next > Clone URI
2. Copy paste the URI of the project in the URI field, it should look like git@gitlab.inria.fr:agrall/eb2da.git.
3. Select master.
4. Choose a directory for this project.
5. Import existing project
6. Select project fr.loria.mosel.rodin.eb2da (the name may change in the future) and finish.

### Setting up Rodin as target platform and run configuration

1. [Follow the steps in this link to setup Rodin as the target platform of our plug-in.](http://wiki.event-b.org/index.php/Developer_FAQ#How_to_setup_an_environment_for_plug-in_development.3F)
2. Open the "run configurations" window and double click "eclipse application" to create a new run configuration.
3. Change the name of this freshly created run configuration to the version of Rodin you downloaded.
4. In the field "run a product", select org.rodinp.platform.product.
5. The execution environment should be javaSE-1.8.
6. In the plugins tab, select "plugins selected below only", select eb2da in the workspace and "target platform", type "test" in the filter text field and unselect the result.
7. Apply.